﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zemedya.Business.Abstract.UnitOfWork;
using Zemedya.Model.VM.Page;

namespace Zemedya.AspNetCoreMVC.NavbarComponents
{
    public class NavbarViewComponent : ViewComponent
    {
        private IUnitOfWork _unitOfWork;
        public NavbarViewComponent(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IViewComponentResult Invoke()
        {
            NavbarVM navbarVM = new NavbarVM
            {
                KurumsalListesi = _unitOfWork.KurumsalManager.HepsiniGetir(x => x.Aktif),
                BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.Aktif),
                //UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                //AltKategoriler = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.UstKategoriId != 1 || x.Aktif == true),
                MediaListesi = _unitOfWork.EkipManager.HepsiniGetir(x => x.Aktif == true),
                MevcutSayfalar = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1),
                MevcutDiller = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1),
                EtkinlikListesi = _unitOfWork.EtkinlikManager.HepsiniGetir(x => x.Aktif),
                Ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1),
                DuyuruListesi = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif == true),
                KurumsalDTO = _unitOfWork.KurumsalManager.Getir(x => x.Id == 2),
                VideoDTO = _unitOfWork.VideoManager.Getir(x => x.Id == 2),
                Videolar = _unitOfWork.VideoManager.HepsiniGetir(x => x.Id != 2)
            };
            return View(navbarVM);
        }
    }
}
