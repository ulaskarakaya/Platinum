﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zemedya.Business.Abstract.UnitOfWork;
using Zemedya.Model.VM.Page;

namespace Zemedya.AspNetCoreMVC.NavbarComponents
{
    public class FooterViewComponent : ViewComponent
    {
        private IUnitOfWork _unitOfWork;
        public FooterViewComponent(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IViewComponentResult Invoke()
        {
            FooterVM footerVM = new FooterVM();
            footerVM.Iletisim = _unitOfWork.IletisimManager.Getir(x => x.Aktif == true);
            footerVM.Ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            footerVM.Duyurular = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif == true);
            footerVM.Etkinlikler = _unitOfWork.EtkinlikManager.HepsiniGetir(x => x.Aktif == true);
            footerVM.MediaListesi = _unitOfWork.EkipManager.HepsiniGetir(x => x.Aktif == true);
            footerVM.BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.Aktif);
            footerVM.KurumsalDTO = _unitOfWork.KurumsalManager.Getir(x => x.Id == 2);
            footerVM.VideoDTO = _unitOfWork.VideoManager.Getir(x => x.Id == 2);
            footerVM.Videolar = _unitOfWork.VideoManager.HepsiniGetir(x => x.Id != 2);
            return View(footerVM);
        }
    }
}
