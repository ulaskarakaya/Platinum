﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zemedya.Business.Abstract.UnitOfWork;

namespace Zemedya.AspNetCoreMVC.NavbarComponents
{
    public class PanelNavbarViewComponent : ViewComponent
    {
        private IUnitOfWork _unitOfWork;
        public PanelNavbarViewComponent(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IViewComponentResult Invoke()
        {
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            return View(sayfa);
        }
    }
}
