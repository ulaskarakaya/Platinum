﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Zemedya.AspNetCoreMVC.Models;
using Zemedya.Business.Abstract.UnitOfWork;
using Zemedya.Entity.Concrete.AcikPozisyonEtities;
using Zemedya.Entity.Concrete.MediaEntities;
using Zemedya.Entity.Concrete.MembershipEntities;
using Zemedya.Entity.Concrete.PressEntities;
using Zemedya.Entity.Concrete.Team;
using Zemedya.Model.DTO.AyarlarDTOs;
using Zemedya.Model.DTO.BlogDTOs;
using Zemedya.Model.DTO.DuyuruDTOs;
using Zemedya.Model.DTO.EkipDTOs;
using Zemedya.Model.DTO.EtkinlikDTOs;
using Zemedya.Model.DTO.GaleriDTOs;
using Zemedya.Model.DTO.GirisDTO;
using Zemedya.Model.DTO.HaberDTOs;
using Zemedya.Model.DTO.IletisimDTOs;
using Zemedya.Model.DTO.KurumsalDTOs;
using Zemedya.Model.DTO.PopupDTOs;
using Zemedya.Model.DTO.ReferansDTOs;
using Zemedya.Model.DTO.SliderDTOs;
using Zemedya.Model.DTO.UrunDTOs;
using Zemedya.Model.DTO.VideoDTOs;
using Zemedya.Model.DTO.YoneticiDTOs;
using Zemedya.Model.VM;
using Zemedya.Model.VM.Page;

namespace Zemedya.AspNetCoreMVC.Controllers
{
    public class PanelController : Controller
    {
        private IUnitOfWork _unitOfWork;
        public PanelController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        [Route("/Panel/Career")]
        public IActionResult Video()
        {
            var videolar = _unitOfWork.VideoManager.HepsiniGetir();
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(videolar);
        }

        [Authorize]
        public IActionResult KontrolPaneli()
        {
            KontrolPaneliVM kontrolPaneliVM = new KontrolPaneliVM
            {
                MevcutSayfalar = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1),
                MevcutDiller = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1)
            };
            return View(kontrolPaneliVM);
        }


        public ActionResult PasifeCevirSayfa(string Id)
        {
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            if (Id == "1")
            {
                if (sayfa.Haberler)
                {
                    sayfa.Haberler = false;
                }
                else
                {
                    sayfa.Haberler = true;
                }
            }
            else if (Id == "2")
            {
                if (sayfa.Duyuru)
                {
                    sayfa.Duyuru = false;
                }
                else
                {
                    sayfa.Duyuru = true;
                }
            }
            else if (Id == "3")
            {
                if (sayfa.Blog)
                {
                    sayfa.Blog = false;
                }
                else
                {
                    sayfa.Blog = true;
                }
            }
            else if (Id == "31")
            {
                if (sayfa.BlogKategori)
                {
                    sayfa.BlogKategori = false;
                }
                else
                {
                    sayfa.BlogKategori = true;
                }
            }
            else if (Id == "4")
            {
                if (sayfa.Etkinlik)
                {
                    sayfa.Etkinlik = false;
                }
                else
                {
                    sayfa.Etkinlik = true;
                }
            }
            else if (Id == "5")
            {
                if (sayfa.Iletisim)
                {
                    sayfa.Iletisim = false;
                }
                else
                {
                    sayfa.Iletisim = true;
                }
            }
            else if (Id == "6")
            {
                if (sayfa.Kurumsal)
                {
                    sayfa.Kurumsal = false;
                }
                else
                {
                    sayfa.Kurumsal = true;
                }
            }
            else if (Id == "7")
            {
                if (sayfa.Referans)
                {
                    sayfa.Referans = false;
                }
                else
                {
                    sayfa.Referans = true;
                }
            }
            else if (Id == "8")
            {
                if (sayfa.Urunler)
                {
                    sayfa.Urunler = false;
                }
                else
                {
                    sayfa.Urunler = true;
                }
            }
            else if (Id == "81")
            {
                if (sayfa.UrunKategori)
                {
                    sayfa.UrunKategori = false;
                }
                else
                {
                    sayfa.UrunKategori = true;
                }
            }
            else if (Id == "9")
            {
                if (sayfa.Video)
                {
                    sayfa.Video = false;
                }
                else
                {
                    sayfa.Video = true;
                }
            }
            else if (Id == "10")
            {
                if (sayfa.Ekip)
                {
                    sayfa.Ekip = false;
                }
                else
                {
                    sayfa.Ekip = true;
                }
            }
            else if (Id == "11")
            {
                if (sayfa.Galeri)
                {
                    sayfa.Galeri = false;
                }
                else
                {
                    sayfa.Galeri = true;
                }
            }
            else if (Id == "111")
            {
                if (sayfa.GaleriKategori)
                {
                    sayfa.GaleriKategori = false;
                }
                else
                {
                    sayfa.GaleriKategori = true;
                }
            }

            _unitOfWork.MevcutSayfalarManager.Guncelle(sayfa);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirDil(string Id)
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (Id == "2")
            {
                if (dil.EN)
                {
                    dil.EN = false;
                }
                else
                {
                    dil.EN = true;
                }
            }

            _unitOfWork.MevcutDillerManager.Guncelle(dil);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }


        [HttpPost]
        public ActionResult VideoEkle([FromForm] VideoDTO videoDTO, IFormFile resim, IFormFile resimEN)
        {
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                videoDTO.Gorsel = resimAd;
            }
            else
            {
                videoDTO.Gorsel = "null.png";
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                videoDTO.GorselEN = resimAdEN;
            }
            else
            {
                videoDTO.GorselEN = "null.png";
            }
            _unitOfWork.VideoManager.Ekle(videoDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult VideoGuncelle([FromForm] string Id)
        {
            var entity = _unitOfWork.VideoManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                ad = entity.Ad,
                adEN = entity.AdEN,
                baslik = entity.Baslik,
                baslikEN = entity.BaslikEN,
                videourl = entity.VideoUrl,
                videourlEN = entity.VideoUrlEN,
                gorsel = entity.Gorsel,
                gorselEN = entity.GorselEN,
                aktif = entity.Aktif
            });
        }

        public ActionResult VideoGuncel([FromForm] VideoDTO videoDTO, IFormFile resim, IFormFile resimEN)
        {
            var video = _unitOfWork.VideoManager.Getir(x => x.Id == videoDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                videoDTO.Gorsel = resimAd;
            }
            else
            {
                videoDTO.Gorsel = video.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                videoDTO.GorselEN = resimAdEN;
            }
            else
            {
                videoDTO.GorselEN = video.GorselEN;
            }
            _unitOfWork.VideoManager.Guncelle(videoDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirVideo(string Id)
        {
            var referans = _unitOfWork.VideoManager.Getir(x => x.Id == int.Parse(Id));
            if (referans.Aktif)
            {
                referans.Aktif = false;
            }
            else
            {
                referans.Aktif = true;
            }
            _unitOfWork.VideoManager.Guncelle(referans);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult VideoSil(string Id)
        {
            _unitOfWork.VideoManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        [Route("/Panel/AnaSayfa")]
        public IActionResult Kurumsal()
        {
            var entity = _unitOfWork.KurumsalManager.HepsiniGetir(x => x.Aktif);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(entity);
        }

        [HttpPost]
        [Route("/Panel/AnaSayfa")]
        public ActionResult KurumsalEkle([FromForm] KurumsalDTO kurumsalDTO)
        {

            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (kurumsalDTO.SEOEN != null && !(_unitOfWork.KurumsalManager.SEOKontrolEN(kurumsalDTO.SEOEN)))
                {
                    if (kurumsalDTO.SEO != null && !(_unitOfWork.KurumsalManager.SEOKontrol(kurumsalDTO.SEO)))
                    {
                        var entitysayisi = _unitOfWork.KurumsalManager.HepsiniGetir().Count();
                        kurumsalDTO.SıralamaNo = entitysayisi + 1;
                        _unitOfWork.KurumsalManager.Ekle(kurumsalDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (kurumsalDTO.SEO != null && !(_unitOfWork.KurumsalManager.SEOKontrol(kurumsalDTO.SEO)))
                {
                    var entitysayisi = _unitOfWork.KurumsalManager.HepsiniGetir().Count();
                    kurumsalDTO.SıralamaNo = entitysayisi + 1;
                    _unitOfWork.KurumsalManager.Ekle(kurumsalDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }


        public ActionResult KurumsalGuncelle([FromForm] string Id)
        {
            var entity = _unitOfWork.KurumsalManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = entity.Isim,
                isimEN = entity.IsimEN,
                seo = entity.SEO,
                seoEN = entity.SEOEN,
                detay = entity.Detay,
                detayEN = entity.DetayEN,
                aktif = entity.Aktif,
                sıralamaNo = entity.SıralamaNo,
                gorsel = entity.Gorsel,
                gorselEN = entity.GorselEN
            });
        }

        public ActionResult KurumsalGuncel([FromForm] KurumsalDTO kurumsalDTO, IFormFile resim, IFormFile resimEN)
        {

            var kurumsal = _unitOfWork.KurumsalManager.Getir(x => x.Id == kurumsalDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                kurumsalDTO.Gorsel = resimAd;
            }
            else
            {
                kurumsalDTO.Gorsel = kurumsal.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                kurumsalDTO.GorselEN = resimAdEN;
            }
            else
            {
                kurumsalDTO.GorselEN = kurumsal.GorselEN;
            }
            _unitOfWork.KurumsalManager.Guncelle(kurumsalDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirKurumsal(string Id)
        {
            var entity = _unitOfWork.KurumsalManager.Getir(x => x.Id == int.Parse(Id));
            if (entity.Aktif)
            {
                entity.Aktif = false;
            }
            else
            {
                entity.Aktif = true;
            }
            _unitOfWork.KurumsalManager.Guncelle(entity);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }



        [Authorize]
        public IActionResult Referans()
        {
            var referanslar = _unitOfWork.ReferansManager.HepsiniGetir();
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(referanslar);
        }

        [HttpPost]
        public ActionResult ReferansEkle([FromForm] ReferansDTO referansDTO, IFormFile resim, IFormFile resimEN)
        {
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                referansDTO.Gorsel = resimAd;
            }
            else
            {
                referansDTO.Gorsel = "null.png";
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                referansDTO.GorselEN = resimAdEN;
            }
            else
            {
                referansDTO.GorselEN = "null.png";
            }
            _unitOfWork.ReferansManager.Ekle(referansDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult ReferansGuncelle([FromForm] string Id)
        {
            var referans = _unitOfWork.ReferansManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = referans.Isim,
                isimEN = referans.IsimEN,
                gorsel = referans.Gorsel,
                gorselEN = referans.GorselEN
            });
        }

        public ActionResult ReferansGuncel([FromForm] ReferansDTO referansDTO, IFormFile resim, IFormFile resimEN)
        {
            var referans = _unitOfWork.ReferansManager.Getir(x => x.Id == referansDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                referansDTO.Gorsel = resimAd;
            }
            else
            {
                referansDTO.Gorsel = referans.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                referansDTO.GorselEN = resimAdEN;
            }
            else
            {
                referansDTO.GorselEN = referans.GorselEN;
            }
            _unitOfWork.ReferansManager.Guncelle(referansDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirReferans(string Id)
        {
            var referans = _unitOfWork.ReferansManager.Getir(x => x.Id == int.Parse(Id));
            if (referans.Aktif)
            {
                referans.Aktif = false;
            }
            else
            {
                referans.Aktif = true;
            }
            _unitOfWork.ReferansManager.Guncelle(referans);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult ReferansSil(string Id)
        {
            _unitOfWork.ReferansManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult Galeri()
        {
            GaleriEkleVM galeriEkleVM = new GaleriEkleVM
            {
                GaleriListesi = _unitOfWork.GaleriManager.HepsiniGetir(),
                GaleriKategorileri = _unitOfWork.GaleriKategoriManager.HepsiniGetir()
            };
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            ViewBag.kategori = sayfa.GaleriKategori.ToString();
            return View(galeriEkleVM);
        }

        [HttpPost]
        public ActionResult GaleriEkle([FromForm] GaleriDTO galeriDTO, IFormFile resim, IFormFile resimEN)
        {
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                galeriDTO.Gorsel = resimAd;
            }
            else
            {
                galeriDTO.Gorsel = "null.png";
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                galeriDTO.GorselEN = resimAdEN;
            }
            else
            {
                galeriDTO.GorselEN = "null.png";
            }
            _unitOfWork.GaleriManager.Ekle(galeriDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult GaleriGuncelle([FromForm] string Id)
        {
            var galeri = _unitOfWork.GaleriManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = galeri.Isim,
                isimEN = galeri.IsimEN,
                gorsel = galeri.Gorsel,
                gorselEN = galeri.GorselEN,
                kategori = galeri.GaleriKategoriId
            });
        }

        public ActionResult GaleriGuncel([FromForm] GaleriDTO galeriDTO, IFormFile resim, IFormFile resimEN)
        {
            var galeri = _unitOfWork.GaleriManager.Getir(x => x.Id == galeriDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                galeriDTO.Gorsel = resimAd;
            }
            else
            {
                galeriDTO.Gorsel = galeri.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                galeriDTO.GorselEN = resimAdEN;
            }
            else
            {
                galeriDTO.GorselEN = galeri.GorselEN;
            }
            _unitOfWork.GaleriManager.Guncelle(galeriDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirGaleri(string Id)
        {
            var entity = _unitOfWork.GaleriManager.Getir(x => x.Id == int.Parse(Id));
            if (entity.Aktif)
            {
                entity.Aktif = false;
            }
            else
            {
                entity.Aktif = true;
            }
            _unitOfWork.GaleriManager.Guncelle(entity);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult GaleriSil(string Id)
        {
            _unitOfWork.GaleriManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult GaleriKategori()
        {
            var kategoriler = _unitOfWork.GaleriKategoriManager.HepsiniGetir();
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(kategoriler);
        }

        [HttpPost]
        public ActionResult GaleriKategoriEkle([FromForm] GaleriKategoriDTO galeriKategoriDTO, IFormFile resim, IFormFile resimEN)
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (galeriKategoriDTO.SEOEN != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrolEN(galeriKategoriDTO.SEOEN)))
                {
                    if (galeriKategoriDTO.SEO != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrol(galeriKategoriDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            galeriKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            galeriKategoriDTO.Gorsel = "null.png";
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            galeriKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            galeriKategoriDTO.GorselEN = "null.png";
                        }
                        _unitOfWork.GaleriKategoriManager.Ekle(galeriKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (galeriKategoriDTO.SEO != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrol(galeriKategoriDTO.SEO)))
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        galeriKategoriDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        galeriKategoriDTO.Gorsel = "null.png";
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        galeriKategoriDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        galeriKategoriDTO.GorselEN = "null.png";
                    }
                    _unitOfWork.GaleriKategoriManager.Ekle(galeriKategoriDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }



        }


        public ActionResult GaleriKategoriGuncelle([FromForm] string Id)
        {
            var galerikategori = _unitOfWork.GaleriKategoriManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = galerikategori.Isim,
                isimEN = galerikategori.IsimEN,
                seo = galerikategori.SEO,
                seoEN = galerikategori.SEOEN,
                gorsel = galerikategori.Gorsel,
                gorselEN = galerikategori.GorselEN
            });
        }

        public ActionResult GaleriKategoriGuncel([FromForm] GaleriKategoriDTO galeriKategoriDTO, IFormFile resim, IFormFile resimEN)
        {
            var galerikategori = _unitOfWork.GaleriKategoriManager.Getir(x => x.Id == galeriKategoriDTO.Id);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (galeriKategoriDTO.SEOEN != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrolEN(galeriKategoriDTO.SEOEN)))
                {
                    if (galeriKategoriDTO.SEO != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrol(galeriKategoriDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            galeriKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            galeriKategoriDTO.Gorsel = galerikategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            galeriKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            galeriKategoriDTO.GorselEN = galerikategori.GorselEN;
                        }
                        _unitOfWork.GaleriKategoriManager.Guncelle(galeriKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (galeriKategoriDTO.SEO != null && galeriKategoriDTO.SEO == galerikategori.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            galeriKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            galeriKategoriDTO.Gorsel = galerikategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            galeriKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            galeriKategoriDTO.GorselEN = galerikategori.GorselEN;
                        }
                        _unitOfWork.GaleriKategoriManager.Guncelle(galeriKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else if (galeriKategoriDTO.SEOEN != null && galeriKategoriDTO.SEOEN == galerikategori.SEOEN)
                {
                    if (galeriKategoriDTO.SEO != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrol(galeriKategoriDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            galeriKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            galeriKategoriDTO.Gorsel = galerikategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            galeriKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            galeriKategoriDTO.GorselEN = galerikategori.GorselEN;
                        }
                        _unitOfWork.GaleriKategoriManager.Guncelle(galeriKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (galeriKategoriDTO.SEO != null && galeriKategoriDTO.SEO == galerikategori.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            galeriKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            galeriKategoriDTO.Gorsel = galerikategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            galeriKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            galeriKategoriDTO.GorselEN = galerikategori.GorselEN;
                        }
                        _unitOfWork.GaleriKategoriManager.Guncelle(galeriKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (galeriKategoriDTO.SEO != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrol(galeriKategoriDTO.SEO)))
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        galeriKategoriDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        galeriKategoriDTO.Gorsel = galerikategori.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        galeriKategoriDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        galeriKategoriDTO.GorselEN = galerikategori.GorselEN;
                    }
                    _unitOfWork.GaleriKategoriManager.Guncelle(galeriKategoriDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else if (galeriKategoriDTO.SEO != null && galeriKategoriDTO.SEO == galerikategori.SEO)
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        galeriKategoriDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        galeriKategoriDTO.Gorsel = galerikategori.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        galeriKategoriDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        galeriKategoriDTO.GorselEN = galerikategori.GorselEN;
                    }
                    _unitOfWork.GaleriKategoriManager.Guncelle(galeriKategoriDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }

        [HttpPost]
        public ActionResult PasifeCevirGaleriKategori(string Id)
        {
            var entity = _unitOfWork.GaleriKategoriManager.Getir(x => x.Id == int.Parse(Id));
            if (entity.Aktif)
            {
                entity.Aktif = false;
            }
            else
            {
                entity.Aktif = true;
            }
            _unitOfWork.GaleriKategoriManager.Guncelle(entity);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult GaleriKategoriSil(string Id)
        {
            _unitOfWork.GaleriKategoriManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        [Route("/Panel/Media")]
        public IActionResult Ekip()
        {
            var ekip = _unitOfWork.EkipManager.HepsiniGetir();
            return View(ekip);
        }

        [HttpPost]
        public ActionResult EkipEkle([FromForm] EkipDTO ekipDTO, IFormFile resim, IFormFile resimEN)
        {
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                ekipDTO.Gorsel = resimAd;
            }
            else
            {
                ekipDTO.Gorsel = "null.png";
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resim.CopyTo(streamEN);
                }
                ekipDTO.GorselEN = resimAdEN;
            }
            else
            {
                ekipDTO.GorselEN = "null.png";
            }

            _unitOfWork.EkipManager.Ekle(ekipDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult EkipGuncelle([FromForm] string Id)
        {
            var ekip = _unitOfWork.EkipManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = ekip.Isim,
                gorsel = ekip.Gorsel,
                isimEN = ekip.IsimEN,
                gorselEN = ekip.GorselEN,
                seo = ekip.SEO,
                seoEN = ekip.SEOEN,
                detay = ekip.Detay,
                detayEN = ekip.DetayEN
            });
        }

        public ActionResult EkipGuncel([FromForm] EkipDTO ekipDTO, IFormFile resim, IFormFile resimEN)
        {
            var ekip = _unitOfWork.EkipManager.Getir(x => x.Id == ekipDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                ekipDTO.Gorsel = resimAd;
            }
            else
            {
                ekipDTO.Gorsel = ekip.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                ekipDTO.GorselEN = resimAdEN;
            }
            else
            {
                ekipDTO.GorselEN = ekip.GorselEN;
            }
            _unitOfWork.EkipManager.Guncelle(ekipDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult PasifeCevirEkip(string Id)
        {
            var entity = _unitOfWork.EkipManager.Getir(x => x.Id == int.Parse(Id));
            if (entity.Aktif)
            {
                entity.Aktif = false;
            }
            else
            {
                entity.Aktif = true;
            }
            _unitOfWork.EkipManager.Guncelle(entity);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult EkipSil(string Id)
        {
            _unitOfWork.EkipManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        [Route("/Panel/SolutionPartners")]
        public IActionResult Etkinlik()
        {
            var etkinlikler = _unitOfWork.EtkinlikManager.HepsiniGetir();
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(etkinlikler);
        }


        public ActionResult EtkinlikEkle([FromForm] EtkinlikDTO etkinlikDTO, IFormFile resim, IFormFile resimEN, IFormFile resim2, IFormFile resimEN2)
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (etkinlikDTO.SEOEN != null && !(_unitOfWork.EtkinlikManager.SEOKontrolEN(etkinlikDTO.SEOEN)))
                {
                    if (etkinlikDTO.SEO != null && !(_unitOfWork.EtkinlikManager.SEOKontrol(etkinlikDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            etkinlikDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel = "null.png";
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            etkinlikDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            etkinlikDTO.GorselEN = "null.png";
                        }
                        if (resim2 != null)
                        {
                            string uzanti2 = Path.GetExtension(resim2.FileName);
                            string resimAd2 = Guid.NewGuid() + uzanti2;
                            string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                            using (var stream2 = new FileStream(path2, FileMode.Create))
                            {
                                resim2.CopyTo(stream2);
                            }
                            etkinlikDTO.Gorsel2 = resimAd2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2 = "null.png";
                        }
                        if (resimEN2 != null)
                        {
                            string uzantiEN2 = Path.GetExtension(resimEN.FileName);
                            string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                            string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                            using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                            {
                                resimEN2.CopyTo(streamEN2);
                            }
                            etkinlikDTO.Gorsel2EN = resimAdEN2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2EN = "null.png";
                        }
                        var entitysayisi = _unitOfWork.EtkinlikManager.HepsiniGetir().Count();
                        etkinlikDTO.SıralamaNo = entitysayisi + 1;
                        _unitOfWork.EtkinlikManager.Ekle(etkinlikDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (etkinlikDTO.SEO != null && !(_unitOfWork.EtkinlikManager.SEOKontrol(etkinlikDTO.SEO)))
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        etkinlikDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel = "null.png";
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        etkinlikDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        etkinlikDTO.GorselEN = "null.png";
                    }
                    if (resim2 != null)
                    {
                        string uzanti2 = Path.GetExtension(resim2.FileName);
                        string resimAd2 = Guid.NewGuid() + uzanti2;
                        string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                        using (var stream2 = new FileStream(path2, FileMode.Create))
                        {
                            resim2.CopyTo(stream2);
                        }
                        etkinlikDTO.Gorsel2 = resimAd2;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel2 = "null.png";
                    }
                    if (resimEN2 != null)
                    {
                        string uzantiEN2 = Path.GetExtension(resimEN.FileName);
                        string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                        string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                        using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                        {
                            resimEN2.CopyTo(streamEN2);
                        }
                        etkinlikDTO.Gorsel2EN = resimAdEN2;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel2EN = "null.png";
                    }
                    var entitysayisi = _unitOfWork.EtkinlikManager.HepsiniGetir().Count();
                    etkinlikDTO.SıralamaNo = entitysayisi + 1;
                    _unitOfWork.EtkinlikManager.Ekle(etkinlikDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }


        public ActionResult EtkinlikGuncelle([FromForm] string Id)
        {
            var etkinlik = _unitOfWork.EtkinlikManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = etkinlik.Isim,
                gorsel = etkinlik.Gorsel,
                isimEN = etkinlik.IsimEN,
                gorselEN = etkinlik.GorselEN,
                seo = etkinlik.SEO,
                seoEN = etkinlik.SEOEN,
                ozet = etkinlik.Ozet,
                ozetEN = etkinlik.OzetEN,
                detay = etkinlik.Detay,
                detayEN = etkinlik.DetayEN,
                aktif = etkinlik.Aktif,
                detay2 = etkinlik.Detay2,
                detay2EN = etkinlik.Detay2EN,
                gorsel2 = etkinlik.Gorsel2,
                gorselEN2 = etkinlik.Gorsel2EN
            });
        }

        public ActionResult EtkinlikGuncel([FromForm] EtkinlikDTO etkinlikDTO, IFormFile resim, IFormFile resimEN, IFormFile resim2, IFormFile resimEN2)
        {
            var etkinlik = _unitOfWork.EtkinlikManager.Getir(x => x.Id == etkinlikDTO.Id);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (etkinlikDTO.SEOEN != null && !(_unitOfWork.EtkinlikManager.SEOKontrolEN(etkinlikDTO.SEOEN)))
                {
                    if (etkinlikDTO.SEO != null && !(_unitOfWork.EtkinlikManager.SEOKontrol(etkinlikDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            etkinlikDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel = etkinlik.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            etkinlikDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            etkinlikDTO.GorselEN = etkinlik.GorselEN;
                        }
                        if (resim2 != null)
                        {
                            string uzanti2 = Path.GetExtension(resim2.FileName);
                            string resimAd2 = Guid.NewGuid() + uzanti2;
                            string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                            using (var stream2 = new FileStream(path2, FileMode.Create))
                            {
                                resim2.CopyTo(stream2);
                            }
                            etkinlikDTO.Gorsel2 = resimAd2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2 = etkinlik.Gorsel2;
                        }
                        if (resimEN2 != null)
                        {
                            string uzantiEN2 = Path.GetExtension(resimEN2.FileName);
                            string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                            string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                            using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                            {
                                resimEN2.CopyTo(streamEN2);
                            }
                            etkinlikDTO.Gorsel2EN = resimAdEN2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2EN = etkinlik.Gorsel2EN;
                        }
                        _unitOfWork.EtkinlikManager.Guncelle(etkinlikDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (etkinlikDTO.SEO != null && etkinlikDTO.SEO == etkinlik.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            etkinlikDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel = etkinlik.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            etkinlikDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            etkinlikDTO.GorselEN = etkinlik.GorselEN;
                        }
                        if (resim2 != null)
                        {
                            string uzanti2 = Path.GetExtension(resim2.FileName);
                            string resimAd2 = Guid.NewGuid() + uzanti2;
                            string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                            using (var stream2 = new FileStream(path2, FileMode.Create))
                            {
                                resim2.CopyTo(stream2);
                            }
                            etkinlikDTO.Gorsel2 = resimAd2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2 = etkinlik.Gorsel2;
                        }
                        if (resimEN2 != null)
                        {
                            string uzantiEN2 = Path.GetExtension(resimEN2.FileName);
                            string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                            string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                            using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                            {
                                resimEN2.CopyTo(streamEN2);
                            }
                            etkinlikDTO.Gorsel2EN = resimAdEN2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2EN = etkinlik.Gorsel2EN;
                        }
                        _unitOfWork.EtkinlikManager.Guncelle(etkinlikDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else if (etkinlikDTO.SEOEN != null && etkinlikDTO.SEOEN == etkinlik.SEOEN)
                {
                    if (etkinlikDTO.SEO != null && !(_unitOfWork.EtkinlikManager.SEOKontrol(etkinlikDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            etkinlikDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel = etkinlik.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            etkinlikDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            etkinlikDTO.GorselEN = etkinlik.GorselEN;
                        }
                        if (resim2 != null)
                        {
                            string uzanti2 = Path.GetExtension(resim2.FileName);
                            string resimAd2 = Guid.NewGuid() + uzanti2;
                            string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                            using (var stream2 = new FileStream(path2, FileMode.Create))
                            {
                                resim2.CopyTo(stream2);
                            }
                            etkinlikDTO.Gorsel2 = resimAd2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2 = etkinlik.Gorsel2;
                        }
                        if (resimEN2 != null)
                        {
                            string uzantiEN2 = Path.GetExtension(resimEN2.FileName);
                            string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                            string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                            using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                            {
                                resimEN2.CopyTo(streamEN2);
                            }
                            etkinlikDTO.Gorsel2EN = resimAdEN2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2EN = etkinlik.Gorsel2EN;
                        }
                        _unitOfWork.EtkinlikManager.Guncelle(etkinlikDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (etkinlikDTO.SEO != null && etkinlikDTO.SEO == etkinlik.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            etkinlikDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel = etkinlik.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            etkinlikDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            etkinlikDTO.GorselEN = etkinlik.GorselEN;
                        }
                        if (resim2 != null)
                        {
                            string uzanti2 = Path.GetExtension(resim2.FileName);
                            string resimAd2 = Guid.NewGuid() + uzanti2;
                            string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                            using (var stream2 = new FileStream(path2, FileMode.Create))
                            {
                                resim2.CopyTo(stream2);
                            }
                            etkinlikDTO.Gorsel2 = resimAd2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2 = etkinlik.Gorsel2;
                        }
                        if (resimEN2 != null)
                        {
                            string uzantiEN2 = Path.GetExtension(resimEN2.FileName);
                            string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                            string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                            using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                            {
                                resimEN2.CopyTo(streamEN2);
                            }
                            etkinlikDTO.Gorsel2EN = resimAdEN2;
                        }
                        else
                        {
                            etkinlikDTO.Gorsel2EN = etkinlik.Gorsel2EN;
                        }
                        _unitOfWork.EtkinlikManager.Guncelle(etkinlikDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (etkinlikDTO.SEO != null && !(_unitOfWork.EtkinlikManager.SEOKontrol(etkinlikDTO.SEO)))
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        etkinlikDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel = etkinlik.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        etkinlikDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        etkinlikDTO.GorselEN = etkinlik.GorselEN;
                    }
                    if (resim2 != null)
                    {
                        string uzanti2 = Path.GetExtension(resim2.FileName);
                        string resimAd2 = Guid.NewGuid() + uzanti2;
                        string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                        using (var stream2 = new FileStream(path2, FileMode.Create))
                        {
                            resim2.CopyTo(stream2);
                        }
                        etkinlikDTO.Gorsel2 = resimAd2;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel2 = etkinlik.Gorsel2;
                    }
                    if (resimEN2 != null)
                    {
                        string uzantiEN2 = Path.GetExtension(resimEN2.FileName);
                        string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                        string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                        using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                        {
                            resimEN2.CopyTo(streamEN2);
                        }
                        etkinlikDTO.Gorsel2EN = resimAdEN2;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel2EN = etkinlik.Gorsel2EN;
                    }
                    _unitOfWork.EtkinlikManager.Guncelle(etkinlikDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else if (etkinlikDTO.SEO != null && etkinlikDTO.SEO == etkinlik.SEO)
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        etkinlikDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel = etkinlik.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        etkinlikDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        etkinlikDTO.GorselEN = etkinlik.GorselEN;
                    }
                    if (resim2 != null)
                    {
                        string uzanti2 = Path.GetExtension(resim2.FileName);
                        string resimAd2 = Guid.NewGuid() + uzanti2;
                        string path2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd2);
                        using (var stream2 = new FileStream(path2, FileMode.Create))
                        {
                            resim2.CopyTo(stream2);
                        }
                        etkinlikDTO.Gorsel2 = resimAd2;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel2 = etkinlik.Gorsel2;
                    }
                    if (resimEN2 != null)
                    {
                        string uzantiEN2 = Path.GetExtension(resimEN2.FileName);
                        string resimAdEN2 = Guid.NewGuid() + uzantiEN2;
                        string pathEN2 = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN2);
                        using (var streamEN2 = new FileStream(pathEN2, FileMode.Create))
                        {
                            resimEN2.CopyTo(streamEN2);
                        }
                        etkinlikDTO.Gorsel2EN = resimAdEN2;
                    }
                    else
                    {
                        etkinlikDTO.Gorsel2EN = etkinlik.Gorsel2EN;
                    }
                    _unitOfWork.EtkinlikManager.Guncelle(etkinlikDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }

        [HttpPost]
        public ActionResult PasifeCevirEtkinlik(string Id)
        {
            var etkinlik = _unitOfWork.EtkinlikManager.Getir(x => x.Id == int.Parse(Id));
            if (etkinlik.Aktif)
            {
                etkinlik.Aktif = false;
            }
            else
            {
                etkinlik.Aktif = true;
            }
            _unitOfWork.EtkinlikManager.Guncelle(etkinlik);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult EtkinlikSil(string Id)
        {
            var entity = _unitOfWork.EtkinlikManager.Getir(x => x.Id == int.Parse(Id));
            int kayitsayisi = _unitOfWork.EtkinlikManager.HepsiniGetir().Count();
            for (int i = entity.SıralamaNo + 1; i < kayitsayisi + 1; i++)
            {
                var entities = _unitOfWork.EtkinlikManager.Getir(x => x.SıralamaNo == i);
                entities.SıralamaNo--;
                _unitOfWork.EtkinlikManager.Guncelle(entities);
            }
            _unitOfWork.EtkinlikManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult Ayarlar()
        {
            var ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ayarlar.EN = dil.EN;
            return View(ayarlar);
        }





        [Authorize]
        [Route("/Panel/Solutions")]
        public IActionResult Duyuru()
        {
            var duyurular = _unitOfWork.DuyuruManager.HepsiniGetir();
            var kategoriler = _unitOfWork.GaleriKategoriManager.HepsiniGetir(x => x.Aktif == true);
            DuyuruVM duyuruVM = new DuyuruVM();
            duyuruVM.Duyurular = duyurular;
            duyuruVM.GaleriKategorileri = kategoriler;
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(duyuruVM);
        }

        public ActionResult DuyuruEkle([FromForm] DuyuruDTO duyuruDTO, IFormFile resim, IFormFile resimEN, IFormFile Anaresim, IFormFile AnaresimEN)
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (duyuruDTO.SEOEN != null && !(_unitOfWork.DuyuruManager.SEOKontrolEN(duyuruDTO.SEOEN)))
                {
                    if (duyuruDTO.SEO != null && !(_unitOfWork.DuyuruManager.SEOKontrol(duyuruDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            duyuruDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            duyuruDTO.Gorsel = "null.png";
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            duyuruDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            duyuruDTO.GorselEN = "null.png";
                        }
                        if (Anaresim != null)
                        {
                            string Anauzanti = Path.GetExtension(Anaresim.FileName);
                            string AnaresimAd = Guid.NewGuid() + Anauzanti;
                            string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                            using (var Anastream = new FileStream(Anapath, FileMode.Create))
                            {
                                Anaresim.CopyTo(Anastream);
                            }
                            duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorsel = "null.png";
                        }
                        if (AnaresimEN != null)
                        {
                            string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                            string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                            string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                            using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                            {
                                AnaresimEN.CopyTo(AnastreamEN);
                            }
                            duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorselEN = "null.png";
                        }
                        _unitOfWork.DuyuruManager.Ekle(duyuruDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (duyuruDTO.SEO != null && !(_unitOfWork.DuyuruManager.SEOKontrol(duyuruDTO.SEO)))
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        duyuruDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        duyuruDTO.Gorsel = "null.png";
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        duyuruDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        duyuruDTO.GorselEN = "null.png";
                    }
                    if (Anaresim != null)
                    {
                        string Anauzanti = Path.GetExtension(Anaresim.FileName);
                        string AnaresimAd = Guid.NewGuid() + Anauzanti;
                        string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                        using (var Anastream = new FileStream(Anapath, FileMode.Create))
                        {
                            Anaresim.CopyTo(Anastream);
                        }
                        duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                    }
                    else
                    {
                        duyuruDTO.AnaSayfaGorsel = "null.png";
                    }
                    if (AnaresimEN != null)
                    {
                        string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                        string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                        string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                        using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                        {
                            AnaresimEN.CopyTo(AnastreamEN);
                        }
                        duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                    }
                    else
                    {
                        duyuruDTO.AnaSayfaGorselEN = "null.png";
                    }
                    _unitOfWork.DuyuruManager.Ekle(duyuruDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }


        public JsonResult DuyuruGuncelle([FromForm] string Id)
        {
            var duyuru = _unitOfWork.DuyuruManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = duyuru.Isim,
                gorsel = duyuru.Gorsel,
                isimEN = duyuru.IsimEN,
                gorselEN = duyuru.GorselEN,
                seo = duyuru.SEO,
                seoEN = duyuru.SEOEN,
                detay = duyuru.Detay,
                detayEN = duyuru.DetayEN,
                aktif = duyuru.Aktif,
                detay2 = duyuru.Detay2,
                detay2EN = duyuru.Detay2EN,
                galerikategoriId = duyuru.GaleriKategoriId,
                gorsel2 = duyuru.AnaSayfaGorsel,
                gorsel2EN = duyuru.AnaSayfaGorselEN,
                anaBaslik = duyuru.AnaSayfaBaslik,
                anaBaslikEN = duyuru.AnaSayfaBaslikEN
            });
        }

        public ActionResult DuyuruGuncel([FromForm] DuyuruDTO duyuruDTO, IFormFile resim, IFormFile resimEN, IFormFile Anaresim, IFormFile AnaresimEN)
        {
            var duyuru = _unitOfWork.DuyuruManager.Getir(x => x.Id == duyuruDTO.Id);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (duyuruDTO.SEOEN != null && !(_unitOfWork.DuyuruManager.SEOKontrolEN(duyuruDTO.SEOEN)))
                {
                    if (duyuruDTO.SEO != null && !(_unitOfWork.DuyuruManager.SEOKontrol(duyuruDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            duyuruDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            duyuruDTO.Gorsel = duyuru.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            duyuruDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            duyuruDTO.GorselEN = duyuru.GorselEN;
                        }
                        if (Anaresim != null)
                        {
                            string Anauzanti = Path.GetExtension(Anaresim.FileName);
                            string AnaresimAd = Guid.NewGuid() + Anauzanti;
                            string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                            using (var Anastream = new FileStream(Anapath, FileMode.Create))
                            {
                                Anaresim.CopyTo(Anastream);
                            }
                            duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorsel = duyuru.AnaSayfaGorsel;
                        }
                        if (AnaresimEN != null)
                        {
                            string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                            string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                            string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                            using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                            {
                                AnaresimEN.CopyTo(AnastreamEN);
                            }
                            duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorselEN = duyuru.AnaSayfaGorselEN;
                        }
                        _unitOfWork.DuyuruManager.Guncelle(duyuruDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (duyuruDTO.SEO != null && duyuruDTO.SEO == duyuru.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            duyuruDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            duyuruDTO.Gorsel = duyuru.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            duyuruDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            duyuruDTO.GorselEN = duyuru.GorselEN;
                        }
                        if (Anaresim != null)
                        {
                            string Anauzanti = Path.GetExtension(Anaresim.FileName);
                            string AnaresimAd = Guid.NewGuid() + Anauzanti;
                            string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                            using (var Anastream = new FileStream(Anapath, FileMode.Create))
                            {
                                Anaresim.CopyTo(Anastream);
                            }
                            duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorsel = duyuru.AnaSayfaGorsel;
                        }
                        if (AnaresimEN != null)
                        {
                            string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                            string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                            string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                            using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                            {
                                AnaresimEN.CopyTo(AnastreamEN);
                            }
                            duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorselEN = duyuru.AnaSayfaGorselEN;
                        }
                        _unitOfWork.DuyuruManager.Guncelle(duyuruDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else if (duyuruDTO.SEOEN != null && duyuruDTO.SEOEN == duyuru.SEOEN)
                {
                    if (duyuruDTO.SEO != null && !(_unitOfWork.DuyuruManager.SEOKontrol(duyuruDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            duyuruDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            duyuruDTO.Gorsel = duyuru.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            duyuruDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            duyuruDTO.GorselEN = duyuru.GorselEN;
                        }
                        if (Anaresim != null)
                        {
                            string Anauzanti = Path.GetExtension(Anaresim.FileName);
                            string AnaresimAd = Guid.NewGuid() + Anauzanti;
                            string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                            using (var Anastream = new FileStream(Anapath, FileMode.Create))
                            {
                                Anaresim.CopyTo(Anastream);
                            }
                            duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorsel = duyuru.AnaSayfaGorsel;
                        }
                        if (AnaresimEN != null)
                        {
                            string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                            string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                            string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                            using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                            {
                                AnaresimEN.CopyTo(AnastreamEN);
                            }
                            duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorselEN = duyuru.AnaSayfaGorselEN;
                        }
                        _unitOfWork.DuyuruManager.Guncelle(duyuruDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (duyuruDTO.SEO != null && duyuruDTO.SEO == duyuru.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            duyuruDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            duyuruDTO.Gorsel = duyuru.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            duyuruDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            duyuruDTO.GorselEN = duyuru.GorselEN;
                        }
                        if (Anaresim != null)
                        {
                            string Anauzanti = Path.GetExtension(Anaresim.FileName);
                            string AnaresimAd = Guid.NewGuid() + Anauzanti;
                            string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                            using (var Anastream = new FileStream(Anapath, FileMode.Create))
                            {
                                Anaresim.CopyTo(Anastream);
                            }
                            duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorsel = duyuru.AnaSayfaGorsel;
                        }
                        if (AnaresimEN != null)
                        {
                            string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                            string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                            string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                            using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                            {
                                AnaresimEN.CopyTo(AnastreamEN);
                            }
                            duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                        }
                        else
                        {
                            duyuruDTO.AnaSayfaGorselEN = duyuru.AnaSayfaGorselEN;
                        }
                        _unitOfWork.DuyuruManager.Guncelle(duyuruDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (duyuruDTO.SEO != null && !(_unitOfWork.DuyuruManager.SEOKontrol(duyuruDTO.SEO)))
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        duyuruDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        duyuruDTO.Gorsel = duyuru.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        duyuruDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        duyuruDTO.GorselEN = duyuru.GorselEN;
                    }
                    if (Anaresim != null)
                    {
                        string Anauzanti = Path.GetExtension(Anaresim.FileName);
                        string AnaresimAd = Guid.NewGuid() + Anauzanti;
                        string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                        using (var Anastream = new FileStream(Anapath, FileMode.Create))
                        {
                            Anaresim.CopyTo(Anastream);
                        }
                        duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                    }
                    else
                    {
                        duyuruDTO.AnaSayfaGorsel = duyuru.AnaSayfaGorsel;
                    }
                    if (AnaresimEN != null)
                    {
                        string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                        string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                        string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                        using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                        {
                            AnaresimEN.CopyTo(AnastreamEN);
                        }
                        duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                    }
                    else
                    {
                        duyuruDTO.AnaSayfaGorselEN = duyuru.AnaSayfaGorselEN;
                    }
                    _unitOfWork.DuyuruManager.Guncelle(duyuruDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else if (duyuruDTO.SEO != null && duyuruDTO.SEO == duyuru.SEO)
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        duyuruDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        duyuruDTO.Gorsel = duyuru.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        duyuruDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        duyuruDTO.GorselEN = duyuru.GorselEN;
                    }
                    if (Anaresim != null)
                    {
                        string Anauzanti = Path.GetExtension(Anaresim.FileName);
                        string AnaresimAd = Guid.NewGuid() + Anauzanti;
                        string Anapath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAd);
                        using (var Anastream = new FileStream(Anapath, FileMode.Create))
                        {
                            Anaresim.CopyTo(Anastream);
                        }
                        duyuruDTO.AnaSayfaGorsel = AnaresimAd;
                    }
                    else
                    {
                        duyuruDTO.AnaSayfaGorsel = duyuru.AnaSayfaGorsel;
                    }
                    if (AnaresimEN != null)
                    {
                        string AnauzantiEN = Path.GetExtension(AnaresimEN.FileName);
                        string AnaresimAdEN = Guid.NewGuid() + AnauzantiEN;
                        string AnapathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + AnaresimAdEN);
                        using (var AnastreamEN = new FileStream(AnapathEN, FileMode.Create))
                        {
                            AnaresimEN.CopyTo(AnastreamEN);
                        }
                        duyuruDTO.AnaSayfaGorselEN = AnaresimAdEN;
                    }
                    else
                    {
                        duyuruDTO.AnaSayfaGorselEN = duyuru.AnaSayfaGorselEN;
                    }
                    _unitOfWork.DuyuruManager.Guncelle(duyuruDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }


        public ActionResult PasifeCevirDuyuru(string Id)
        {
            var duyuru = _unitOfWork.DuyuruManager.Getir(x => x.Id == int.Parse(Id));
            if (duyuru.Aktif)
            {
                duyuru.Aktif = false;
            }
            else
            {
                duyuru.Aktif = true;
            }
            _unitOfWork.DuyuruManager.Guncelle(duyuru);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult DuyuruSil(string Id)
        {
            _unitOfWork.DuyuruManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult Odalar()
        {
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            var odalar = _unitOfWork.OdaManager.HepsiniGetir();
            ViewBag.kategori = sayfa.UrunKategori.ToString();
            return View(odalar);
        }

        [Authorize]
        public IActionResult OdaEkle()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult OdaEkle(string oda)
        //{
        //    return View();

        //}

        [Authorize]
        public IActionResult OdaGuncelle()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult OdaGuncelle()
        //{
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult PasifeCevirOda(string Id)
        //{

        //    return Json(new
        //    {
        //        success = true,
        //    });
        //}

        //public ActionResult OdaSil(string Id)
        //{

        //    return Json(new
        //    {
        //        success = true,
        //        result = 1
        //    });
        //}


        [Authorize]
        public IActionResult Urunler()
        {
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            var urunler = _unitOfWork.UrunManager.HepsiniGetir();
            ViewBag.kategori = sayfa.UrunKategori.ToString();
            return View(urunler);
        }

        [Authorize]
        public IActionResult UrunEkle()
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            UrunEkleVM urunEkleVM = new UrunEkleVM()
            {
                UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(),
                EN = dil.EN
            };
            ViewBag.kategori = sayfa.UrunKategori.ToString();
            return View(urunEkleVM);
        }

        [HttpPost]
        public IActionResult UrunEkle(UrunEkleVM urunEkleVM)
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN)
            {
                if (urunEkleVM.Urun.SEOEN != null && !(_unitOfWork.UrunManager.SEOKontrolEN(urunEkleVM.Urun.SEOEN)))
                {
                    if (!(_unitOfWork.UrunManager.SEOKontrol(urunEkleVM.Urun.SEO)) && urunEkleVM.Urun.SEO != null)
                    {
                        if (urunEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel = "null.png";
                        }
                        if (urunEkleVM.GorselEN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.GorselEN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.GorselEN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.GorselEN = "null.png";
                        }
                        if (urunEkleVM.Gorsel_2 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2 = "null.png";
                        }
                        if (urunEkleVM.Gorsel_2EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2EN = "null.png";
                        }
                        if (urunEkleVM.Gorsel_3 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3 = "null.png";
                        }
                        if (urunEkleVM.Gorsel_3EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3EN = "null.png";
                        }
                        if (urunEkleVM.Gorsel_4 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4 = "null.png";
                        }
                        if (urunEkleVM.Gorsel_4EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4EN = "null.png";
                        }
                        if (urunEkleVM.Gorsel_5 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5 = "null.png";
                        }
                        if (urunEkleVM.Gorsel_5EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5EN = "null.png";
                        }
                        if (urunEkleVM.Gorsel_6 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6 = "null.png";
                        }
                        if (urunEkleVM.Gorsel_6EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6EN = "null.png";
                        }
                        _unitOfWork.UrunManager.Ekle(urunEkleVM.Urun);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Redirect("Urunler");
                    }
                    else
                    {
                        UrunEkleVM urunEkleVMDTO = new UrunEkleVM()
                        {
                            UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                            Urun = urunEkleVM.Urun,
                            EN = dil.EN
                        };
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(urunEkleVMDTO);
                    }
                }
                else
                {
                    UrunEkleVM urunEkleVMDTO = new UrunEkleVM()
                    {
                        UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                        Urun = urunEkleVM.Urun,
                        EN = dil.EN
                    };
                    ViewData["hata"] = "* Girdiğiniz SEO-EN bilgileri sistemde kayıtlıdır.";
                    return View(urunEkleVMDTO);
                }
            }
            else
            {
                if (!(_unitOfWork.UrunManager.SEOKontrol(urunEkleVM.Urun.SEO)) && urunEkleVM.Urun.SEO != null)
                {
                    if (urunEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel = "null.png";
                    }
                    if (urunEkleVM.GorselEN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.GorselEN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.GorselEN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.GorselEN = "null.png";
                    }
                    if (urunEkleVM.Gorsel_2 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_2.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_2 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_2 = "null.png";
                    }
                    if (urunEkleVM.Gorsel_2EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_2EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_2EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_2EN = "null.png";
                    }
                    if (urunEkleVM.Gorsel_3 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_3.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_3 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_3 = "null.png";
                    }
                    if (urunEkleVM.Gorsel_3EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_3EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_3EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_3EN = "null.png";
                    }
                    if (urunEkleVM.Gorsel_4 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_4.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_4 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_4 = "null.png";
                    }
                    if (urunEkleVM.Gorsel_4EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_4EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_4EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_4EN = "null.png";
                    }
                    if (urunEkleVM.Gorsel_5 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_5.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_5 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_5 = "null.png";
                    }
                    if (urunEkleVM.Gorsel_5EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_5EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_5EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_5EN = "null.png";
                    }
                    if (urunEkleVM.Gorsel_6 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_6.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_6 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_6 = "null.png";
                    }
                    if (urunEkleVM.Gorsel_6EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_6EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_6EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_6EN = "null.png";
                    }
                    _unitOfWork.UrunManager.Ekle(urunEkleVM.Urun);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Redirect("Urunler");
                }
                else
                {
                    UrunEkleVM urunEkleVMDTO = new UrunEkleVM()
                    {
                        UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                        Urun = urunEkleVM.Urun,
                        EN = dil.EN
                    };
                    ViewData["hata"] = "* Girdiğiniz SEO-TR sistemde kayıtlıdır. ";
                    return View(urunEkleVMDTO);
                }
            }

        }

        [Authorize]
        public IActionResult UrunGuncelle(int id)
        {
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            UrunEkleVM urunEkleVM = new UrunEkleVM()
            {
                UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                Urun = _unitOfWork.UrunManager.Getir(x => x.Id == id)
            };
            ViewBag.kategori = sayfa.UrunKategori.ToString();
            return View(urunEkleVM);
        }

        [HttpPost]
        public IActionResult UrunGuncelle(UrunEkleVM urunEkleVM)
        {
            var urun = _unitOfWork.UrunManager.Getir(x => x.Id == urunEkleVM.Urun.Id);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (urunEkleVM.Urun.SEOEN != null && !(_unitOfWork.UrunManager.SEOKontrolEN(urunEkleVM.Urun.SEOEN)))
                {
                    if (urunEkleVM.Urun.SEO != null && !(_unitOfWork.UrunManager.SEOKontrol(urunEkleVM.Urun.SEO)))
                    {
                        if (urunEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel = urun.Gorsel;
                        }
                        if (urunEkleVM.GorselEN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.GorselEN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.GorselEN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.GorselEN = urun.GorselEN;
                        }
                        if (urunEkleVM.Gorsel_2 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                        }
                        if (urunEkleVM.Gorsel_2EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                        }
                        if (urunEkleVM.Gorsel_3 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                        }
                        if (urunEkleVM.Gorsel_3EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                        }
                        if (urunEkleVM.Gorsel_4 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                        }
                        if (urunEkleVM.Gorsel_4EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                        }
                        if (urunEkleVM.Gorsel_5 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                        }
                        if (urunEkleVM.Gorsel_5EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                        }
                        if (urunEkleVM.Gorsel_6 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                        }
                        if (urunEkleVM.Gorsel_6EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;
                        }

                        _unitOfWork.UrunManager.Guncelle(urunEkleVM.Urun);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Urunler");
                    }
                    else if (urunEkleVM.Urun.SEO != null && urunEkleVM.Urun.SEO == urun.SEO)
                    {
                        if (urunEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel = urun.Gorsel;
                        }
                        if (urunEkleVM.GorselEN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.GorselEN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.GorselEN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.GorselEN = urun.GorselEN;
                        }
                        if (urunEkleVM.Gorsel_2 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                        }
                        if (urunEkleVM.Gorsel_2EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                        }
                        if (urunEkleVM.Gorsel_3 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                        }
                        if (urunEkleVM.Gorsel_3EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                        }
                        if (urunEkleVM.Gorsel_4 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                        }
                        if (urunEkleVM.Gorsel_4EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                        }
                        if (urunEkleVM.Gorsel_5 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                        }
                        if (urunEkleVM.Gorsel_5EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                        }
                        if (urunEkleVM.Gorsel_6 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                        }
                        if (urunEkleVM.Gorsel_6EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;
                        }

                        _unitOfWork.UrunManager.Guncelle(urunEkleVM.Urun);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Urunler");
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel = urun.Gorsel;
                        urunEkleVM.Urun.GorselEN = urun.GorselEN;
                        urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                        urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                        urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                        urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                        urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                        urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                        urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                        urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                        urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                        urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;

                        UrunEkleVM urunEkleVMDTO = new UrunEkleVM()
                        {
                            UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                            Urun = urunEkleVM.Urun
                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(urunEkleVMDTO);
                    }


                }
                else if (urunEkleVM.Urun.SEOEN != null && urunEkleVM.Urun.SEOEN == urun.SEOEN)
                {
                    if (urunEkleVM.Urun.SEO != null && !(_unitOfWork.UrunManager.SEOKontrol(urunEkleVM.Urun.SEO)))
                    {
                        if (urunEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel = urun.Gorsel;
                        }
                        if (urunEkleVM.GorselEN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.GorselEN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.GorselEN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.GorselEN = urun.GorselEN;
                        }
                        if (urunEkleVM.Gorsel_2 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                        }
                        if (urunEkleVM.Gorsel_2EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                        }
                        if (urunEkleVM.Gorsel_3 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                        }
                        if (urunEkleVM.Gorsel_3EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                        }
                        if (urunEkleVM.Gorsel_4 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                        }
                        if (urunEkleVM.Gorsel_4EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                        }
                        if (urunEkleVM.Gorsel_5 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                        }
                        if (urunEkleVM.Gorsel_5EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                        }
                        if (urunEkleVM.Gorsel_6 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                        }
                        if (urunEkleVM.Gorsel_6EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;
                        }

                        _unitOfWork.UrunManager.Guncelle(urunEkleVM.Urun);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Urunler");
                    }
                    else if (urunEkleVM.Urun.SEO != null && urunEkleVM.Urun.SEO == urun.SEO)
                    {
                        if (urunEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel = urun.Gorsel;
                        }
                        if (urunEkleVM.GorselEN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.GorselEN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.GorselEN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.GorselEN = urun.GorselEN;
                        }
                        if (urunEkleVM.Gorsel_2 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                        }
                        if (urunEkleVM.Gorsel_2EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_2EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_2EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                        }
                        if (urunEkleVM.Gorsel_3 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                        }
                        if (urunEkleVM.Gorsel_3EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_3EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_3EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                        }
                        if (urunEkleVM.Gorsel_4 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                        }
                        if (urunEkleVM.Gorsel_4EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_4EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_4EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                        }
                        if (urunEkleVM.Gorsel_5 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                        }
                        if (urunEkleVM.Gorsel_5EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_5EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_5EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                        }
                        if (urunEkleVM.Gorsel_6 != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6 = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                        }
                        if (urunEkleVM.Gorsel_6EN != null)
                        {
                            string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                urunEkleVM.Gorsel_6EN.CopyTo(stream);
                            }
                            urunEkleVM.Urun.Gorsel_6EN = resimAd;
                        }
                        else
                        {
                            urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;
                        }

                        _unitOfWork.UrunManager.Guncelle(urunEkleVM.Urun);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Urunler");
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel = urun.Gorsel;
                        urunEkleVM.Urun.GorselEN = urun.GorselEN;
                        urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                        urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                        urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                        urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                        urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                        urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                        urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                        urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                        urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                        urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;

                        UrunEkleVM urunEkleVMDTO = new UrunEkleVM()
                        {
                            UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                            Urun = urunEkleVM.Urun
                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(urunEkleVMDTO);
                    }
                }
                else
                {
                    urunEkleVM.Urun.Gorsel = urun.Gorsel;
                    urunEkleVM.Urun.GorselEN = urun.GorselEN;
                    urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                    urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                    urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                    urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                    urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                    urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                    urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                    urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                    urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                    urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;

                    UrunEkleVM urunEkleVMDTO = new UrunEkleVM()
                    {
                        UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                        Urun = urunEkleVM.Urun
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-EN bilgileri sistemde kayıtlıdır.";
                    return View(urunEkleVMDTO);
                }
            }
            else
            {
                if (urunEkleVM.Urun.SEO != null && !(_unitOfWork.UrunManager.SEOKontrol(urunEkleVM.Urun.SEO)))
                {
                    if (urunEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel = urun.Gorsel;
                    }
                    if (urunEkleVM.GorselEN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.GorselEN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.GorselEN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.GorselEN = urun.GorselEN;
                    }
                    if (urunEkleVM.Gorsel_2 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_2.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_2 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                    }
                    if (urunEkleVM.Gorsel_2EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_2EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_2EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                    }
                    if (urunEkleVM.Gorsel_3 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_3.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_3 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                    }
                    if (urunEkleVM.Gorsel_3EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_3EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_3EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                    }
                    if (urunEkleVM.Gorsel_4 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_4.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_4 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                    }
                    if (urunEkleVM.Gorsel_4EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_4EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_4EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                    }
                    if (urunEkleVM.Gorsel_5 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_5.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_5 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                    }
                    if (urunEkleVM.Gorsel_5EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_5EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_5EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                    }
                    if (urunEkleVM.Gorsel_6 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_6.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_6 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                    }
                    if (urunEkleVM.Gorsel_6EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_6EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_6EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;
                    }

                    _unitOfWork.UrunManager.Guncelle(urunEkleVM.Urun);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction("Urunler");
                }
                else if (urunEkleVM.Urun.SEO != null && urunEkleVM.Urun.SEO == urun.SEO)
                {
                    if (urunEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel = urun.Gorsel;
                    }
                    if (urunEkleVM.GorselEN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.GorselEN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.GorselEN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.GorselEN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.GorselEN = urun.GorselEN;
                    }
                    if (urunEkleVM.Gorsel_2 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_2.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_2 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                    }
                    if (urunEkleVM.Gorsel_2EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_2EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_2EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_2EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                    }
                    if (urunEkleVM.Gorsel_3 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_3.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_3 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                    }
                    if (urunEkleVM.Gorsel_3EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_3EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_3EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_3EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                    }
                    if (urunEkleVM.Gorsel_4 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_4.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_4 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                    }
                    if (urunEkleVM.Gorsel_4EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_4EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_4EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_4EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                    }
                    if (urunEkleVM.Gorsel_5 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_5.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_5 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                    }
                    if (urunEkleVM.Gorsel_5EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_5EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_5EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_5EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                    }
                    if (urunEkleVM.Gorsel_6 != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_6.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_6 = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                    }
                    if (urunEkleVM.Gorsel_6EN != null)
                    {
                        string uzanti = Path.GetExtension(urunEkleVM.Gorsel_6EN.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            urunEkleVM.Gorsel_6EN.CopyTo(stream);
                        }
                        urunEkleVM.Urun.Gorsel_6EN = resimAd;
                    }
                    else
                    {
                        urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;
                    }

                    _unitOfWork.UrunManager.Guncelle(urunEkleVM.Urun);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction("Urunler");
                }
                else
                {
                    urunEkleVM.Urun.Gorsel = urun.Gorsel;
                    urunEkleVM.Urun.GorselEN = urun.GorselEN;
                    urunEkleVM.Urun.Gorsel_2 = urun.Gorsel_2;
                    urunEkleVM.Urun.Gorsel_2EN = urun.Gorsel_2EN;
                    urunEkleVM.Urun.Gorsel_3 = urun.Gorsel_3;
                    urunEkleVM.Urun.Gorsel_3EN = urun.Gorsel_3EN;
                    urunEkleVM.Urun.Gorsel_4 = urun.Gorsel_4;
                    urunEkleVM.Urun.Gorsel_4EN = urun.Gorsel_4EN;
                    urunEkleVM.Urun.Gorsel_5 = urun.Gorsel_5;
                    urunEkleVM.Urun.Gorsel_5EN = urun.Gorsel_5EN;
                    urunEkleVM.Urun.Gorsel_6 = urun.Gorsel_6;
                    urunEkleVM.Urun.Gorsel_6EN = urun.Gorsel_6EN;

                    UrunEkleVM urunEkleVMDTO = new UrunEkleVM()
                    {
                        UrunKategorileri = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.Aktif),
                        Urun = urunEkleVM.Urun
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                    return View(urunEkleVMDTO);
                }
            }
        }
        [HttpPost]
        public ActionResult PasifeCevirUrun(string Id)
        {
            var urun = _unitOfWork.UrunManager.Getir(x => x.Id == int.Parse(Id));
            if (urun.Aktif)
            {
                urun.Aktif = false;
            }
            else
            {
                urun.Aktif = true;
            }
            _unitOfWork.UrunManager.Guncelle(urun);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult UrunSil(string Id)
        {
            _unitOfWork.UrunManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult UrunKategorileri()
        {
            var kategoriler = _unitOfWork.UrunKategoriManager.HepsiniGetir(); ;
            return View(kategoriler);
        }

        [HttpPost]
        public ActionResult UrunKategoriEkle([FromForm] UrunKategoriDTO urunKategoriDTO, IFormFile resim, IFormFile resimEN)
        {
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                urunKategoriDTO.Gorsel = resimAd;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                urunKategoriDTO.GorselEN = resimAdEN;
            }
            _unitOfWork.UrunKategoriManager.Ekle(urunKategoriDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult UrunKategoriGuncelle([FromForm] string Id)
        {
            var kategori = _unitOfWork.UrunKategoriManager.Getir(x => x.Id == int.Parse(Id));
            var altkategoriler = _unitOfWork.UrunKategoriManager.HepsiniGetir(x => x.UstKategoriId == kategori.Id);

            return Json(new
            {
                success = true,
                isim = kategori.Isim,
                gorsel = kategori.Gorsel,
                isimEN = kategori.IsimEN,
                gorselEN = kategori.GorselEN,
                SEO = kategori.SEO,
                SEOEN = kategori.SEOEN,
                ustkategoriId = kategori.UstKategoriId
            });
        }

        public ActionResult UrunKategoriGuncel([FromForm] UrunKategoriDTO urunKategoriDTO, IFormFile resim, IFormFile resimEN)
        {
            var urunkategori = _unitOfWork.UrunKategoriManager.Getir(x => x.Id == urunKategoriDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                urunKategoriDTO.Gorsel = resimAd;
            }
            else
            {
                urunKategoriDTO.Gorsel = urunkategori.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                urunKategoriDTO.GorselEN = resimAdEN;
            }
            else
            {
                urunKategoriDTO.GorselEN = urunkategori.GorselEN;
            }
            _unitOfWork.UrunKategoriManager.Guncelle(urunKategoriDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirUrunKategori(string Id)
        {
            var kategori = _unitOfWork.UrunKategoriManager.Getir(x => x.Id == int.Parse(Id));
            if (kategori.Aktif)
            {
                kategori.Aktif = false;
            }
            else
            {
                kategori.Aktif = true;
            }
            _unitOfWork.UrunKategoriManager.Guncelle(kategori);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult UrunKategoriSil(string Id)
        {
            _unitOfWork.UrunKategoriManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult Popup()
        {
            var popuplar = _unitOfWork.PopupManager.HepsiniGetir();
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(popuplar);
        }

        public ActionResult PopupGuncelle([FromForm] string Id)
        {
            var popup = _unitOfWork.PopupManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = popup.Isim,
                gorsel = popup.Gorsel,
                isimEN = popup.IsimEN,
                gorselEN = popup.GorselEN,
                link = popup.Link,
                linkEN = popup.LinkEN
            });
        }

        public ActionResult PopupGuncel([FromForm] PopupDTO popupDTO, IFormFile resim, IFormFile resimEN)
        {
            var popup = _unitOfWork.PopupManager.Getir(x => x.Id == popupDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                popupDTO.Gorsel = resimAd;
            }
            else
            {
                popupDTO.Gorsel = popup.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                popupDTO.GorselEN = resimAdEN;
            }
            else
            {
                popupDTO.GorselEN = popup.GorselEN;
            }
            _unitOfWork.PopupManager.Guncelle(popupDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult PasifeCevirPopup(string Id)
        {
            var popup = _unitOfWork.PopupManager.Getir(x => x.Id == int.Parse(Id));
            if (popup.Aktif)
            {
                popup.Aktif = false;
            }
            else
            {
                popup.Aktif = true;
            }
            _unitOfWork.PopupManager.Guncelle(popup);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }


        [Authorize]
        public IActionResult Slider()
        {
            var sliderlar = _unitOfWork.SliderManager.HepsiniGetir();
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(sliderlar);
        }

        [HttpPost]
        public ActionResult SliderEkle([FromForm] SliderDTO sliderDTO, IFormFile resim, IFormFile resimEN)
        {
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                sliderDTO.Gorsel = resimAd;
            }
            else
            {
                sliderDTO.Gorsel = "null.png";
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                sliderDTO.GorselEN = resimAdEN;
            }
            else
            {
                sliderDTO.GorselEN = "null.png";
            }
            var slidersayisi = _unitOfWork.SliderManager.HepsiniGetir().Count();
            sliderDTO.SıraNo = slidersayisi + 1;
            _unitOfWork.SliderManager.Ekle(sliderDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult SliderGuncelle([FromForm] string Id)
        {
            var slider = _unitOfWork.SliderManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = slider.Isim,
                gorsel = slider.Gorsel,
                isimEN = slider.IsimEN,
                gorselEN = slider.GorselEN,
                link = slider.Link,
                linkEN = slider.LinkEN,
                sırano = slider.SıraNo,
                linkbaslik = slider.LinkBaslik,
                linkbaslikEN = slider.LinkBaslikEN,
                aciklama = slider.Aciklama,
                aciklamaEN = slider.AciklamaEN,
                aktif = slider.Aktif
            });
        }

        public ActionResult SliderGuncel([FromForm] SliderDTO sliderDTO, IFormFile resim, IFormFile resimEN)
        {
            var slider = _unitOfWork.SliderManager.Getir(x => x.Id == sliderDTO.Id);
            if (resim != null)
            {
                string uzanti = Path.GetExtension(resim.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    resim.CopyTo(stream);
                }
                sliderDTO.Gorsel = resimAd;
            }
            else
            {
                sliderDTO.Gorsel = slider.Gorsel;
            }
            if (resimEN != null)
            {
                string uzantiEN = Path.GetExtension(resimEN.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    resimEN.CopyTo(streamEN);
                }
                sliderDTO.GorselEN = resimAdEN;
            }
            else
            {
                sliderDTO.GorselEN = slider.GorselEN;
            }
            _unitOfWork.SliderManager.Guncelle(sliderDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirSlider(string Id)
        {
            var slider = _unitOfWork.SliderManager.Getir(x => x.Id == int.Parse(Id));
            if (slider.Aktif)
            {
                slider.Aktif = false;
            }
            else
            {
                slider.Aktif = true;
            }
            _unitOfWork.SliderManager.Guncelle(slider);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult SliderSil(string Id)
        {
            var entity = _unitOfWork.SliderManager.Getir(x => x.Id == int.Parse(Id));
            int kayitsayisi = _unitOfWork.SliderManager.HepsiniGetir().Count();
            for (int i = entity.SıraNo + 1; i < kayitsayisi + 1; i++)
            {
                var entities = _unitOfWork.SliderManager.Getir(x => x.SıraNo == i);
                entities.SıraNo--;
                _unitOfWork.SliderManager.Guncelle(entities);
            }
            _unitOfWork.SliderManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Route("/Panel/Corporate")]
        [Authorize]
        public IActionResult BlogKategori()
        {
            var kategoriler = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.Id != 1);
            return View(kategoriler);
        }

        [Route("/Panel/Team")]
        [Authorize]
        public IActionResult Team()
        {
            var model = _unitOfWork.TeamManager.GetAllTeam();
            return View(model);
        }

        [Route("/Panel/Membership")]
        [Authorize]
        public IActionResult Membership()
        {
            var model = _unitOfWork.MembershipManager.GetAll();
            return View(model);
        }

        [Route("/Panel/MembershipAddOrUpdate")]
        [Authorize]
        public IActionResult MembershipAddOrUpdate(int id)
        {
            var model = _unitOfWork.MembershipManager.Get(x => x.Id == id);
            return View(model);
        }

        [HttpPost]
        [Route("/Panel/MembershipAddOrUpdate")]
        [Authorize]
        public IActionResult MembershipAddOrUpdate(Membership model)
        {
            if ((model.Image == null && model.ImageFile == null) ||
                      (model.ImageFile != null &&
                      !string.Equals(model.ImageFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Resim dosyası seçiniz";
                return View(model);
            }


            if (model.ImageFile != null)
            {
                string uzanti = Path.GetExtension(model.ImageFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.ImageFile.CopyTo(stream);
                }
                model.Image = resimAd;
            }

            _unitOfWork.MembershipManager.AddOrUpdate(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();



            return Redirect("/Panel/Membership");
        }

        [Route("/Panel/MembershipDelete")]
        [Authorize]
        public IActionResult MembershipDelete(int Id)
        {
            _unitOfWork.MembershipManager.Delete(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Membership");
        }

        [Route("/Panel/MembershipUp")]
        [Authorize]
        public IActionResult MembershipUp(int Id)
        {
            _unitOfWork.MembershipManager.UpMembership(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Membership");
        }

        [Route("/Panel/MembershipDown")]
        [Authorize]
        public IActionResult MembershipDown(int Id)
        {
            _unitOfWork.MembershipManager.DownMembership(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Membership");
        }


        [Route("/Panel/Category")]
        [Authorize]
        public IActionResult TeamCategory(int Id)
        {
            var model = _unitOfWork.TeamManager.GetCategory(x => x.Id == Id);


            return View(model);
        }

        [HttpPost]
        [Route("/Panel/Category")]
        [Authorize]
        public IActionResult TeamCategory(TeamCategory model)
        {
            _unitOfWork.TeamManager.AddOrUpdateCategory(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Redirect("/Panel/Team");
        }

        [Route("/Panel/CategoryDelete")]
        [Authorize]
        public IActionResult TeamCategoryDelete(int Id)
        {
            _unitOfWork.TeamManager.DeleteCategory(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Team");
        }

        [Route("/Panel/CategoryUp")]
        [Authorize]
        public IActionResult TeamCategoryUp(int Id)
        {
            _unitOfWork.TeamManager.UpCategory(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Team");
        }

        [Route("/Panel/CategoryDown")]
        [Authorize]
        public IActionResult TeamCategoryDown(int Id)
        {
            _unitOfWork.TeamManager.DownCategory(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Team");
        }





        [Route("/Panel/TeamCategoryListUp")]
        [Authorize]
        public IActionResult TeamCategoryListUp(int Id)
        {
            _unitOfWork.TeamManager.UpTeamList(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Team");
        }

        [Route("/Panel/TeamCategoryListDown")]
        [Authorize]
        public IActionResult TeamCategoryListDown(int Id)
        {
            _unitOfWork.TeamManager.DownTeamList(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Team");
        }







        [HttpGet]
        [Route("/Panel/CategoryTeamDelete")]
        [Authorize]
        public IActionResult TeamCategoryListDelete(int id)
        {
            _unitOfWork.TeamManager.DeleteTeamList(x => x.Id == id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Redirect("/Panel/Team");
        }


        [HttpGet]
        [Route("/Panel/CategoryTeam")]
        [Authorize]
        public IActionResult TeamCategoryList(int Id, int teamCategoryId)
        {
            var model = _unitOfWork.TeamManager.GetCategoryList(x => x.Id == Id);
            return View(model);
        }

        [HttpPost]
        [Route("/Panel/CategoryTeam")]
        [Authorize]
        public IActionResult TeamCategoryList(TeamList model)
        {
            if ((model.Image == null && model.ImageFile == null) ||
                      (model.ImageFile != null &&
                      !string.Equals(model.ImageFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Resim dosyası seçiniz";
                return View(model);
            }


            if (model.ImageFile != null)
            {
                string uzanti = Path.GetExtension(model.ImageFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.ImageFile.CopyTo(stream);
                }
                model.Image = resimAd;
            }

            _unitOfWork.TeamManager.AddOrUpdateTeamList(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();



            return Redirect("/Panel/Team");
        }


        [HttpPost]
        public ActionResult BlogKategoriEkle([FromForm] BlogKategoriDTO blogKategoriDTO, IFormFile resim, IFormFile resimEN)
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (blogKategoriDTO.SEOEN != null && !(_unitOfWork.BlogKategoriManager.SEOKontrolEN(blogKategoriDTO.SEOEN)))
                {
                    if (blogKategoriDTO.SEO != null && !(_unitOfWork.BlogKategoriManager.SEOKontrol(blogKategoriDTO.SEO)))
                    {
                        var kategorisayisi = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.SıralamaNo != 0).Count();
                        blogKategoriDTO.SıralamaNo = kategorisayisi + 1;
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            blogKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogKategoriDTO.Gorsel = "null.png";
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            blogKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogKategoriDTO.GorselEN = "null.png";
                        }
                        _unitOfWork.BlogKategoriManager.Ekle(blogKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (blogKategoriDTO.SEO != null && !(_unitOfWork.BlogKategoriManager.SEOKontrol(blogKategoriDTO.SEO)))
                {
                    var kategorisayisi = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.SıralamaNo != 0).Count();
                    blogKategoriDTO.SıralamaNo = kategorisayisi + 1;
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        blogKategoriDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        blogKategoriDTO.Gorsel = "null.png";
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        blogKategoriDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        blogKategoriDTO.GorselEN = "null.png";
                    }
                    _unitOfWork.BlogKategoriManager.Ekle(blogKategoriDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }


        public ActionResult BlogKategoriGuncelle([FromForm] string Id)
        {
            var kategori = _unitOfWork.BlogKategoriManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = kategori.Isim,
                gorsel = kategori.Gorsel,
                isimEN = kategori.IsimEN,
                gorselEN = kategori.GorselEN,
                seo = kategori.SEO,
                seoEN = kategori.SEOEN,
                aktif = kategori.Aktif,
                sıralamaNo = kategori.SıralamaNo,
                detay = kategori.Detay,
                detayEN = kategori.DetayEN
            });
        }

        public ActionResult BlogKategoriGuncel([FromForm] BlogKategoriDTO blogKategoriDTO, IFormFile resim, IFormFile resimEN)
        {
            var blogkategori = _unitOfWork.BlogKategoriManager.Getir(x => x.Id == blogKategoriDTO.Id);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (blogKategoriDTO.SEOEN != null && !(_unitOfWork.BlogKategoriManager.SEOKontrolEN(blogKategoriDTO.SEOEN)))
                {
                    if (blogKategoriDTO.SEO != null && !(_unitOfWork.BlogKategoriManager.SEOKontrol(blogKategoriDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            blogKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogKategoriDTO.Gorsel = blogkategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            blogKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogKategoriDTO.GorselEN = blogkategori.GorselEN;
                        }
                        _unitOfWork.BlogKategoriManager.Guncelle(blogKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (blogKategoriDTO.SEO != null && blogKategoriDTO.SEO == blogkategori.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            blogKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogKategoriDTO.Gorsel = blogkategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            blogKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogKategoriDTO.GorselEN = blogkategori.GorselEN;
                        }

                        _unitOfWork.BlogKategoriManager.Guncelle(blogKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }


                }
                else if (blogKategoriDTO.SEOEN != null && blogKategoriDTO.SEOEN == blogkategori.SEOEN)
                {
                    if (blogKategoriDTO.SEO != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrol(blogKategoriDTO.SEO)))
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            blogKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogKategoriDTO.Gorsel = blogkategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            blogKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogKategoriDTO.GorselEN = blogkategori.GorselEN;
                        }

                        _unitOfWork.BlogKategoriManager.Guncelle(blogKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });

                    }
                    else if (blogKategoriDTO.SEO != null && blogKategoriDTO.SEO == blogkategori.SEO)
                    {
                        if (resim != null)
                        {
                            string uzanti = Path.GetExtension(resim.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                resim.CopyTo(stream);
                            }
                            blogKategoriDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogKategoriDTO.Gorsel = blogkategori.Gorsel;
                        }
                        if (resimEN != null)
                        {
                            string uzantiEN = Path.GetExtension(resimEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                resimEN.CopyTo(streamEN);
                            }
                            blogKategoriDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogKategoriDTO.GorselEN = blogkategori.GorselEN;
                        }

                        _unitOfWork.BlogKategoriManager.Guncelle(blogKategoriDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new
                        {
                            success = true,
                            result = 1
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = true,
                            result = 2
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 3
                    });
                }
            }
            else
            {
                if (blogKategoriDTO.SEO != null && !(_unitOfWork.GaleriKategoriManager.SEOKontrol(blogKategoriDTO.SEO)))
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        blogKategoriDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        blogKategoriDTO.Gorsel = blogkategori.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        blogKategoriDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        blogKategoriDTO.GorselEN = blogkategori.GorselEN;
                    }

                    _unitOfWork.BlogKategoriManager.Guncelle(blogKategoriDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });

                }
                else if (blogKategoriDTO.SEO != null && blogKategoriDTO.SEO == blogkategori.SEO)
                {
                    if (resim != null)
                    {
                        string uzanti = Path.GetExtension(resim.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            resim.CopyTo(stream);
                        }
                        blogKategoriDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        blogKategoriDTO.Gorsel = blogkategori.Gorsel;
                    }
                    if (resimEN != null)
                    {
                        string uzantiEN = Path.GetExtension(resimEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            resimEN.CopyTo(streamEN);
                        }
                        blogKategoriDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        blogKategoriDTO.GorselEN = blogkategori.GorselEN;
                    }

                    _unitOfWork.BlogKategoriManager.Guncelle(blogKategoriDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }

        [HttpPost]
        public ActionResult PasifeCevirBlogKategori(string Id)
        {
            var entity = _unitOfWork.BlogKategoriManager.Getir(x => x.Id == int.Parse(Id));
            if (entity.Aktif)
            {
                entity.Aktif = false;
            }
            else
            {
                entity.Aktif = true;
            }
            _unitOfWork.BlogKategoriManager.Guncelle(entity);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult BlogKategoriSil(string Id)
        {
            var entity = _unitOfWork.BlogKategoriManager.Getir(x => x.Id == int.Parse(Id));
            int kayitsayisi = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.SıralamaNo != 0).Count();
            for (int i = entity.SıralamaNo + 1; i < kayitsayisi + 1; i++)
            {
                var entities = _unitOfWork.BlogKategoriManager.Getir(x => x.SıralamaNo == i);
                entities.SıralamaNo--;
                _unitOfWork.BlogKategoriManager.Guncelle(entities);
            }
            _unitOfWork.BlogKategoriManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult Iletisim()
        {
            var iletisim = _unitOfWork.IletisimManager.HepsiniGetir();
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(iletisim);
        }

        [HttpPost]
        public ActionResult IletisimEkle([FromForm] IletisimDTO iletisimDTO)
        {

            _unitOfWork.IletisimManager.Ekle(iletisimDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult IletisimGuncelle([FromForm] string Id)
        {
            var iletisim = _unitOfWork.IletisimManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                isim = iletisim.Baslik,
                adres = iletisim.Adres,
                isimEN = iletisim.BaslikEN,
                fax = iletisim.Fax,
                telefon = iletisim.Telefon,
                telefon2 = iletisim.Telefon2,
                email = iletisim.Email,
                haritakod = iletisim.HaritaKod,
                aktif = iletisim.Aktif
            });
        }

        public ActionResult IletisimGuncel([FromForm] IletisimDTO iletisimDTO)
        {

            _unitOfWork.IletisimManager.Guncelle(iletisimDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult PasifeCevirIletisim(string Id)
        {
            var iletisim = _unitOfWork.IletisimManager.Getir(x => x.Id == int.Parse(Id));
            if (iletisim.Aktif)
            {
                iletisim.Aktif = false;
            }
            else
            {
                iletisim.Aktif = true;
            }
            _unitOfWork.IletisimManager.Guncelle(iletisim);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        public ActionResult IletisimSil(string Id)
        {
            _unitOfWork.IletisimManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Authorize]
        public IActionResult Blog()
        {
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            var bloglar = _unitOfWork.BlogManager.HepsiniGetir();
            ViewBag.kategori = sayfa.BlogKategori.ToString();
            return View(bloglar);
        }

        [Authorize]
        public IActionResult BlogEkle()
        {
            BlogEkleVM blogEkleVM = new BlogEkleVM()
            {
                BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.Id != 1)
            };
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            ViewBag.kategori = sayfa.BlogKategori.ToString();
            return View(blogEkleVM);
        }

        [HttpPost]
        public ActionResult PasifeCevirBlog(string Id)
        {
            var blog = _unitOfWork.BlogManager.Getir(x => x.Id == int.Parse(Id));
            if (blog.Aktif)
            {
                blog.Aktif = false;
            }
            else
            {
                blog.Aktif = true;
            }
            _unitOfWork.BlogManager.Guncelle(blog);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }

        [HttpPost]
        public IActionResult BlogEkle(BlogEkleVM blogEkleVM)
        {
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (blogEkleVM.BlogDTO.SEOEN != null && !(_unitOfWork.BlogManager.SEOKontrolEN(blogEkleVM.BlogDTO.SEOEN)))
                {
                    if (blogEkleVM.BlogDTO.SEO != null && !(_unitOfWork.BlogManager.SEOKontrol(blogEkleVM.BlogDTO.SEO)))
                    {
                        if (blogEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                blogEkleVM.Gorsel.CopyTo(stream);
                            }
                            blogEkleVM.BlogDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.Gorsel = "null.png";
                        }
                        if (blogEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                blogEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.GorselEN = "null.png";
                        }
                        _unitOfWork.BlogManager.Ekle(blogEkleVM.BlogDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Redirect("/Panel/Blog");

                    }
                    else
                    {
                        BlogEkleVM blogEkleVMDTO = new BlogEkleVM()
                        {
                            BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(),
                            BlogDTO = blogEkleVM.BlogDTO
                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(blogEkleVMDTO);
                    }


                }
                else
                {
                    BlogEkleVM blogEkleVMDTO = new BlogEkleVM()
                    {
                        BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(),
                        BlogDTO = blogEkleVM.BlogDTO
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-EN bilgileri sistemde kayıtlıdır.";
                    return View(blogEkleVMDTO);
                }
            }
            else
            {
                if (blogEkleVM.BlogDTO.SEO != null && !(_unitOfWork.BlogManager.SEOKontrol(blogEkleVM.BlogDTO.SEO)))
                {
                    if (blogEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            blogEkleVM.Gorsel.CopyTo(stream);
                        }
                        blogEkleVM.BlogDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.Gorsel = "null.png";
                    }
                    if (blogEkleVM.GorselEN != null)
                    {
                        string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            blogEkleVM.GorselEN.CopyTo(streamEN);
                        }
                        blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.GorselEN = "null.png";
                    }
                    _unitOfWork.BlogManager.Ekle(blogEkleVM.BlogDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Redirect("/Panel/Blog");

                }
                else
                {
                    BlogEkleVM blogEkleVMDTO = new BlogEkleVM()
                    {
                        BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(),
                        BlogDTO = blogEkleVM.BlogDTO
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                    return View(blogEkleVMDTO);
                }
            }

        }

        [Authorize]
        public IActionResult BlogGuncelle(int id)
        {
            BlogEkleVM blogEkleVM = new BlogEkleVM()
            {
                BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.Id != 1),
                BlogDTO = _unitOfWork.BlogManager.Getir(x => x.Id == id)
            };
            var sayfa = _unitOfWork.MevcutSayfalarManager.Getir(x => x.Id == 1);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            ViewBag.kategori = sayfa.BlogKategori.ToString();
            return View(blogEkleVM);
        }

        [HttpPost]
        public IActionResult BlogGuncelle(BlogEkleVM blogEkleVM)
        {
            var blog = _unitOfWork.BlogManager.Getir(x => x.Id == blogEkleVM.BlogDTO.Id);
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (blogEkleVM.BlogDTO.SEOEN != null && !(_unitOfWork.BlogManager.SEOKontrolEN(blogEkleVM.BlogDTO.SEOEN)))
                {
                    if (blogEkleVM.BlogDTO.SEO != null && !(_unitOfWork.BlogManager.SEOKontrol(blogEkleVM.BlogDTO.SEO)))
                    {
                        if (blogEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                blogEkleVM.Gorsel.CopyTo(stream);
                            }
                            blogEkleVM.BlogDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                        }
                        if (blogEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                blogEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                        }
                        _unitOfWork.BlogManager.Guncelle(blogEkleVM.BlogDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Blog");

                    }
                    else if (blogEkleVM.BlogDTO.SEO != null && blogEkleVM.BlogDTO.SEO == blog.SEO)
                    {
                        if (blogEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                blogEkleVM.Gorsel.CopyTo(stream);
                            }
                            blogEkleVM.BlogDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                        }
                        if (blogEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                blogEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                        }
                        _unitOfWork.BlogManager.Guncelle(blogEkleVM.BlogDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Blog");
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                        blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                        BlogEkleVM blogEkleVMDTO = new BlogEkleVM()
                        {
                            BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(),
                            BlogDTO = blogEkleVM.BlogDTO
                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(blogEkleVMDTO);
                    }


                }
                else if (blogEkleVM.BlogDTO.SEOEN != null && blogEkleVM.BlogDTO.SEOEN == blog.SEOEN)
                {
                    if (blogEkleVM.BlogDTO.SEO != null && !(_unitOfWork.BlogManager.SEOKontrol(blogEkleVM.BlogDTO.SEO)))
                    {
                        if (blogEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                blogEkleVM.Gorsel.CopyTo(stream);
                            }
                            blogEkleVM.BlogDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                        }
                        if (blogEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                blogEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                        }
                        _unitOfWork.BlogManager.Guncelle(blogEkleVM.BlogDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Blog");

                    }
                    else if (blogEkleVM.BlogDTO.SEO != null && blogEkleVM.BlogDTO.SEO == blog.SEO)
                    {
                        if (blogEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                blogEkleVM.Gorsel.CopyTo(stream);
                            }
                            blogEkleVM.BlogDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                        }
                        if (blogEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                blogEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                        }
                        _unitOfWork.BlogManager.Guncelle(blogEkleVM.BlogDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Blog");
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                        blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                        BlogEkleVM blogEkleVMDTO = new BlogEkleVM()
                        {
                            BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(),
                            BlogDTO = blogEkleVM.BlogDTO
                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(blogEkleVMDTO);
                    }
                }
                else
                {

                    blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                    blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                    BlogEkleVM blogEkleVMDTO = new BlogEkleVM()
                    {
                        BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(),
                        BlogDTO = blogEkleVM.BlogDTO
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-EN bilgileri sistemde kayıtlıdır.";
                    return View(blogEkleVMDTO);
                }
            }
            else
            {
                if (blogEkleVM.BlogDTO.SEO != null && !(_unitOfWork.BlogManager.SEOKontrol(blogEkleVM.BlogDTO.SEO)))
                {
                    if (blogEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            blogEkleVM.Gorsel.CopyTo(stream);
                        }
                        blogEkleVM.BlogDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                    }
                    if (blogEkleVM.GorselEN != null)
                    {
                        string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            blogEkleVM.GorselEN.CopyTo(streamEN);
                        }
                        blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                    }
                    _unitOfWork.BlogManager.Guncelle(blogEkleVM.BlogDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction("Blog");

                }
                else if (blogEkleVM.BlogDTO.SEO != null && blogEkleVM.BlogDTO.SEO == blog.SEO)
                {
                    if (blogEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(blogEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            blogEkleVM.Gorsel.CopyTo(stream);
                        }
                        blogEkleVM.BlogDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                    }
                    if (blogEkleVM.GorselEN != null)
                    {
                        string uzantiEN = Path.GetExtension(blogEkleVM.GorselEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            blogEkleVM.GorselEN.CopyTo(streamEN);
                        }
                        blogEkleVM.BlogDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                    }
                    _unitOfWork.BlogManager.Guncelle(blogEkleVM.BlogDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction("Blog");
                }
                else
                {
                    blogEkleVM.BlogDTO.Gorsel = blog.Gorsel;
                    blogEkleVM.BlogDTO.GorselEN = blog.GorselEN;
                    BlogEkleVM blogEkleVMDTO = new BlogEkleVM()
                    {
                        BlogKategorileri = _unitOfWork.BlogKategoriManager.HepsiniGetir(),
                        BlogDTO = blogEkleVM.BlogDTO
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                    return View(blogEkleVMDTO);
                }
            }


        }



        [Authorize]
        public IActionResult Haberler()
        {
            var haberler = _unitOfWork.HaberManager.HepsiniGetir();
            return View(haberler);
        }

        [Authorize]
        public IActionResult HaberEkle()
        {
            HaberEkleVM haberEkleVM = new HaberEkleVM()
            {
                HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir()
            };
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(haberEkleVM);
        }


        [HttpPost]
        public IActionResult HaberEkle(HaberEkleVM haberEkleVM)
        {

            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (haberEkleVM.HaberDTO.SEOEN != null && !(_unitOfWork.HaberManager.SEOKontrolEN(haberEkleVM.HaberDTO.SEOEN)))
                {
                    if (haberEkleVM.HaberDTO.SEO != null && !(_unitOfWork.HaberManager.SEOKontrol(haberEkleVM.HaberDTO.SEO)))
                    {
                        if (haberEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                haberEkleVM.Gorsel.CopyTo(stream);
                            }
                            haberEkleVM.HaberDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.Gorsel = "null.png";
                        }
                        if (haberEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                haberEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.GorselEN = "null.png";
                        }
                        _unitOfWork.HaberManager.Ekle(haberEkleVM.HaberDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Redirect("Haberler");

                    }
                    else
                    {
                        HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
                        {
                            HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                            HaberDTO = haberEkleVM.HaberDTO,


                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(haberEkleVMDTO);
                    }


                }
                else
                {
                    HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
                    {
                        HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                        HaberDTO = haberEkleVM.HaberDTO,


                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-EN bilgileri sistemde kayıtlıdır.";
                    return View(haberEkleVMDTO);
                }
            }
            else
            {
                if (haberEkleVM.HaberDTO.SEO != null && !(_unitOfWork.HaberManager.SEOKontrol(haberEkleVM.HaberDTO.SEO)))
                {
                    if (haberEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            haberEkleVM.Gorsel.CopyTo(stream);
                        }
                        haberEkleVM.HaberDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.Gorsel = "null.png";
                    }
                    if (haberEkleVM.GorselEN != null)
                    {
                        string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            haberEkleVM.GorselEN.CopyTo(streamEN);
                        }
                        haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.GorselEN = "null.png";
                    }
                    _unitOfWork.HaberManager.Ekle(haberEkleVM.HaberDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Redirect("Haberler");

                }
                else
                {
                    HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
                    {
                        HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                        HaberDTO = haberEkleVM.HaberDTO,


                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                    return View(haberEkleVMDTO);
                }
            }


        }

        [Authorize]
        public IActionResult HaberGuncelle(int id)
        {
            HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
            {
                HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                HaberDTO = _unitOfWork.HaberManager.Getir(x => x.Id == id)
            };
            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            ViewBag.en = dil.EN.ToString();
            return View(haberEkleVMDTO);
        }

        [HttpPost]
        public IActionResult HaberGuncelle(HaberEkleVM haberEkleVM)
        {
            var haber = _unitOfWork.HaberManager.Getir(x => x.Id == haberEkleVM.HaberDTO.Id);


            var dil = _unitOfWork.MevcutDillerManager.Getir(x => x.Id == 1);
            if (dil.EN == true)
            {
                if (haberEkleVM.HaberDTO.SEOEN != null && !(_unitOfWork.HaberManager.SEOKontrolEN(haberEkleVM.HaberDTO.SEOEN)))
                {
                    if (haberEkleVM.HaberDTO.SEO != null && !(_unitOfWork.HaberManager.SEOKontrol(haberEkleVM.HaberDTO.SEO)))
                    {
                        if (haberEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                haberEkleVM.Gorsel.CopyTo(stream);
                            }
                            haberEkleVM.HaberDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                        }
                        if (haberEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                haberEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                        }
                        _unitOfWork.HaberManager.Guncelle(haberEkleVM.HaberDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Haberler");

                    }
                    else if (haberEkleVM.HaberDTO.SEO != null && haberEkleVM.HaberDTO.SEO == haber.SEO)
                    {
                        if (haberEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                haberEkleVM.Gorsel.CopyTo(stream);
                            }
                            haberEkleVM.HaberDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                        }
                        if (haberEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                haberEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                        }
                        _unitOfWork.HaberManager.Guncelle(haberEkleVM.HaberDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Haberler");
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                        haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                        HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
                        {
                            HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                            HaberDTO = haberEkleVM.HaberDTO,


                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(haberEkleVMDTO);
                    }


                }
                else if (haberEkleVM.HaberDTO.SEOEN != null && haberEkleVM.HaberDTO.SEOEN == haber.SEOEN)
                {
                    if (haberEkleVM.HaberDTO.SEO != null && !(_unitOfWork.HaberManager.SEOKontrol(haberEkleVM.HaberDTO.SEO)))
                    {
                        if (haberEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                haberEkleVM.Gorsel.CopyTo(stream);
                            }
                            haberEkleVM.HaberDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                        }
                        if (haberEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                haberEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                        }
                        _unitOfWork.HaberManager.Guncelle(haberEkleVM.HaberDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Haberler");

                    }
                    else if (haberEkleVM.HaberDTO.SEO != null && haberEkleVM.HaberDTO.SEO == haber.SEO)
                    {
                        if (haberEkleVM.Gorsel != null)
                        {
                            string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                            string resimAd = Guid.NewGuid() + uzanti;
                            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                haberEkleVM.Gorsel.CopyTo(stream);
                            }
                            haberEkleVM.HaberDTO.Gorsel = resimAd;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                        }
                        if (haberEkleVM.GorselEN != null)
                        {
                            string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                            string resimAdEN = Guid.NewGuid() + uzantiEN;
                            string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                            using (var streamEN = new FileStream(pathEN, FileMode.Create))
                            {
                                haberEkleVM.GorselEN.CopyTo(streamEN);
                            }
                            haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                        }
                        else
                        {
                            haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                        }
                        _unitOfWork.HaberManager.Guncelle(haberEkleVM.HaberDTO);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Haberler");
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                        haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                        HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
                        {
                            HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                            HaberDTO = haberEkleVM.HaberDTO
                        };
                        ViewBag.en = dil.EN.ToString();
                        ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                        return View(haberEkleVMDTO);
                    }
                }
                else
                {

                    haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                    haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                    HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
                    {
                        HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                        HaberDTO = haberEkleVM.HaberDTO,
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-EN bilgileri sistemde kayıtlıdır.";
                    return View(haberEkleVMDTO);
                }
            }
            else
            {
                if (haberEkleVM.HaberDTO.SEO != null && !(_unitOfWork.HaberManager.SEOKontrol(haberEkleVM.HaberDTO.SEO)))
                {
                    if (haberEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            haberEkleVM.Gorsel.CopyTo(stream);
                        }
                        haberEkleVM.HaberDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                    }
                    if (haberEkleVM.GorselEN != null)
                    {
                        string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            haberEkleVM.GorselEN.CopyTo(streamEN);
                        }
                        haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                    }
                    _unitOfWork.HaberManager.Guncelle(haberEkleVM.HaberDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction("Haberler");

                }
                else if (haberEkleVM.HaberDTO.SEO != null && haberEkleVM.HaberDTO.SEO == haber.SEO)
                {
                    if (haberEkleVM.Gorsel != null)
                    {
                        string uzanti = Path.GetExtension(haberEkleVM.Gorsel.FileName);
                        string resimAd = Guid.NewGuid() + uzanti;
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            haberEkleVM.Gorsel.CopyTo(stream);
                        }
                        haberEkleVM.HaberDTO.Gorsel = resimAd;
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                    }
                    if (haberEkleVM.GorselEN != null)
                    {
                        string uzantiEN = Path.GetExtension(haberEkleVM.GorselEN.FileName);
                        string resimAdEN = Guid.NewGuid() + uzantiEN;
                        string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                        using (var streamEN = new FileStream(pathEN, FileMode.Create))
                        {
                            haberEkleVM.GorselEN.CopyTo(streamEN);
                        }
                        haberEkleVM.HaberDTO.GorselEN = resimAdEN;
                    }
                    else
                    {
                        haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                    }
                    _unitOfWork.HaberManager.Guncelle(haberEkleVM.HaberDTO);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction("Haberler");
                }
                else
                {
                    haberEkleVM.HaberDTO.Gorsel = haber.Gorsel;
                    haberEkleVM.HaberDTO.GorselEN = haber.GorselEN;
                    HaberEkleVM haberEkleVMDTO = new HaberEkleVM()
                    {
                        HaberKategorileri = _unitOfWork.HaberKategoriManager.HepsiniGetir(),
                        HaberDTO = haberEkleVM.HaberDTO
                    };
                    ViewBag.en = dil.EN.ToString();
                    ViewData["hata"] = "* Girdiğiniz SEO-TR bilgileri sistemde kayıtlıdır.";
                    return View(haberEkleVMDTO);
                }
            }
        }


        [Authorize]
        public IActionResult Yoneticiler()
        {
            var yoneticiler = _unitOfWork.YoneticiManager.HepsiniGetir();
            return View(yoneticiler);
        }
        [HttpPost]
        public ActionResult KullaniciEkle([FromForm] YoneticiDTO yoneticiDTO)
        {
            _unitOfWork.YoneticiManager.Ekle(yoneticiDTO);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public ActionResult KullaniciGetir([FromForm] string Id)
        {
            var kullanici = _unitOfWork.YoneticiManager.Getir(x => x.Id == int.Parse(Id));
            return Json(new
            {
                success = true,
                ad = kullanici.Ad,
                soyad = kullanici.Soyad,
                email = kullanici.Email,
                kullaniciadi = kullanici.KullaniciAdi,
                sifre = kullanici.Sifre
            });
        }

        public ActionResult KullaniciGuncelle([FromForm] YoneticiDTO yoneticiDTO)
        {
            var kullanici = _unitOfWork.YoneticiManager.Getir(x => x.Id == yoneticiDTO.Id);
            if (yoneticiDTO.KullaniciAdi == kullanici.KullaniciAdi)
            {
                _unitOfWork.YoneticiManager.Guncelle(yoneticiDTO);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });
            }
            else
            {
                if (!(_unitOfWork.YoneticiManager.KullaniciAdiKontrol(yoneticiDTO.KullaniciAdi)))
                {
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = true,
                        result = 2
                    });
                }
            }
        }

        public IActionResult GirisYap()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GirisYap(GirisDTO girisDTO)
        {
            YoneticiDTO kullanici = _unitOfWork.YoneticiManager.Getir(x => x.KullaniciAdi == girisDTO.KullaniciAdi);
            if (kullanici != null)
            {
                if (kullanici.Sifre == girisDTO.Sifre)
                {
                    var userClaims = new List<Claim>()
                    {
                    new Claim("Id", kullanici.Id.ToString()),
                    };
                    var grandmaIdentity = new ClaimsIdentity(userClaims, "Login");
                    var userPrincipal = new ClaimsPrincipal(new[] { grandmaIdentity });
                    var authProperties = new AuthenticationProperties()
                    {
                        IsPersistent = girisDTO.BeniHatirla
                    };
                    await HttpContext.SignInAsync(userPrincipal, authProperties);
                    return Redirect("Ayarlar");
                }
                else
                {
                    ViewData["hata"] = "E-Posta veya şifre hatalı.";
                    return View(girisDTO);
                }
            }
            else
            {
                ViewData["hata"] = "E-Posta veya şifre hatalı.";
                return View(girisDTO);
            }

        }


        public ActionResult KullaniciSil(string Id)
        {
            _unitOfWork.YoneticiManager.KaliciSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        public ActionResult HaberGorselSil([FromForm] int Id, string dil)
        {
            var haber = _unitOfWork.HaberManager.Getir(x => x.Id == Id);
            if (dil == "tr")
            {
                haber.Gorsel = "null.png";
            }
            else
            {
                haber.GorselEN = "null.png";
            }
            _unitOfWork.HaberManager.Guncelle(haber);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }
        public ActionResult UrunGorselSil([FromForm] int Id, string dil, int sayi)
        {
            var urun = _unitOfWork.UrunManager.Getir(x => x.Id == Id);
            if (dil == "tr")
            {
                if (sayi == 1)
                {
                    urun.Gorsel = "null.png";
                }
                else if (sayi == 2)
                {
                    urun.Gorsel_2 = "null.png";
                }
                else if (sayi == 3)
                {
                    urun.Gorsel_3 = "null.png";
                }
                else if (sayi == 4)
                {
                    urun.Gorsel_4 = "null.png";
                }
                else if (sayi == 5)
                {
                    urun.Gorsel_5 = "null.png";
                }
                else if (sayi == 6)
                {
                    urun.Gorsel_6 = "null.png";
                };

            }
            else
            {
                if (sayi == 1)
                {
                    urun.GorselEN = "null.png";
                }
                else if (sayi == 2)
                {
                    urun.Gorsel_2EN = "null.png";
                }
                else if (sayi == 3)
                {
                    urun.Gorsel_3EN = "null.png";
                }
                else if (sayi == 4)
                {
                    urun.Gorsel_4EN = "null.png";
                }
                else if (sayi == 5)
                {
                    urun.Gorsel_5EN = "null.png";
                }
                else if (sayi == 6)
                {
                    urun.Gorsel_6EN = "null.png";
                };
            }
            _unitOfWork.UrunManager.Guncelle(urun);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }
        public ActionResult GorselSil([FromForm] int Id, string dil, string sayfa)
        {
            if (sayfa == "video")
            {
                var video = _unitOfWork.VideoManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    video.Gorsel = "null.png";
                }
                else
                {
                    video.GorselEN = "null.png";
                }
                _unitOfWork.VideoManager.Guncelle(video);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "slider")
            {
                var slider = _unitOfWork.SliderManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    slider.Gorsel = "null.png";
                }
                else
                {
                    slider.GorselEN = "null.png";
                }
                _unitOfWork.SliderManager.Guncelle(slider);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "referans")
            {
                var referans = _unitOfWork.ReferansManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    referans.Gorsel = "null.png";
                }
                else
                {
                    referans.GorselEN = "null.png";
                }
                _unitOfWork.ReferansManager.Guncelle(referans);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "popup")
            {
                var popup = _unitOfWork.PopupManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    popup.Gorsel = "null.png";
                }
                else
                {
                    popup.GorselEN = "null.png";
                }
                _unitOfWork.PopupManager.Guncelle(popup);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "blog")
            {
                var blog = _unitOfWork.BlogManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    blog.Gorsel = "null.png";
                }
                else
                {
                    blog.GorselEN = "null.png";
                }
                _unitOfWork.BlogManager.Guncelle(blog);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "galeri")
            {
                var galeri = _unitOfWork.GaleriManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    galeri.Gorsel = "null.png";
                }
                else
                {
                    galeri.GorselEN = "null.png";
                }
                _unitOfWork.GaleriManager.Guncelle(galeri);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "galerikategori")
            {
                var galeri = _unitOfWork.GaleriKategoriManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    galeri.Gorsel = "null.png";
                }
                else
                {
                    galeri.GorselEN = "null.png";
                }
                _unitOfWork.GaleriKategoriManager.Guncelle(galeri);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "ekip")
            {
                var ekip = _unitOfWork.EkipManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    ekip.Gorsel = "null.png";
                }
                else
                {
                    ekip.GorselEN = "null.png";
                }

                _unitOfWork.EkipManager.Guncelle(ekip);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }

            else if (sayfa == "etkinlik")
            {
                var entity = _unitOfWork.EtkinlikManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    entity.Gorsel = "null.png";
                }
                else
                {
                    entity.GorselEN = "null.png";
                }
                _unitOfWork.EtkinlikManager.Guncelle(entity);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "etkinlik2")
            {
                var entity = _unitOfWork.EtkinlikManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    entity.Gorsel2 = "null.png";
                }
                else
                {
                    entity.Gorsel2EN = "null.png";
                }
                _unitOfWork.EtkinlikManager.Guncelle(entity);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }

            else if (sayfa == "duyuru")
            {
                var entity = _unitOfWork.DuyuruManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    entity.Gorsel = "null.png";
                }
                else
                {
                    entity.GorselEN = "null.png";
                }
                _unitOfWork.DuyuruManager.Guncelle(entity);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "duyuru2")
            {
                var entity = _unitOfWork.DuyuruManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    entity.AnaSayfaGorsel = "null.png";
                }
                else
                {
                    entity.AnaSayfaGorselEN = "null.png";
                }
                _unitOfWork.DuyuruManager.Guncelle(entity);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }

            else if (sayfa == "ayarlar")
            {
                var entity = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
                if (dil == "logo")
                {
                    entity.Logo = "null.png";
                }
                else
                {
                    entity.Favicon = "null.png";
                }
                _unitOfWork.AyarlarManager.Guncelle(entity);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            else if (sayfa == "kurumsal")
            {
                var entity = _unitOfWork.KurumsalManager.Getir(x => x.Id == Id);
                if (dil == "tr")
                {
                    entity.Gorsel = "null.png";
                }
                else
                {
                    entity.GorselEN = "null.png";
                }
                _unitOfWork.KurumsalManager.Guncelle(entity);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }

            return Json(new
            {
                success = true,
                result = 1
            });
        }

        public ActionResult HaberSil(string Id)
        {
            _unitOfWork.HaberManager.KaliciSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }



        public ActionResult SıraArtır(string Id, string sayfa)
        {
            if (sayfa == "blog")
            {
                var blog = _unitOfWork.BlogManager.Getir(x => x.Id == int.Parse(Id));
                var blogsayisi = _unitOfWork.BlogManager.HepsiniGetir().Count();
                if (blog.SıraNo != 1)
                {

                    blog.SıraNo--;
                    var blog2 = _unitOfWork.BlogManager.Getir(x => x.SıraNo == blog.SıraNo);
                    blog2.SıraNo++;
                    _unitOfWork.BlogManager.Guncelle(blog);
                    _unitOfWork.BlogManager.Guncelle(blog2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "urun")
            {
                var urun = _unitOfWork.UrunManager.Getir(x => x.Id == int.Parse(Id));
                var urunsayisi = _unitOfWork.UrunManager.HepsiniGetir().Count();
                if (urun.SıraNo != 1)
                {

                    urun.SıraNo--;
                    var urun2 = _unitOfWork.UrunManager.Getir(x => x.SıraNo == urun.SıraNo);
                    urun2.SıraNo++;
                    _unitOfWork.UrunManager.Guncelle(urun);
                    _unitOfWork.UrunManager.Guncelle(urun2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "slider")
            {
                var slider = _unitOfWork.SliderManager.Getir(x => x.Id == int.Parse(Id));
                var slidersayisi = _unitOfWork.SliderManager.HepsiniGetir().Count();
                if (slider.SıraNo != 1)
                {

                    slider.SıraNo--;
                    var slider2 = _unitOfWork.SliderManager.Getir(x => x.SıraNo == slider.SıraNo);
                    slider2.SıraNo++;
                    _unitOfWork.SliderManager.Guncelle(slider);
                    _unitOfWork.SliderManager.Guncelle(slider2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "blogkategori")
            {
                var entity = _unitOfWork.BlogKategoriManager.Getir(x => x.Id == int.Parse(Id));
                var entitysayisi = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.SıralamaNo != 0).Count();
                if (entity.SıralamaNo != 1)
                {

                    entity.SıralamaNo--;
                    var entity2 = _unitOfWork.BlogKategoriManager.Getir(x => x.SıralamaNo == entity.SıralamaNo);
                    entity2.SıralamaNo++;
                    _unitOfWork.BlogKategoriManager.Guncelle(entity);
                    _unitOfWork.BlogKategoriManager.Guncelle(entity2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "kurumsal")
            {
                var entity = _unitOfWork.KurumsalManager.Getir(x => x.Id == int.Parse(Id));
                var entitysayisi = _unitOfWork.KurumsalManager.HepsiniGetir().Count();
                if (entity.SıralamaNo != 1)
                {

                    entity.SıralamaNo--;
                    var entity2 = _unitOfWork.KurumsalManager.Getir(x => x.SıralamaNo == entity.SıralamaNo);
                    entity2.SıralamaNo++;
                    _unitOfWork.KurumsalManager.Guncelle(entity);
                    _unitOfWork.KurumsalManager.Guncelle(entity2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "etkinlik")
            {
                var entity = _unitOfWork.EtkinlikManager.Getir(x => x.Id == int.Parse(Id));
                var entitysayisi = _unitOfWork.EtkinlikManager.HepsiniGetir().Count();
                if (entity.SıralamaNo != 1)
                {

                    entity.SıralamaNo--;
                    var entity2 = _unitOfWork.EtkinlikManager.Getir(x => x.SıralamaNo == entity.SıralamaNo);
                    entity2.SıralamaNo++;
                    _unitOfWork.EtkinlikManager.Guncelle(entity);
                    _unitOfWork.EtkinlikManager.Guncelle(entity2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    result = 2
                });
            }


        }

        public ActionResult SıraAzalt(string Id, string sayfa)
        {
            if (sayfa == "blog")
            {
                var blog = _unitOfWork.BlogManager.Getir(x => x.Id == int.Parse(Id));
                var blogsayisi = _unitOfWork.BlogManager.HepsiniGetir().Count();
                if (blog.SıraNo != blogsayisi)
                {

                    blog.SıraNo++;
                    var blog2 = _unitOfWork.BlogManager.Getir(x => x.SıraNo == blog.SıraNo);
                    blog2.SıraNo--;
                    _unitOfWork.BlogManager.Guncelle(blog);
                    _unitOfWork.BlogManager.Guncelle(blog2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "urun")
            {
                var urun = _unitOfWork.UrunManager.Getir(x => x.Id == int.Parse(Id));
                var urunsayisi = _unitOfWork.UrunManager.HepsiniGetir().Count();
                if (urun.SıraNo != urunsayisi)
                {

                    urun.SıraNo++;
                    var urun2 = _unitOfWork.UrunManager.Getir(x => x.SıraNo == urun.SıraNo);
                    urun2.SıraNo--;
                    _unitOfWork.UrunManager.Guncelle(urun);
                    _unitOfWork.UrunManager.Guncelle(urun2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "slider")
            {
                var entity = _unitOfWork.SliderManager.Getir(x => x.Id == int.Parse(Id));
                var entitysayisi = _unitOfWork.SliderManager.HepsiniGetir().Count();
                if (entity.SıraNo != entitysayisi)
                {

                    entity.SıraNo++;
                    var entity2 = _unitOfWork.SliderManager.Getir(x => x.SıraNo == entity.SıraNo);
                    entity2.SıraNo--;
                    _unitOfWork.SliderManager.Guncelle(entity);
                    _unitOfWork.SliderManager.Guncelle(entity2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "blogkategori")
            {
                var entity = _unitOfWork.BlogKategoriManager.Getir(x => x.Id == int.Parse(Id));
                var entitysayisi = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.SıralamaNo != 0).Count();
                if (entity.SıralamaNo != entitysayisi)
                {

                    entity.SıralamaNo++;
                    var entity2 = _unitOfWork.BlogKategoriManager.Getir(x => x.SıralamaNo == entity.SıralamaNo);
                    entity2.SıralamaNo--;
                    _unitOfWork.BlogKategoriManager.Guncelle(entity);
                    _unitOfWork.BlogKategoriManager.Guncelle(entity2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "kurumsal")
            {
                var entity = _unitOfWork.KurumsalManager.Getir(x => x.Id == int.Parse(Id));
                var entitysayisi = _unitOfWork.KurumsalManager.HepsiniGetir().Count();
                if (entity.SıralamaNo != entitysayisi)
                {

                    entity.SıralamaNo++;
                    var entity2 = _unitOfWork.KurumsalManager.Getir(x => x.SıralamaNo == entity.SıralamaNo);
                    entity2.SıralamaNo--;
                    _unitOfWork.KurumsalManager.Guncelle(entity);
                    _unitOfWork.KurumsalManager.Guncelle(entity2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else if (sayfa == "etkinlik")
            {
                var entity = _unitOfWork.EtkinlikManager.Getir(x => x.Id == int.Parse(Id));
                var entitysayisi = _unitOfWork.EtkinlikManager.HepsiniGetir().Count();
                if (entity.SıralamaNo != entitysayisi)
                {

                    entity.SıralamaNo++;
                    var entity2 = _unitOfWork.EtkinlikManager.Getir(x => x.SıralamaNo == entity.SıralamaNo);
                    entity2.SıralamaNo--;
                    _unitOfWork.EtkinlikManager.Guncelle(entity);
                    _unitOfWork.EtkinlikManager.Guncelle(entity2);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new
                    {
                        success = true,
                        result = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        result = 2
                    });
                }
            }
            else
            {
                return Json(new
                {
                    success = false,
                    result = 2
                });
            }


        }


        public ActionResult BlogSil(string Id)
        {
            _unitOfWork.BlogManager.KalıcıSil(x => x.Id == int.Parse(Id));
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }


        public async Task<IActionResult> CıkısYap()
        {
            await HttpContext.SignOutAsync();
            return Redirect("Index");
        }

        [HttpPost]
        public ActionResult PasifeCevir(string Id)
        {
            var haber = _unitOfWork.HaberManager.Getir(x => x.Id == int.Parse(Id));
            if (haber.Aktif)
            {
                haber.Aktif = false;
            }
            else
            {
                haber.Aktif = true;
            }
            _unitOfWork.HaberManager.Guncelle(haber);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }



        [HttpPost]
        public ActionResult PasifeCevirKullanici(string Id)
        {
            var kullanici = _unitOfWork.YoneticiManager.Getir(x => x.Id == int.Parse(Id));
            if (kullanici.Aktif)
            {
                kullanici.Aktif = false;
            }
            else
            {
                kullanici.Aktif = true;
            }
            _unitOfWork.YoneticiManager.Guncelle(kullanici);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
            });
        }


        public IActionResult ParolaUnuttum()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ParolaUnuttum(string email)
        {
            var kullanici = _unitOfWork.YoneticiManager.Getir(x => x.Email == email);
            if (kullanici != null)
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.Credentials = new NetworkCredential("testzemedya@gmail.com", "zemedya34");
                client.EnableSsl = true;
                MailMessage msj = new MailMessage();
                msj.From = new MailAddress("testzemedya@gmail.com", "Zemedya");
                msj.To.Add(email);
                msj.Subject = "Şifre mi Unuttum";
                msj.Body = "Şifreniz : " + kullanici.Sifre;
                client.Send(msj);
                ViewData["hata"] = "E-Posta adresinize şifreniz gönderilmiştir";
                return View();
            }
            else
            {
                ViewData["hata"] = "E-Posta adresiniz sistemde kayıtlı değildir.";
                return View();
            }

        }

        [HttpPost]
        public ActionResult EmailAyar(string Adres, string Server, string Sifre, string Port, string IletilecekAdres, string IletilecekAdres2, string WhatsappTelefon, string WhatsappMesaj)
        {
            var ayar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            ayar.Adres = Adres;
            ayar.Server = Server;
            ayar.Sifre = Sifre;
            ayar.Port = Port;
            ayar.IletilecekAdres2 = IletilecekAdres2;
            ayar.IletilecekAdres = IletilecekAdres;
            ayar.WhatsappTelefon = WhatsappTelefon;
            ayar.WhatsappMesaj = WhatsappMesaj;
            _unitOfWork.AyarlarManager.Guncelle(ayar);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult SosyalAyar(string Facebook, string Instagram, string Linkedin, string Youtube, string Twitter, int FacebookOrder, int InstagramOrder, int LinkedinOrder, int YoutubeOrder, int TwitterOrder)
        {
            var ayar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            ayar.Facebook = Facebook;
            ayar.FacebookOrder = FacebookOrder;
            ayar.Twitter = Twitter;
            ayar.TwitterOrder = TwitterOrder;
            ayar.Instagram = Instagram;
            ayar.InstagramOrder = InstagramOrder;
            ayar.Youtube = Youtube;
            ayar.YoutubeOrder = YoutubeOrder;
            ayar.Linkedin = Linkedin;
            ayar.LinkedinOrder = LinkedinOrder;
            _unitOfWork.AyarlarManager.Guncelle(ayar);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult GoogleAyar(string GoogleAnalyticsKodu, string GoogleDogrulamaKodu)
        {
            var ayar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            ayar.GoogleAnalyticsKodu = GoogleAnalyticsKodu;
            ayar.GoogleDogrulamaKodu = GoogleDogrulamaKodu;
            _unitOfWork.AyarlarManager.Guncelle(ayar);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
                result = 1
            });
        }


        [HttpPost]
        public ActionResult WhatsappAyar(string WhatsappMesaj, string WhatsappTelefon, string RecaptchaSecretKey, string CanliDestekKodu, bool WhatsappDurum, bool CanliDestekAktif)
        {
            var ayar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            ayar.WhatsappMesaj = WhatsappMesaj;
            ayar.WhatsappTelefon = WhatsappTelefon;
            ayar.CanliDestekKodu = CanliDestekKodu;
            ayar.WhatsappDurum = WhatsappDurum;
            ayar.CanliDestekAktif = CanliDestekAktif;
            _unitOfWork.AyarlarManager.Guncelle(ayar);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [HttpPost]
        public ActionResult SiteAyar(string SiteUrl, string SiteTitle, string SiteTitleEN, string Description, string DescriptionEN, IFormFile logo, IFormFile favicon)
        {
            var ayar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            ayar.SiteUrl = SiteUrl;
            ayar.SiteTitle = SiteTitle;
            ayar.SiteTitleEN = SiteTitleEN;
            ayar.Description = Description;
            ayar.DescriptionEN = DescriptionEN;
            if (logo != null)
            {
                string uzanti = Path.GetExtension(logo.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    logo.CopyTo(stream);
                }
                ayar.Logo = resimAd;
            }

            if (favicon != null)
            {
                string uzantiEN = Path.GetExtension(favicon.FileName);
                string resimAdEN = Guid.NewGuid() + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    favicon.CopyTo(streamEN);
                }
                ayar.Favicon = resimAdEN;
            }

            _unitOfWork.AyarlarManager.Guncelle(ayar);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Json(new
            {
                success = true,
                result = 1
            });
        }



        public ActionResult BrochureEkle(IFormFile pdf)
        {
            var brochure = _unitOfWork.KurumsalManager.Getir(x => x.Id == 2);
            if (pdf != null)
            {
                string uzantiEN = Path.GetExtension(pdf.FileName);
                string resimAdEN = "brochure-" + Guid.NewGuid().ToString("N").Substring(0, 4) + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    pdf.CopyTo(streamEN);
                }
                brochure.Gorsel = resimAdEN;
            }
            _unitOfWork.KurumsalManager.Guncelle(brochure);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        public ActionResult BrochureEkle2(IFormFile pdf)
        {
            var brochure = _unitOfWork.KurumsalManager.Getir(x => x.Id == 2);
            if (pdf != null)
            {
                string uzantiEN = Path.GetExtension(pdf.FileName);
                string resimAdEN = "brochure-" + Guid.NewGuid().ToString("N").Substring(0, 4) + uzantiEN;
                string pathEN = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + resimAdEN);
                using (var streamEN = new FileStream(pathEN, FileMode.Create))
                {
                    pdf.CopyTo(streamEN);
                }
                brochure.GorselEN = resimAdEN;
            }
            _unitOfWork.KurumsalManager.Guncelle(brochure);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();
            return Json(new
            {
                success = true,
                result = 1
            });
        }

        [Route("/Panel/Certificates")]
        [Authorize]
        public IActionResult Certificates()
        {
            var model = _unitOfWork.MembershipManager.GetAllCertificates();
            return View(model);
        }

        [Route("/Panel/CertificatesAddOrUpdate")]
        [Authorize]
        public IActionResult CertificatesAddOrUpdate(int id)
        {
            var model = _unitOfWork.MembershipManager.Get(x => x.Id == id);
            return View(model);
        }

        [HttpPost]
        [Route("/Panel/CertificatesAddOrUpdate")]
        [Authorize]
        public IActionResult CertificatesAddOrUpdate(Membership model)
        {
            model.Type = 1;
            if ((model.Image == null && model.ImageFile == null) ||
                      (model.ImageFile != null &&
                      !string.Equals(model.ImageFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.ImageFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Resim dosyası seçiniz";
                return View(model);
            }


            if (model.ImageFile != null)
            {
                string uzanti = Path.GetExtension(model.ImageFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.ImageFile.CopyTo(stream);
                }
                model.Image = resimAd;
            }

            _unitOfWork.MembershipManager.AddOrUpdate(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();



            return Redirect("/Panel/Certificates");
        }

        [Route("/Panel/CertificatesDelete")]
        [Authorize]
        public IActionResult CertificatesDelete(int Id)
        {
            _unitOfWork.MembershipManager.Delete(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Certificates");
        }

        [Route("/Panel/CertificatesUp")]
        [Authorize]
        public IActionResult CertificatesUp(int Id)
        {
            _unitOfWork.MembershipManager.UpMembership(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Certificates");
        }

        [Route("/Panel/CertificatesDown")]
        [Authorize]
        public IActionResult CertificatesDown(int Id)
        {
            _unitOfWork.MembershipManager.DownMembership(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Certificates");
        }


        [Route("/Panel/Press")]
        [Authorize]
        public IActionResult Press()
        {
            var model = _unitOfWork.PressService.GetAll();
            return View(model);
        }

        [Route("/Panel/PressAddOrUpdate")]
        [Authorize]
        public IActionResult PressAddOrUpdate(int id)
        {
            var model = _unitOfWork.PressService.Get(x => x.Id == id);
            return View(model);
        }

        [HttpPost]
        [Route("/Panel/PressAddOrUpdate")]
        [Authorize]
        public IActionResult PressAddOrUpdate(Press model)
        {
            if ((model.GorselTRFile == null && model.GorselTR == null) ||
                      (model.GorselTRFile != null &&
                      !string.Equals(model.GorselTRFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselTRFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselTRFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselTRFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselTRFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselTRFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Her dil için resim dosyası seçiniz";
                return View(model);
            }

            if ((model.GorselENFile == null && model.GorselEN == null) ||
                      (model.GorselENFile != null &&
                      !string.Equals(model.GorselENFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselENFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselENFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselENFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselENFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                      !string.Equals(model.GorselENFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Her dil için resim dosyası seçiniz";
                return View(model);
            }


            if (model.GorselTRFile != null)
            {
                string uzanti = Path.GetExtension(model.GorselTRFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.GorselTRFile.CopyTo(stream);
                }
                model.GorselTR = resimAd;
            }

            if (model.GorselENFile != null)
            {
                string uzanti = Path.GetExtension(model.GorselENFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.GorselENFile.CopyTo(stream);
                }
                model.GorselEN = resimAd;
            }

            _unitOfWork.PressService.AddOrUpdate(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();



            return Redirect("/Panel/Press");
        }

        [Route("/Panel/PressDelete")]
        [Authorize]
        public IActionResult PressDelete(int Id)
        {
            _unitOfWork.PressService.Delete(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Press");
        }

        [Route("/Panel/PressUp")]
        [Authorize]
        public IActionResult PressUp(int Id)
        {
            _unitOfWork.PressService.Up(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Press");
        }

        [Route("/Panel/PressDown")]
        [Authorize]
        public IActionResult PressDown(int Id)
        {
            _unitOfWork.PressService.Down(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/Press");
        }

        [Authorize]
        public IActionResult ImageMediaCategories()
        {
            var model = _unitOfWork.ImageMediaCategoryManager.GetAll();

            return View(model);
        }

        [Authorize]
        public IActionResult ImageMediaCategoryAddOrUpdate(int id)
        {
            var model = _unitOfWork.ImageMediaCategoryManager.Get(x => x.Id == id);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult ImageMediaCategoryAddOrUpdate(ImageMediaCategory model)
        {
            if ((model.Image == null && model.ImageFile == null) ||
                  (model.ImageFile != null &&
                  !string.Equals(model.ImageFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Resim dosyası seçiniz";
                return View(model);
            }


            if (model.ImageFile != null)
            {
                string uzanti = Path.GetExtension(model.ImageFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.ImageFile.CopyTo(stream);
                }
                model.Image = resimAd;
            }

            _unitOfWork.ImageMediaCategoryManager.AddOrUpdate(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Redirect("/Panel/ImageMediaCategories");
        }

        [Authorize]
        public IActionResult ImageMediaCategoryDelete(int Id)
        {
            _unitOfWork.ImageMediaCategoryManager.Delete(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/ImageMediaCategories");
        }

        [Authorize]
        public IActionResult ImageMediaItems(int categoryId)
        {
            var model = _unitOfWork.ImageMediaCategoryManager.Get(x => x.Id == categoryId);

            return View(model);
        }




        [Authorize]
        public IActionResult ImageMediaItemAddOrUpdate(int id, int imageMediaCategoryId)
        {
            var model = _unitOfWork.ImageMediaCategoryManager.GetItem(x => x.Id == id);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult ImageMediaItemAddOrUpdate(ImageMediaItem model)
        {
            if ((model.Image == null && model.ImageFile == null) ||
                  (model.ImageFile != null &&
                  !string.Equals(model.ImageFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Resim dosyası seçiniz";
                return View(model);
            }


            if (model.ImageFile != null)
            {
                string uzanti = Path.GetExtension(model.ImageFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.ImageFile.CopyTo(stream);
                }
                model.Image = resimAd;
            }

            _unitOfWork.ImageMediaCategoryManager.AddOrUpdateItem(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return RedirectToAction("ImageMediaItems", new { categoryId = model.ImageMediaCategoryId });
        }

        [Authorize]
        public IActionResult ImageMediaItemDelete(int Id, int imageMediaCategoryId)
        {
            _unitOfWork.ImageMediaCategoryManager.DeleteItem(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return RedirectToAction("ImageMediaItems", new { categoryId = imageMediaCategoryId });
        }






























        [Authorize]
        public IActionResult VideoMediaCategories()
        {
            var model = _unitOfWork.VideoMediaCategoryManager.GetAll();

            return View(model);
        }

        [Authorize]
        public IActionResult VideoMediaCategoryAddOrUpdate(int id)
        {
            var model = _unitOfWork.VideoMediaCategoryManager.Get(x => x.Id == id);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult VideoMediaCategoryAddOrUpdate(VideoMediaCategory model)
        {
            if ((model.Image == null && model.ImageFile == null) ||
                  (model.ImageFile != null &&
                  !string.Equals(model.ImageFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                  !string.Equals(model.ImageFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
            {
                model.ErrorMessage = "Resim dosyası seçiniz";
                return View(model);
            }


            if (model.ImageFile != null)
            {
                string uzanti = Path.GetExtension(model.ImageFile.FileName);
                string resimAd = Guid.NewGuid() + uzanti;
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/" + resimAd);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    model.ImageFile.CopyTo(stream);
                }
                model.Image = resimAd;
            }

            _unitOfWork.VideoMediaCategoryManager.AddOrUpdate(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Redirect("/Panel/VideoMediaCategories");
        }

        [Authorize]
        public IActionResult VideoMediaCategoryDelete(int Id)
        {
            _unitOfWork.VideoMediaCategoryManager.Delete(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return Redirect("/Panel/VideoMediaCategories");
        }




        [Authorize]
        public IActionResult VideoMediaItems(int categoryId)
        {
            var model = _unitOfWork.VideoMediaCategoryManager.Get(x => x.Id == categoryId);

            return View(model);
        }


        [Authorize]
        public IActionResult VideoMediaItemAddOrUpdate(int id, int videoMediaCategoryId)
        {
            var model = _unitOfWork.VideoMediaCategoryManager.GetItem(x => x.Id == id);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult VideoMediaItemAddOrUpdate(VideoMediaItem model)
        {

            _unitOfWork.VideoMediaCategoryManager.AddOrUpdateItem(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return RedirectToAction("VideoMediaItems", new { categoryId = model.VideoMediaCategoryId });
        }

        [Authorize]
        public IActionResult VideoMediaItemDelete(int Id, int VideoMediaCategoryId)
        {
            _unitOfWork.VideoMediaCategoryManager.DeleteItem(x => x.Id == Id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();


            return RedirectToAction("VideoMediaItems", new { categoryId = VideoMediaCategoryId });
        }

        [Authorize]
        public IActionResult OpenPositions()
        {
            var model = _unitOfWork.AcikPozisyonManager.GetAll();
            return View(model);
        }

        [Authorize]
        public IActionResult OpenPositionAddOrUpdate(int id)
        {
            var model = _unitOfWork.AcikPozisyonManager.Get(x => x.Id == id);
            return View(model);
        }


        [Authorize]
        [HttpPost]
        public IActionResult OpenPositionAddOrUpdate(AcikPozisyon model)
        {
            _unitOfWork.AcikPozisyonManager.AddOrUpdate(model);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Redirect("/Panel/OpenPositions");
        }

        [Authorize]
        public IActionResult OpenPositionDelete(int id)
        {
            _unitOfWork.AcikPozisyonManager.Delete(x => x.Id == id);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Redirect("/Panel/OpenPositions");
        }

    }
}
