﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zemedya.Business.Abstract.UnitOfWork;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Model.DTO.BlogDTOs;
using Zemedya.Model.DTO.DuyuruDTOs;
using Zemedya.Model.DTO.EkipDTOs;
using Zemedya.Model.DTO.EmailDTOs;
using Zemedya.Model.DTO.VideoDTOs;
using Zemedya.Model.VM.Page;

namespace Zemedya.AspNetCoreMVC.Controllers
{
    public class PageController : Controller
    {
        private IUnitOfWork _unitOfWork;
        public PageController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [Route("/tr")]
        public IActionResult Media()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            if (siteBilgisi.SiteTitle == null)
            {
                siteBilgisi.SiteTitle = "Zemedya";
            }
            if (siteBilgisi.GoogleAnalyticsKodu == null)
            {
                siteBilgisi.GoogleAnalyticsKodu = "google";
            }
            AnasayfaVM anasayfaVM = new AnasayfaVM()
            {
                Sliderlar = _unitOfWork.SliderManager.HepsiniGetir(x => x.Aktif),
                Title = siteBilgisi.SiteTitle.Trim(),
                GoogleDogrulama = siteBilgisi.GoogleDogrulamaKodu,
                Favicon = siteBilgisi.Favicon,
                GoogleAnalytics = siteBilgisi.GoogleAnalyticsKodu.Trim(),
                BlogKategoriListesi = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.Aktif),
                Solutions = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif == true),
                Referans = _unitOfWork.ReferansManager.HepsiniGetir(x => x.Aktif == true),
                KurumsalDTO = _unitOfWork.KurumsalManager.Getir(x => x.Id == 1)
            };
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            return View(anasayfaVM);
        }

        [Route("")]
        public IActionResult MediaEN()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            if (siteBilgisi.SiteTitleEN == null)
            {
                siteBilgisi.SiteTitleEN = "Zemedya";
            }
            if (siteBilgisi.GoogleAnalyticsKodu == null)
            {
                siteBilgisi.GoogleAnalyticsKodu = "google";
            }
            AnasayfaVM anasayfaVM = new AnasayfaVM()
            {
                Sliderlar = _unitOfWork.SliderManager.HepsiniGetir(x => x.Aktif),
                Title = siteBilgisi.SiteTitleEN.Trim(),
                GoogleDogrulama = siteBilgisi.GoogleDogrulamaKodu,
                Favicon = siteBilgisi.Favicon,
                GoogleAnalytics = siteBilgisi.GoogleAnalyticsKodu.Trim(),
                BlogKategoriListesi = _unitOfWork.BlogKategoriManager.HepsiniGetir(x => x.Aktif),
                Solutions = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif == true),
                Referans = _unitOfWork.ReferansManager.HepsiniGetir(x => x.Aktif == true),
                KurumsalDTO = _unitOfWork.KurumsalManager.Getir(x => x.Id == 1)
            };
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.description = siteBilgisi.DescriptionEN.Trim();
            }
            return View(anasayfaVM);
        }




        [Route("iletisim")]
        public IActionResult Iletisim()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            var iletisim = _unitOfWork.IletisimManager.Getir(x => x.Id == 1);
            ViewBag.facebook = siteBilgisi.Facebook;
            ViewBag.twitter = siteBilgisi.Twitter;
            ViewBag.linkedin = siteBilgisi.Linkedin;
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(iletisim);
        }


        [Route("contact/{seo}")]
        public IActionResult Contact(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            var iletisim = _unitOfWork.IletisimManager.Getir(x => x.Id == 1);
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            if (seo == "en")
            {
                ViewBag.en = "true";
            }
            return View(iletisim);
        }



        public ActionResult EmailGonder([FromForm] EmailDTO emailDTO, IFormFile dosya)
        {
            try
            {
                var ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
                SmtpClient client = new SmtpClient(ayarlar.Server, int.Parse(ayarlar.Port));
                client.Credentials = new NetworkCredential(ayarlar.Adres, ayarlar.Sifre);
                client.EnableSsl = false;
                MailMessage msj = new MailMessage();

                msj.From = new MailAddress("website@platinum-ms.com", emailDTO.Ad + " " + emailDTO.Soyad);



                msj.Body = "Ad Soyad:  " + emailDTO.Ad + " " + emailDTO.Soyad + Environment.NewLine + "Telefon:   " + emailDTO.Telefon + Environment.NewLine + "Email:     " + emailDTO.EpostaAdı + Environment.NewLine + "Konu:      " + emailDTO.Konu + Environment.NewLine + "Mesaj:     " + emailDTO.Mesaj;
                if (dosya != null)
                {
                    string FileName = Path.GetFileName(dosya.FileName);
                    msj.Attachments.Add(new Attachment(dosya.OpenReadStream(), FileName));
                    msj.To.Add(ayarlar.IletilecekAdres2);
                    if (ayarlar.WhatsappMesaj != null)
                    {
                        msj.Subject = ayarlar.WhatsappMesaj;
                    }
                    else
                    {
                        msj.Subject = emailDTO.Konu;
                    }

                }
                else
                {
                    if (ayarlar.WhatsappTelefon != null)
                    {
                        msj.Subject = ayarlar.WhatsappTelefon;
                    }
                    else
                    {
                        msj.Subject = emailDTO.Konu;
                    }
                    msj.To.Add(ayarlar.IletilecekAdres);
                }
                client.Send(msj);
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            catch (Exception)
            {

                return Json(new
                {
                    success = true,
                    result = 2
                });
            }

        }





        public ActionResult SiteBilgileri(string bilgi)
        {
            var ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            return Json(new
            {
                success = true,
                whatsapptel = ayarlar.WhatsappTelefon,
                whatsappdurum = ayarlar.WhatsappDurum,
                whatsappMesaj = ayarlar.WhatsappMesaj,
                canlidestek = ayarlar.CanliDestekKodu,
                canlidestekaktif = ayarlar.CanliDestekAktif
            });
        }



        [Route("solution-partners/{seo}")]
        [Route("partnerler/{seo}")]
        public IActionResult Etkinlik(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var etkinlik = _unitOfWork.EtkinlikManager.Getir(x => x.SEO == seo);
            if (etkinlik == null)
            {
                etkinlik = _unitOfWork.EtkinlikManager.Getir(x => x.SEOEN == seo);
                if (etkinlik != null)
                {
                    ViewBag.en = "true";
                }
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(etkinlik);
        }



        [Route("solutions/{seo}")]
        [Route("cozumler/{seo}")]
        public IActionResult Duyuru(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            DuyuruDTO duyuru = _unitOfWork.DuyuruManager.Getir(x => x.SEO == seo);
            if (duyuru == null)
            {
                duyuru = _unitOfWork.DuyuruManager.Getir(x => x.SEOEN == seo);
                if (duyuru != null)
                {
                    ViewBag.en = "true";
                }
            }

            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            duyuru.Galeri = _unitOfWork.GaleriManager.HepsiniGetir(x => x.GaleriKategoriId == duyuru.GaleriKategoriId && x.Aktif == true);
            duyuru.Referans = _unitOfWork.ReferansManager.HepsiniGetir(x => x.Aktif == true);
            return View(duyuru);
        }

        [Route("corporate/{seo}")]
        [Route("kurumsal/{seo}")]
        public IActionResult Corporate(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            BlogKategoriDTO model = _unitOfWork.BlogKategoriManager.Getir(x => x.SEO == seo);
            if (model == null)
            {
                model = _unitOfWork.BlogKategoriManager.Getir(x => x.SEOEN == seo);
                if (model != null)
                {
                    ViewBag.en = "true";
                }
            }

            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;

            return View(model);
        }

        [Route("media/{seo}")]
        public IActionResult MediaPage(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            EkipDTO model = _unitOfWork.EkipManager.Getir(x => x.SEO == seo);
            if (model == null)
            {
                model = _unitOfWork.EkipManager.Getir(x => x.SEOEN == seo);
                if (model != null)
                {
                    ViewBag.en = "true";
                }
            }

            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(model);
        }



        [Route("career/{seo}")]
        [Route("kariyer/{seo}")]
        public IActionResult Human(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            VideoDTO model = _unitOfWork.VideoManager.Getir(x => x.Baslik == seo);
            if (model == null)
            {
                model = _unitOfWork.VideoManager.Getir(x => x.BaslikEN == seo);
                ViewBag.en = "true";
            }



            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;

            if (model.Id==1)
            {
                ViewBag.OpenPosition = _unitOfWork.AcikPozisyonManager.GetAll();
            }


            return View(model);
        }




        //[Route("certificates")]
        //public ActionResult Certificates()
        //{

        //    var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
        //    if (siteBilgisi.GoogleDogrulamaKodu != null)
        //    {
        //        siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
        //        siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
        //        int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
        //        siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
        //    }
        //    ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
        //    ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
        //    if (siteBilgisi.DescriptionEN != null)
        //    {
        //        ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
        //    }
        //    if (siteBilgisi.Description != null)
        //    {
        //        ViewBag.description = siteBilgisi.Description.Trim();
        //    }
        //    if (siteBilgisi.GoogleAnalyticsKodu != null)
        //    {
        //        ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
        //    }
        //    ViewBag.favicon = siteBilgisi.Favicon;
        //    return View();
        //}

        [Route("innovation")]
        [Route("inovasyon")]
        public ActionResult Innovation()
        {

            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            VideoDTO model = _unitOfWork.VideoManager.Getir(x => x.Id == 2);


            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(model);
        }


        [Route("references")]
        [Route("referanslar")]
        public ActionResult Logolar(string seo)
        {

            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }

            var model = _unitOfWork.ReferansManager.HepsiniGetir(x => x.Aktif == true);


            if (seo == "en")
            {
                ViewBag.en = "true";
            }


            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(model);
        }

        [Route("corporate/organization/{seo?}")]
        [Route("kurumsal/organizasyon/{seo?}")]
        public IActionResult OurTeam(string seo)
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("kurumsal"))
            {
                ViewBag.Lang = "tr";
            }

            ViewBag.Seo = seo;

            var model = _unitOfWork.TeamManager.GetAllTeam();
            return View(model);
        }

        [Route("corporate/membership")]
        [Route("kurumsal/uyelikler")]
        public IActionResult Membership()
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("kurumsal"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.MembershipManager.GetAll();
            return View(model);
        }

        [Route("corporate/certificates")]
        [Route("kurumsal/sertifikalar")]
        public IActionResult CertificatesNew()
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("kurumsal"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.MembershipManager.GetAllCertificates();
            return View(model);
        }

        [Route("/press")]
        [Route("/basin")]
        public IActionResult Press()
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("basin"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.PressService.GetAll();
            return View(model);
        }

        [Route("/press/{title}")]
        [Route("/basin/{title}")]
        public IActionResult PressDetail(string title)
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("basin"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.PressService.Get(x => x.BaslikEN == title || x.BaslikTR == title);
            return View(model);
        }

        [Route("/imagegallery")]
        [Route("/resimgaleri")]
        public IActionResult ImageGallery()
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("resimgaleri"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.ImageMediaCategoryManager.GetAll();
            return View(model);
        }

        [Route("/imagegallery/{gallary}")]
        [Route("/resimgaleri/{gallary}")]
        public IActionResult ImageGalleryItems(string gallary)
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("resimgaleri"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.ImageMediaCategoryManager.Get(x => x.NameEN == gallary || x.NameTR == gallary);
            return View(model);
        }

        [Route("/videogallery")]
        [Route("/videogaleri")]
        public IActionResult VideoGallery()
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("videogaleri"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.VideoMediaCategoryManager.GetAll();
            return View(model);
        }

        [Route("/videogallery/{gallary}")]
        [Route("/videogaleri/{gallary}")]
        public IActionResult VideoGalleryItems(string gallary)
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("Videogaleri"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.VideoMediaCategoryManager.Get(x => x.NameEN == gallary || x.NameTR == gallary);
            return View(model);
        }

        [Route("/openposition/{name}")]
        [Route("/acikposizyon/{name}")]
        public IActionResult OpenPosition(string name)
        {
            ViewBag.Lang = "en";
            if (HttpContext.Request.Path.Value.ToLower().Contains("acikposizyon"))
            {
                ViewBag.Lang = "tr";
            }

            var model = _unitOfWork.AcikPozisyonManager.Get(x => x.NameEn == name || x.NameTr == name);
            return View(model);
        }

    }
}
