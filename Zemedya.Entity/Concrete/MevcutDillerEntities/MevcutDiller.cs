﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.MevcutDillerEntities
{
    public class MevcutDiller : IEntity
    {
        [Key]
        public int Id { get; set; }
        public bool TR { get; set; }
        public bool EN { get; set; }
    }
}
