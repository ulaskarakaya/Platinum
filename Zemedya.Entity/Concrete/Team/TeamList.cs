﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.Team
{
    public class TeamList : IEntity
    {
        public int Id { get; set; }
        public string NameSurname { get; set; }
        public string Image { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }
        public string TitleTr { get; set; }
        public string TitleEn { get; set; }
        public int Order { get; set; }
        public int TeamCategoryId { get; set; }
        public virtual TeamCategory TeamCategory { get; set; }
    }
}