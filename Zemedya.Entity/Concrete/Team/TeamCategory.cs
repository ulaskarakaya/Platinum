﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.Team
{
    public class TeamCategory : IEntity
    {
        public TeamCategory()
        {
            TeamLists = new List<TeamList>();
        }

        public int Id { get; set; }
        public string TeamNameTr { get; set; }
        public string TeamNameEn { get; set; }
        public string TeamSeoTr { get; set; }
        public string TeamSeoEn { get; set; }
        public int Order { get; set; }
        public virtual List<TeamList> TeamLists { get; set; }
    }
}
