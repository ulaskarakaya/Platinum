﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.MediaEntities
{
    public class ImageMediaItem : IEntity
    {
        public int Id { get; set; }
        public string Image { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public string DescriptionTR { get; set; }
        public string DescriptionEN { get; set; }
        public int ImageMediaCategoryId { get; set; }
        public virtual ImageMediaCategory ImageMediaCategory { get; set; }
    }
}