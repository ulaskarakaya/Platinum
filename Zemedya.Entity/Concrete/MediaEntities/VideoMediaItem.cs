﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.MediaEntities
{
    public class VideoMediaItem : IEntity
    {
        public int Id { get; set; }
        public string video { get; set; }
        public string DescriptionTR { get; set; }
        public string DescriptionEN { get; set; }
        public int VideoMediaCategoryId { get; set; }
        public virtual VideoMediaCategory VideoMediaCategory { get; set; }
    }
}