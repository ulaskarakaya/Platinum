﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.MediaEntities
{
    public class ImageMediaCategory : IEntity
    {
        public ImageMediaCategory()
        {
            ImageMediaItems = new List<ImageMediaItem>();
        }

        public int Id { get; set; }
        public string NameTR { get; set; }
        public string NameEN { get; set; }
        public string Image { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public virtual List<ImageMediaItem> ImageMediaItems { get; set; }
    }
}
