﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.VideoEntities
{
    public class Video : IEntity
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string AdEN { get; set; }
        public string Baslik { get; set; }
        public string BaslikEN { get; set; }
        public string VideoUrl { get; set; }
        public string VideoUrlEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public bool Aktif { get; set; }
        public string DataVideoId { get; set; }

    }
}
