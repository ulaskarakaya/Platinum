﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.EkipEntities
{
    public class Ekip : IEntity
    {
        [Key]
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Email { get; set; }
        public string Unvan { get; set; }
        public string Telefon { get; set; }
        public string Departman { get; set; }
        public string Aciklama { get; set; }
        public string Gorsel { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public string GorselEN { get; set; }
        public string SEO { get; set; }
        public string SEOEN { get; set; }
        public bool Aktif { get; set; }
    }
}
