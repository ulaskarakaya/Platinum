﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;
using Zemedya.Entity.Concrete.GaleriEntities;

namespace Zemedya.Entity.Concrete.EtkinlikEntities
{
    public class Etkinlik : BaseEntity, IEntity
    {
        public string Ozet { get; set; }
        public string OzetEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public DateTime Tarih { get; set; }
        public int SıralamaNo { get; set; }
        public string Detay2 { get; set; }
        public string Detay2EN { get; set; }
        public string Gorsel2 { get; set; }
        public string Gorsel2EN { get; set; }

       




    }
}

