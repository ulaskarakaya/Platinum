﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.UrunEntities
{
    public class UrunKategori : BaseEntity, IEntity
    {
        public UrunKategori()
        {
            this.Urunler = new HashSet<Urun>();
            this.AltKategoriler = new HashSet<UrunKategori>();
        }

        public int UstKategoriId { get; set; }
        public virtual UrunKategori UstKategori { get; set; }

        public virtual ICollection<UrunKategori> AltKategoriler { get; set; }
        public virtual ICollection<Urun> Urunler { get; set; }
    }
}
