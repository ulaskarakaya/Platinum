﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.HaberEntities
{
    public class Haber : BaseEntity, IEntity
    {
        public string Ozet { get; set; }
        public string OzetEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public DateTime Tarih { get; set; }

        public int HaberKategoriId { get; set; }
        public virtual HaberKategori HaberKategori { get; set; }
    }
}
