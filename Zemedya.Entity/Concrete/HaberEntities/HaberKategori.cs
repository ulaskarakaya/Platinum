﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.HaberEntities
{
    public class HaberKategori : BaseEntity, IEntity
    {
        public HaberKategori()
        {
            this.Haberler = new HashSet<Haber>();
        }

        public virtual ICollection<Haber> Haberler { get; set; }
    }
}
