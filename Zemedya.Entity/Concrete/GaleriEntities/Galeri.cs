﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.GaleriEntities
{
    public class Galeri : BaseEntity , IEntity
    {
        public int GaleriKategoriId { get; set; }
        public virtual GaleriKategori GaleriKategori { get; set; }
        public int SiraNo { get; set; }
    }
}
