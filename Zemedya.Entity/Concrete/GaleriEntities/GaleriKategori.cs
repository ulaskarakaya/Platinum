﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.GaleriEntities
{
    public class GaleriKategori : IEntity
    {
        public GaleriKategori()
        {
            this.Galeriler = new HashSet<Galeri>();
        }
    
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string SEO { get; set; }
        public string SEOEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public bool Aktif { get; set; }
        public virtual ICollection<Galeri> Galeriler { get; set; }
    }
}
