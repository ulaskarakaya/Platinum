﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.PressEntities
{
    public class Press : IEntity
    {
        public int Id { get; set; }
        public string BaslikTR { get; set; }
        public string BaslikEN { get; set; }
        [NotMapped]
        public IFormFile GorselTRFile { get; set; }
        public string GorselTR { get; set; }
        [NotMapped]
        public IFormFile GorselENFile { get; set; }
        public string GorselEN { get; set; }
        public string IcerikTR { get; set; }
        public string IcerikEN { get; set; }
        public DateTime Tarih { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }
        public int Order { get; set; }
    }
}
