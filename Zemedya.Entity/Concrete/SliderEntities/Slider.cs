﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.SliderEntities
{
    public class Slider : BaseEntity, IEntity
    {
        public int SıraNo { get; set; }
        public string LinkBaslik { get; set; }
        public string LinkBaslikEN { get; set; }
        public string Aciklama { get; set; }
        public string AciklamaEN { get; set; }

    }
}
