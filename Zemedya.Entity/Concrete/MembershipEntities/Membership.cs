﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.MembershipEntities
{
    public class Membership : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public int Order { get; set; }
        public int Type { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        [NotMapped]
        public string ErrorMessage { get; set; }
    }
}
