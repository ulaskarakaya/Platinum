﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.AcikPozisyonEtities
{
    public class AcikPozisyon : IEntity
    {
        public int Id { get; set; }
        public string NameTr { get; set; }
        public string NameEn { get; set; }
        public string City { get; set; }
        public string DescriptionTr { get; set; }
        public string DescriptionEn { get; set; }
    }
}
