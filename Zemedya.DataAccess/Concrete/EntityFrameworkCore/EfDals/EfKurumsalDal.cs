﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.KurumsalEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfKurumsalDal : EfBaseDal<Kurumsal, ProjectContext>, IKurumsalDal
    {
        public EfKurumsalDal(ProjectContext context) : base(context) { }
    }
}
