﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.AcikPozisyonEtities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfAcikPozisyonDal : EfBaseDal<AcikPozisyon, ProjectContext>, IAcikPozisyonDal
    {
        public EfAcikPozisyonDal(ProjectContext context) : base(context) { }
    }
}
