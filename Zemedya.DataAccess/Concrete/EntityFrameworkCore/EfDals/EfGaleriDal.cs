﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.GaleriEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfGaleriDal : EfBaseDal<Galeri, ProjectContext>, IGaleriDal
    {

        public EfGaleriDal(ProjectContext context) : base(context) { }
    }
}
