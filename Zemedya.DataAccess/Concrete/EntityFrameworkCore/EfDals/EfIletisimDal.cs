﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.IletisimEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfIletisimDal : EfBaseDal<Iletisim, ProjectContext>, IIletisimDal
    {
        public EfIletisimDal(ProjectContext context) : base(context) { }
    }
}
