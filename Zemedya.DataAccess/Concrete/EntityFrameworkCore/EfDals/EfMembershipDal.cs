﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.MembershipEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfMembershipDal : EfBaseDal<Membership, ProjectContext>, IMembershipDal
    {
        public EfMembershipDal(ProjectContext context) : base(context) { }
    }
}

