﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.Team;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfTeamCategoryDal : EfBaseDal<TeamCategory, ProjectContext>, ITeamCategoryDal
    {
        public EfTeamCategoryDal(ProjectContext context) : base(context) { }
    }
}
