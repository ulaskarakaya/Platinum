﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.EtkinlikEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfEtkinlikDal : EfBaseDal<Etkinlik, ProjectContext>, IEtkinlikDal
    {
        public EfEtkinlikDal(ProjectContext context) : base(context) { }
    }
}
