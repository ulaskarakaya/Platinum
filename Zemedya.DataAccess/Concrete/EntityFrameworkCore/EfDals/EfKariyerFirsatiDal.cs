﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.KariyerFirsatlariEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfKariyerFirsatiDal : EfBaseDal<KariyerFirsati, ProjectContext>, IKariyerFirsatiDal
    {
        public EfKariyerFirsatiDal(ProjectContext context) : base(context)
        {

        }
    }
}
