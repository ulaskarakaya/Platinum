﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Abstract;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfBaseDal<TEntity, TContext> : IBaseDal<TEntity> 
        where TEntity : class, IEntity, new() 
        where TContext : DbContext, new()
    {
        protected TContext _context;
        public EfBaseDal(TContext context)
        {
            _context = context;
        }

        public TEntity Getir(Expression<Func<TEntity, bool>> filter)
        {
            return _context.Set<TEntity>().FirstOrDefault(filter);
        }

        public void Ekle(TEntity entity)
        {
            var addedEntity = _context.Entry(entity);
            addedEntity.State = EntityState.Added;
        }

        public void Guncelle(TEntity entity)
        {
            var updatedEntity = _context.Entry(entity);
            updatedEntity.State = EntityState.Modified;
        }

        public IEnumerable<TEntity> HepsiniGetir(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter == null
                ? _context.Set<TEntity>()
                : _context.Set<TEntity>().Where(filter);
        }

        public void KaliciSil(Expression<Func<TEntity, bool>> filter)
        {
            var Entity = _context.Set<TEntity>().FirstOrDefault(filter);
            var deletedEntity = _context.Entry(Entity);
            deletedEntity.State = EntityState.Deleted;
        }

        public void PasifYap(Expression<Func<TEntity, bool>> filter)
        {
            var deletedEntity = _context.Set<TEntity>().FirstOrDefault(filter);
            var propertie = deletedEntity.GetType().GetProperty("Aktif");
            propertie.SetValue(deletedEntity, false);
        }
    }
}
