﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.HaberEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfHaberDal : EfBaseDal<Haber, ProjectContext>, IHaberDal
    {
        public EfHaberDal(ProjectContext context) : base(context) { }
        
    }
}
