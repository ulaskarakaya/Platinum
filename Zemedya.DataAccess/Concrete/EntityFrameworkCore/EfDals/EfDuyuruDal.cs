﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.DuyuruEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfDuyuruDal : EfBaseDal<Duyuru, ProjectContext>, IDuyuruDal
    {
        public EfDuyuruDal(ProjectContext context) : base(context) { }
    }
}
