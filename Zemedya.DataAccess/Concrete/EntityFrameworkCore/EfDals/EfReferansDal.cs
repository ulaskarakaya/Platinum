﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.ReferansEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfReferansDal : EfBaseDal<Referans, ProjectContext>, IReferansDal
    {
        public EfReferansDal(ProjectContext context) : base(context) { }
    }
}
