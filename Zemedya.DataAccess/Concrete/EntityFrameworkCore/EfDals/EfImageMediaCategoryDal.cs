﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfImageMediaCategoryDal : EfBaseDal<ImageMediaCategory, ProjectContext>, IImageMediaCategoryDal
    {
        private readonly DbContext _context;
        public EfImageMediaCategoryDal(ProjectContext context) : base(context)
        {
            _context = context;
        }

        public void ItemAdd(ImageMediaItem model)
        {
            var addedEntity = _context.Entry(model);
            addedEntity.State = EntityState.Added;
        }

        public ImageMediaItem ItemGet(Expression<Func<ImageMediaItem, bool>> filter)
        {
            return _context.Set<ImageMediaItem>().FirstOrDefault(filter);
        }

        public void ItemSil(Expression<Func<ImageMediaItem, bool>> filter)
        {
            var Entity = _context.Set<ImageMediaItem>().FirstOrDefault(filter);
            var deletedEntity = _context.Entry(Entity);
            deletedEntity.State = EntityState.Deleted;

        }

        public void ItemUpdate(ImageMediaItem model)
        {
            var updatedEntity = _context.Entry(model);
            updatedEntity.State = EntityState.Modified;
        }
    }
}
