﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.SliderEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfSliderDal : EfBaseDal<Slider, ProjectContext>, ISliderDal
    {
        public EfSliderDal(ProjectContext context) : base(context)
        {

        }
    }
}
