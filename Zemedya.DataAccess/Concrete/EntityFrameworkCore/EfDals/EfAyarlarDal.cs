﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.AyarEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfAyarlarDal : EfBaseDal<Ayarlar, ProjectContext>, IAyarlarDal
    {
        public EfAyarlarDal(ProjectContext context) : base(context) { }
    }
}
