﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.PressEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfPressDal : EfBaseDal<Press, ProjectContext>, IPressDal
    {
        public EfPressDal(ProjectContext context) : base(context)
        {

        }
    }
}
