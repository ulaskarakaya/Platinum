﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfVideoMediaCategoryDal : EfBaseDal<VideoMediaCategory, ProjectContext>, IVideoMediaCategoryDal
    {
        private readonly DbContext _context;
        public EfVideoMediaCategoryDal(ProjectContext context) : base(context)
        {
            _context = context;
        }

        public void ItemAdd(VideoMediaItem model)
        {
            var addedEntity = _context.Entry(model);
            addedEntity.State = EntityState.Added;
        }

        public VideoMediaItem ItemGet(Expression<Func<VideoMediaItem, bool>> filter)
        {
            return _context.Set<VideoMediaItem>().FirstOrDefault(filter);
        }

        public void ItemSil(Expression<Func<VideoMediaItem, bool>> filter)
        {
            var Entity = _context.Set<VideoMediaItem>().FirstOrDefault(filter);
            var deletedEntity = _context.Entry(Entity);
            deletedEntity.State = EntityState.Deleted;

        }

        public void ItemUpdate(VideoMediaItem model)
        {
            var updatedEntity = _context.Entry(model);
            updatedEntity.State = EntityState.Modified;
        }
    }
}
