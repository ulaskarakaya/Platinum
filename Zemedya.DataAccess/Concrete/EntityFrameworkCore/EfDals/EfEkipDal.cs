﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.EkipEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfEkipDal : EfBaseDal<Ekip, ProjectContext>, IEkipDal
    {
        public EfEkipDal(ProjectContext context) : base(context) { }
    }
}
