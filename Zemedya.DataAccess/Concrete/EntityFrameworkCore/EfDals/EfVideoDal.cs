﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.VideoEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfVideoDal : EfBaseDal<Video, ProjectContext>, IVideoDal
    {
        public EfVideoDal(ProjectContext context) : base(context) { }
    }
}
