﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfMevcutSayfalarDal : EfBaseDal<MevcutSayfalar, ProjectContext>, IMevcutSayfalarDal
    {
        public EfMevcutSayfalarDal(ProjectContext context) : base(context) { }
    }
}
