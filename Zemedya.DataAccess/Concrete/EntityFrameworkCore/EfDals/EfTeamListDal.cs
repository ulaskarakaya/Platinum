﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.Team;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfTeamListDal : EfBaseDal<TeamList, ProjectContext>, ITeamListDal
    {
        public EfTeamListDal(ProjectContext context) : base(context) { }
    }
}
