﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.MevcutDillerEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfMevcutDillerDal : EfBaseDal<MevcutDiller, ProjectContext>, IMevcutDillerDal
    {
        public EfMevcutDillerDal(ProjectContext context) : base(context) { }
    }
}
