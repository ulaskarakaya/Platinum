﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.AyarEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.AyarlarMaps
{
    public class AyarlarMap : IEntityTypeConfiguration<Ayarlar>
    {
        

        public void Configure(EntityTypeBuilder<Ayarlar> builder)
        {
            builder.ToTable("Ayarlar");
            builder.HasKey(x => x.Id);
        }
    }
}
