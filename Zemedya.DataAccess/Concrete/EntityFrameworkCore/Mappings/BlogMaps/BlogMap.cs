﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.BlogMaps
{
    public class BlogMap : IEntityTypeConfiguration<Blog>
    {
        public void Configure(EntityTypeBuilder<Blog> builder)
        {
            builder.ToTable("Bloglar");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.Isim).HasMaxLength(500);
            builder.Property(x => x.IsimEN).HasMaxLength(500);
            
        }
    }
}
