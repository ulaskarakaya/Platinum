﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.PressEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.PressMaps
{
    public class PressMap : IEntityTypeConfiguration<Press>
    {
        public void Configure(EntityTypeBuilder<Press> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.BaslikTR).IsRequired().HasMaxLength(100);
            builder.Property(x => x.BaslikEN).IsRequired().HasMaxLength(100);
            builder.Property(x => x.GorselTR).IsRequired().HasMaxLength(100);
            builder.Property(x => x.GorselEN).IsRequired().HasMaxLength(100);
        }
    }
}
