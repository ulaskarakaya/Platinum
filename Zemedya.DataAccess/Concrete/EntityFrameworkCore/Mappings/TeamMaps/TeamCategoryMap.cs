﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.Team;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.TeamMaps
{
    public class TeamCategoryMap : IEntityTypeConfiguration<TeamCategory>
    {
        public void Configure(EntityTypeBuilder<TeamCategory> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.TeamNameTr).IsRequired().HasMaxLength(100);
            builder.Property(x => x.TeamNameEn).IsRequired().HasMaxLength(100);
            builder.Property(x => x.TeamSeoTr).IsRequired().HasMaxLength(300);
            builder.Property(x => x.TeamSeoEn).IsRequired().HasMaxLength(300);
        }
    }
}
