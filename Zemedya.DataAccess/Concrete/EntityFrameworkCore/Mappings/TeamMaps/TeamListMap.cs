﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.Team;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.TeamMaps
{
    public class TeamListMap : IEntityTypeConfiguration<TeamList>
    {
        public void Configure(EntityTypeBuilder<TeamList> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.NameSurname).IsRequired().HasMaxLength(100);
            builder.Property(x => x.Image).IsRequired().HasMaxLength(100);
            builder.Property(x => x.TitleTr).IsRequired().HasMaxLength(100);
            builder.Property(x => x.TitleEn).IsRequired().HasMaxLength(100);
        }
    }
}
