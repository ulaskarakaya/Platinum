﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.MediaMaps
{
    public class VideoMediaCategoryMap : IEntityTypeConfiguration<VideoMediaCategory>
    {
        public void Configure(EntityTypeBuilder<VideoMediaCategory> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.NameTR).HasMaxLength(150);
            builder.Property(x => x.NameEN).HasMaxLength(150);
            builder.Property(x => x.Image).HasMaxLength(50);
        }
    }
}
