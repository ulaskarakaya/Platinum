﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.MediaMaps
{
    public class ImageMediaItemMap : IEntityTypeConfiguration<ImageMediaItem>
    {
        public void Configure(EntityTypeBuilder<ImageMediaItem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Image).HasMaxLength(50);
        }
    }
}
