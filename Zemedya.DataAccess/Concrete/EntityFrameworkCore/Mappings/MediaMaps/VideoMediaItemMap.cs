﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.MediaMaps
{
    public class VideoMediaItemMap : IEntityTypeConfiguration<VideoMediaItem>
    {
        public void Configure(EntityTypeBuilder<VideoMediaItem> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}
