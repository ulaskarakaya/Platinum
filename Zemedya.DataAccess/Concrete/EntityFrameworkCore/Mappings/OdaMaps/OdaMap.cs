﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.OdaEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.OdaMaps
{
    public class OdaMap : IEntityTypeConfiguration<Oda>
    {
        public void Configure(EntityTypeBuilder<Oda> builder)
        {
            builder.ToTable("Odalar");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
        }
    }
}
