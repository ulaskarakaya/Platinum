﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.PopupEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.PopupMaps
{
    public class PopupMap : IEntityTypeConfiguration<Popup>
    {
        public void Configure(EntityTypeBuilder<Popup> builder)
        {
            builder.ToTable("Popuplar");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.Isim).HasMaxLength(500);
            builder.Property(x => x.IsimEN).HasMaxLength(500);
        }
    }
}
