﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.ReferansEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.ReferansMaps
{
    public class ReferansMap : IEntityTypeConfiguration<Referans>
    {
        public void Configure(EntityTypeBuilder<Referans> builder)
        {
            builder.ToTable("Referanslar");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
        }
    }
}
