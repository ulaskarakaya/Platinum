﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.GaleriEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.GaleriMaps
{
    public class GaleriMap : IEntityTypeConfiguration<Galeri>
    {
        public void Configure(EntityTypeBuilder<Galeri> builder)
        {
            builder.ToTable("Galeriler");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.Isim).HasMaxLength(500);
            builder.Property(x => x.IsimEN).HasMaxLength(500);
        }
    }
}
