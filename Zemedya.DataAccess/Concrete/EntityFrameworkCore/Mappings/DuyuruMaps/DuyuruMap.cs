﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.DuyuruEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.DuyuruMaps
{
    public class DuyuruMap : IEntityTypeConfiguration<Duyuru>
    {
        public void Configure(EntityTypeBuilder<Duyuru> builder)
        {
            builder.ToTable("Duyurular");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
        }
    }
}
