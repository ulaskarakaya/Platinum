﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.EtkinlikEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.EtkinlikMaps
{
    public class EtkinlikMap : IEntityTypeConfiguration<Etkinlik>
    {
        public void Configure(EntityTypeBuilder<Etkinlik> builder)
        {
            builder.ToTable("Etkinlikler");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
        }
    }
}

