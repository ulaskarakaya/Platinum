﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.UrunEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.UrunMaps
{
    public class UrunKategoriMap : IEntityTypeConfiguration<UrunKategori>
    {
        public void Configure(EntityTypeBuilder<UrunKategori> builder)
        {
            builder.ToTable("UrunKategorileri");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.Isim).HasMaxLength(500);
            builder.Property(x => x.IsimEN).HasMaxLength(500);
        }
    }
}
