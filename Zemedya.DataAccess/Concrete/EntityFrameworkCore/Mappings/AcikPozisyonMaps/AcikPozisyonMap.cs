﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.AcikPozisyonEtities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.AcikPozisyonMaps
{
    public class AcikPozisyonMap : IEntityTypeConfiguration<AcikPozisyon>
    {
        public void Configure(EntityTypeBuilder<AcikPozisyon> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.NameTr).HasMaxLength(300);
            builder.Property(x => x.NameEn).HasMaxLength(300);
            builder.Property(x => x.City).HasMaxLength(100);
        }
    }
}
