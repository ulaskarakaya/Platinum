﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.KurumsalEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.KurumsalMaps
{
    public class KurumsalMap : IEntityTypeConfiguration<Kurumsal>
    {
        public void Configure(EntityTypeBuilder<Kurumsal> builder)
        {
            builder.ToTable("KurumsalListesi");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.Isim).HasMaxLength(500);
            builder.Property(x => x.IsimEN).HasMaxLength(500);
        }
    }
}
