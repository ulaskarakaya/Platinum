﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.AyarEntities;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Entity.Concrete.MevcutDillerEntities;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;
using Zemedya.Entity.Concrete.PopupEntities;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Entity.Concrete.YoneticiEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.SeedData
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HaberKategori>().HasData(
                new BlogKategori { Id = 1, Isim = "Genel" }
            );
            modelBuilder.Entity<UrunKategori>().HasData(
                new UrunKategori { Id = 1, Isim = "Ana Kategori", Aktif = true, UstKategoriId = 1 }
            );
            modelBuilder.Entity<MevcutDiller>().HasData(
                new MevcutDiller { Id = 1, TR = true }
            );
            modelBuilder.Entity<MevcutSayfalar>().HasData(
                new MevcutSayfalar { Id = 1 }
            );
            modelBuilder.Entity<Ayarlar>().HasData(
                new Ayarlar { Id = 1, Description= "Zemedya" }
            );
            modelBuilder.Entity<Popup>().HasData(
                new Popup { Id = 1 }
            );
           
            modelBuilder.Entity<BlogKategori>().HasData(
                new BlogKategori { Id = 1, Isim = "Ana Kategori" }
            );
        }
    }
}
