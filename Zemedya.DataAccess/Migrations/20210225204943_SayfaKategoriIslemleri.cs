﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class SayfaKategoriIslemleri : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "BlogKategori",
                table: "MevcutSayfalar",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "GaleriKategori",
                table: "MevcutSayfalar",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UrunKategori",
                table: "MevcutSayfalar",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BlogKategori",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "GaleriKategori",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "UrunKategori",
                table: "MevcutSayfalar");
        }
    }
}
