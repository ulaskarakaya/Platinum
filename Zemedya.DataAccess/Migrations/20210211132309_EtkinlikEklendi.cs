﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class EtkinlikEklendi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Etkinlikler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(nullable: true),
                    IsimEN = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    Ozet = table.Column<string>(nullable: true),
                    OzetEN = table.Column<string>(nullable: true),
                    Detay = table.Column<string>(nullable: true),
                    DetayEN = table.Column<string>(nullable: true),
                    Tarih = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Etkinlikler", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Etkinlikler");
        }
    }
}
