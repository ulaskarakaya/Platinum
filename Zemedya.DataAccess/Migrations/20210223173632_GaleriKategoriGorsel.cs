﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class GaleriKategoriGorsel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Gorsel",
                table: "GaleriKategorileri",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GorselEN",
                table: "GaleriKategorileri",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gorsel",
                table: "GaleriKategorileri");

            migrationBuilder.DropColumn(
                name: "GorselEN",
                table: "GaleriKategorileri");
        }
    }
}
