﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class _0906221159 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "acikpozisyonlar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NameTr = table.Column<string>(maxLength: 300, nullable: true),
                    NameEn = table.Column<string>(maxLength: 300, nullable: true),
                    City = table.Column<string>(maxLength: 100, nullable: true),
                    DescriptionTr = table.Column<string>(nullable: true),
                    DescriptionEn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_acikpozisyonlar", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "acikpozisyonlar");
        }
    }
}
