﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class SliderAciklama : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Aciklama",
                table: "Sliderlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AciklamaEN",
                table: "Sliderlar",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Aciklama",
                table: "Sliderlar");

            migrationBuilder.DropColumn(
                name: "AciklamaEN",
                table: "Sliderlar");
        }
    }
}
