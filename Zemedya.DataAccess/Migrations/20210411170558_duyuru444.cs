﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class duyuru444 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AnaSayfaBaslik",
                table: "Duyurular",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AnaSayfaBaslikEN",
                table: "Duyurular",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AnaSayfaGorsel",
                table: "Duyurular",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AnaSayfaGorselEN",
                table: "Duyurular",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnaSayfaBaslik",
                table: "Duyurular");

            migrationBuilder.DropColumn(
                name: "AnaSayfaBaslikEN",
                table: "Duyurular");

            migrationBuilder.DropColumn(
                name: "AnaSayfaGorsel",
                table: "Duyurular");

            migrationBuilder.DropColumn(
                name: "AnaSayfaGorselEN",
                table: "Duyurular");
        }
    }
}
