﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class duyuru455343 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Detay2",
                table: "Duyurular",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Detay2EN",
                table: "Duyurular",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GaleriKategoriId",
                table: "Duyurular",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Detay2",
                table: "Duyurular");

            migrationBuilder.DropColumn(
                name: "Detay2EN",
                table: "Duyurular");

            migrationBuilder.DropColumn(
                name: "GaleriKategoriId",
                table: "Duyurular");
        }
    }
}
