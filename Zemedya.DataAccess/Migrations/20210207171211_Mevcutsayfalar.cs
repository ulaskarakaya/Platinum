﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class Mevcutsayfalar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UrunKategorileri_AnaKategoriler_AnaKategoriId",
                table: "UrunKategorileri");

            migrationBuilder.DropTable(
                name: "AnaKategoriler");

            migrationBuilder.DropIndex(
                name: "IX_UrunKategorileri_AnaKategoriId",
                table: "UrunKategorileri");

            migrationBuilder.DropColumn(
                name: "AnaKategoriId",
                table: "UrunKategorileri");

            migrationBuilder.AddColumn<int>(
                name: "UstKategoriId",
                table: "UrunKategorileri",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UstKategorisiId",
                table: "UrunKategorileri",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MevcutSayfalar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Haberler = table.Column<bool>(nullable: false),
                    Blog = table.Column<bool>(nullable: false),
                    Iletisim = table.Column<bool>(nullable: false),
                    Kurumsal = table.Column<bool>(nullable: false),
                    Galeri = table.Column<bool>(nullable: false),
                    Popup = table.Column<bool>(nullable: false),
                    Slider = table.Column<bool>(nullable: false),
                    Urunler = table.Column<bool>(nullable: false),
                    Yonetici = table.Column<bool>(nullable: false),
                    Projeler = table.Column<bool>(nullable: false),
                    Basin = table.Column<bool>(nullable: false),
                    Bayi = table.Column<bool>(nullable: false),
                    Partner = table.Column<bool>(nullable: false),
                    Video = table.Column<bool>(nullable: false),
                    Hizmetler = table.Column<bool>(nullable: false),
                    Mevzuat = table.Column<bool>(nullable: false),
                    Ekip = table.Column<bool>(nullable: false),
                    InsanKaynaklari = table.Column<bool>(nullable: false),
                    Referans = table.Column<bool>(nullable: false),
                    Ayarlar = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MevcutSayfalar", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri",
                column: "UstKategorisiId");

            migrationBuilder.AddForeignKey(
                name: "FK_UrunKategorileri_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri",
                column: "UstKategorisiId",
                principalTable: "UrunKategorileri",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UrunKategorileri_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri");

            migrationBuilder.DropTable(
                name: "MevcutSayfalar");

            migrationBuilder.DropIndex(
                name: "IX_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri");

            migrationBuilder.DropColumn(
                name: "UstKategoriId",
                table: "UrunKategorileri");

            migrationBuilder.DropColumn(
                name: "UstKategorisiId",
                table: "UrunKategorileri");

            migrationBuilder.AddColumn<int>(
                name: "AnaKategoriId",
                table: "UrunKategorileri",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "AnaKategoriler",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Aktif = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Gorsel = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    GorselEN = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Isim = table.Column<string>(type: "varchar(50) CHARACTER SET utf8mb4", maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(type: "varchar(50) CHARACTER SET utf8mb4", maxLength: 50, nullable: true),
                    Link = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    LinkEN = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    SEO = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    SEOEN = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnaKategoriler", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UrunKategorileri_AnaKategoriId",
                table: "UrunKategorileri",
                column: "AnaKategoriId");

            migrationBuilder.AddForeignKey(
                name: "FK_UrunKategorileri_AnaKategoriler_AnaKategoriId",
                table: "UrunKategorileri",
                column: "AnaKategoriId",
                principalTable: "AnaKategoriler",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
