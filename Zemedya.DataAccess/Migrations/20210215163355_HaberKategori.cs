﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class HaberKategori : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BlogKategorileri",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.InsertData(
                table: "HaberKategorileri",
                columns: new[] { "Id", "Aktif", "Gorsel", "GorselEN", "Isim", "IsimEN", "Link", "LinkEN", "SEO", "SEOEN" },
                values: new object[] { 1, false, null, null, "Genel", null, null, null, null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "HaberKategorileri",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.InsertData(
                table: "BlogKategorileri",
                columns: new[] { "Id", "Aktif", "Gorsel", "GorselEN", "Isim", "IsimEN", "SEO", "SEOEN" },
                values: new object[] { 1, false, null, null, "Genel", null, null, null });
        }
    }
}
