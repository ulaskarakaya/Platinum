﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class GaleriKategoriSEO : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SEO",
                table: "GaleriKategorileri",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SEOEN",
                table: "GaleriKategorileri",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SEO",
                table: "GaleriKategorileri");

            migrationBuilder.DropColumn(
                name: "SEOEN",
                table: "GaleriKategorileri");
        }
    }
}
