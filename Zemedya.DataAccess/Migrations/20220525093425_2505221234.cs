﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class _2505221234 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "yoneticiler",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "FacebookOrder",
                table: "ayarlar",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "InstagramOrder",
                table: "ayarlar",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LinkedinOrder",
                table: "ayarlar",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TwitterOrder",
                table: "ayarlar",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Youtube",
                table: "ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "YoutubeOrder",
                table: "ayarlar",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FacebookOrder",
                table: "ayarlar");

            migrationBuilder.DropColumn(
                name: "InstagramOrder",
                table: "ayarlar");

            migrationBuilder.DropColumn(
                name: "LinkedinOrder",
                table: "ayarlar");

            migrationBuilder.DropColumn(
                name: "TwitterOrder",
                table: "ayarlar");

            migrationBuilder.DropColumn(
                name: "Youtube",
                table: "ayarlar");

            migrationBuilder.DropColumn(
                name: "YoutubeOrder",
                table: "ayarlar");

            migrationBuilder.InsertData(
                table: "yoneticiler",
                columns: new[] { "Id", "Ad", "Aktif", "Email", "KullaniciAdi", "Sifre", "Soyad" },
                values: new object[] { 1, null, true, null, "admin", "123456", null });
        }
    }
}
