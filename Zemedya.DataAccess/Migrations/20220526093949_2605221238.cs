﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class _2605221238 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "takimkategorilistesi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TeamNameTr = table.Column<string>(maxLength: 100, nullable: false),
                    TeamNameEn = table.Column<string>(maxLength: 100, nullable: false),
                    TeamSeoTr = table.Column<string>(maxLength: 300, nullable: false),
                    TeamSeoEn = table.Column<string>(maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_takimkategorilistesi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "takimlistesi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NameSurname = table.Column<string>(maxLength: 100, nullable: false),
                    Image = table.Column<string>(maxLength: 100, nullable: false),
                    TitleTr = table.Column<string>(maxLength: 100, nullable: false),
                    TitleEn = table.Column<string>(maxLength: 100, nullable: false),
                    TeamCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_takimlistesi", x => x.Id);
                    table.ForeignKey(
                        name: "FK_takimlistesi_takimkategorilistesi_TeamCategoryId",
                        column: x => x.TeamCategoryId,
                        principalTable: "takimkategorilistesi",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_takimlistesi_TeamCategoryId",
                table: "takimlistesi",
                column: "TeamCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "takimlistesi");

            migrationBuilder.DropTable(
                name: "takimkategorilistesi");
        }
    }
}
