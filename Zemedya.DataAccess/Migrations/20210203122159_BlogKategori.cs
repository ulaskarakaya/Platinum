﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class BlogKategori : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Aktif",
                table: "BlogKategorileri",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Gorsel",
                table: "BlogKategorileri",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GorselEN",
                table: "BlogKategorileri",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SEO",
                table: "BlogKategorileri",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SEOEN",
                table: "BlogKategorileri",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Aktif",
                table: "BlogKategorileri");

            migrationBuilder.DropColumn(
                name: "Gorsel",
                table: "BlogKategorileri");

            migrationBuilder.DropColumn(
                name: "GorselEN",
                table: "BlogKategorileri");

            migrationBuilder.DropColumn(
                name: "SEO",
                table: "BlogKategorileri");

            migrationBuilder.DropColumn(
                name: "SEOEN",
                table: "BlogKategorileri");
        }
    }
}
