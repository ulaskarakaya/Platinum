﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class DilTablosu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Basin",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Bayi",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Ekip",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Galeri",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Hizmetler",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "InsanKaynaklari",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Mevzuat",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Partner",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Projeler",
                table: "MevcutSayfalar");

            migrationBuilder.AddColumn<bool>(
                name: "Duyuru",
                table: "MevcutSayfalar",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Etkinlik",
                table: "MevcutSayfalar",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "MevcutDiller",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TR = table.Column<bool>(nullable: false),
                    EN = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MevcutDiller", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "MevcutDiller",
                columns: new[] { "Id", "EN", "TR" },
                values: new object[] { 1, false, false });

            migrationBuilder.InsertData(
                table: "MevcutSayfalar",
                columns: new[] { "Id", "Ayarlar", "Blog", "Duyuru", "Etkinlik", "Haberler", "Iletisim", "Kurumsal", "Popup", "Referans", "Slider", "Urunler", "Video", "Yonetici" },
                values: new object[] { 1, false, false, false, false, false, false, false, false, false, false, false, false, false });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MevcutDiller");

            migrationBuilder.DeleteData(
                table: "MevcutSayfalar",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "Duyuru",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Etkinlik",
                table: "MevcutSayfalar");

            migrationBuilder.AddColumn<bool>(
                name: "Basin",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Bayi",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Ekip",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Galeri",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Hizmetler",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "InsanKaynaklari",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Mevzuat",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Partner",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Projeler",
                table: "MevcutSayfalar",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }
    }
}
