﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class _0806220916 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "resimmedyakategori",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NameTR = table.Column<string>(maxLength: 150, nullable: true),
                    NameEN = table.Column<string>(maxLength: 150, nullable: true),
                    Image = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_resimmedyakategori", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "resimmediaoge",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Image = table.Column<string>(maxLength: 50, nullable: true),
                    DescriptionTR = table.Column<string>(nullable: true),
                    DescriptionEN = table.Column<string>(nullable: true),
                    ImageMediaCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_resimmediaoge", x => x.Id);
                    table.ForeignKey(
                        name: "FK_resimmediaoge_resimmedyakategori_ImageMediaCategoryId",
                        column: x => x.ImageMediaCategoryId,
                        principalTable: "resimmedyakategori",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_resimmediaoge_ImageMediaCategoryId",
                table: "resimmediaoge",
                column: "ImageMediaCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "resimmediaoge");

            migrationBuilder.DropTable(
                name: "resimmedyakategori");
        }
    }
}
