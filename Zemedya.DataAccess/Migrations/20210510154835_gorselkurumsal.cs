﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class gorselkurumsal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Detay",
                table: "Ekiplistesi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DetayEN",
                table: "Ekiplistesi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GorselEN",
                table: "Ekiplistesi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Isim",
                table: "Ekiplistesi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IsimEN",
                table: "Ekiplistesi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SEO",
                table: "Ekiplistesi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SEOEN",
                table: "Ekiplistesi",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Detay",
                table: "Ekiplistesi");

            migrationBuilder.DropColumn(
                name: "DetayEN",
                table: "Ekiplistesi");

            migrationBuilder.DropColumn(
                name: "GorselEN",
                table: "Ekiplistesi");

            migrationBuilder.DropColumn(
                name: "Isim",
                table: "Ekiplistesi");

            migrationBuilder.DropColumn(
                name: "IsimEN",
                table: "Ekiplistesi");

            migrationBuilder.DropColumn(
                name: "SEO",
                table: "Ekiplistesi");

            migrationBuilder.DropColumn(
                name: "SEOEN",
                table: "Ekiplistesi");
        }
    }
}
