﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AnaKategoriler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnaKategoriler", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BlogKategorileri",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogKategorileri", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GaleriKategorileri",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GaleriKategorileri", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HaberKategorileri",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HaberKategorileri", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Iletisimler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Baslik = table.Column<string>(maxLength: 100, nullable: true),
                    BaslikEN = table.Column<string>(maxLength: 100, nullable: true),
                    Adres = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Telefon = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    HaritaKod = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iletisimler", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KurumsalListesi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 100, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 100, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KurumsalListesi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Popuplar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Popuplar", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sliderlar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sliderlar", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Yoneticiler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Ad = table.Column<string>(maxLength: 25, nullable: true),
                    Soyad = table.Column<string>(maxLength: 25, nullable: true),
                    Email = table.Column<string>(maxLength: 40, nullable: true),
                    KullaniciAdi = table.Column<string>(maxLength: 30, nullable: true),
                    Sifre = table.Column<string>(maxLength: 30, nullable: true),
                    Aktif = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Yoneticiler", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UrunKategorileri",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    AnaKategoriId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UrunKategorileri", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UrunKategorileri_AnaKategoriler_AnaKategoriId",
                        column: x => x.AnaKategoriId,
                        principalTable: "AnaKategoriler",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bloglar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    BlogKategoriId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bloglar", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bloglar_BlogKategorileri_BlogKategoriId",
                        column: x => x.BlogKategoriId,
                        principalTable: "BlogKategorileri",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Galeriler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    GaleriKategoriId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Galeriler", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Galeriler_GaleriKategorileri_GaleriKategoriId",
                        column: x => x.GaleriKategoriId,
                        principalTable: "GaleriKategorileri",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Haberler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 100, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 100, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    Ozet = table.Column<string>(nullable: true),
                    OzetEN = table.Column<string>(nullable: true),
                    Detay = table.Column<string>(nullable: true),
                    DetayEN = table.Column<string>(nullable: true),
                    Tarih = table.Column<DateTime>(nullable: false),
                    HaberKategoriId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Haberler", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Haberler_HaberKategorileri_HaberKategoriId",
                        column: x => x.HaberKategoriId,
                        principalTable: "HaberKategorileri",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Urunler",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(maxLength: 50, nullable: true),
                    IsimEN = table.Column<string>(maxLength: 50, nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    Kod = table.Column<string>(nullable: true),
                    Ozet = table.Column<string>(nullable: true),
                    OzetEN = table.Column<string>(nullable: true),
                    Detay = table.Column<string>(nullable: true),
                    DetayEN = table.Column<string>(nullable: true),
                    Gorsel_2 = table.Column<string>(nullable: true),
                    Gorsel_2EN = table.Column<string>(nullable: true),
                    Gorsel_3 = table.Column<string>(nullable: true),
                    Gorsel_3EN = table.Column<string>(nullable: true),
                    Gorsel_4 = table.Column<string>(nullable: true),
                    Gorsel_4EN = table.Column<string>(nullable: true),
                    Gorsel_5 = table.Column<string>(nullable: true),
                    Gorsel_5EN = table.Column<string>(nullable: true),
                    Gorsel_6 = table.Column<string>(nullable: true),
                    Gorsel_6EN = table.Column<string>(nullable: true),
                    UrunKategoriId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Urunler", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Urunler_UrunKategorileri_UrunKategoriId",
                        column: x => x.UrunKategoriId,
                        principalTable: "UrunKategorileri",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bloglar_BlogKategoriId",
                table: "Bloglar",
                column: "BlogKategoriId");

            migrationBuilder.CreateIndex(
                name: "IX_Galeriler_GaleriKategoriId",
                table: "Galeriler",
                column: "GaleriKategoriId");

            migrationBuilder.CreateIndex(
                name: "IX_Haberler_HaberKategoriId",
                table: "Haberler",
                column: "HaberKategoriId");

            migrationBuilder.CreateIndex(
                name: "IX_UrunKategorileri_AnaKategoriId",
                table: "UrunKategorileri",
                column: "AnaKategoriId");

            migrationBuilder.CreateIndex(
                name: "IX_Urunler_UrunKategoriId",
                table: "Urunler",
                column: "UrunKategoriId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bloglar");

            migrationBuilder.DropTable(
                name: "Galeriler");

            migrationBuilder.DropTable(
                name: "Haberler");

            migrationBuilder.DropTable(
                name: "Iletisimler");

            migrationBuilder.DropTable(
                name: "KurumsalListesi");

            migrationBuilder.DropTable(
                name: "Popuplar");

            migrationBuilder.DropTable(
                name: "Sliderlar");

            migrationBuilder.DropTable(
                name: "Urunler");

            migrationBuilder.DropTable(
                name: "Yoneticiler");

            migrationBuilder.DropTable(
                name: "BlogKategorileri");

            migrationBuilder.DropTable(
                name: "GaleriKategorileri");

            migrationBuilder.DropTable(
                name: "HaberKategorileri");

            migrationBuilder.DropTable(
                name: "UrunKategorileri");

            migrationBuilder.DropTable(
                name: "AnaKategoriler");
        }
    }
}
