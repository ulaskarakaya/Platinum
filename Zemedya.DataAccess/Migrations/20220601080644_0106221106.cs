﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class _0106221106 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "uyelikler",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Order",
                table: "uyelikler");
        }
    }
}
