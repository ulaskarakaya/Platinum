﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class SeedDataAnaKategori : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BlogKategorileri",
                columns: new[] { "Id", "Aktif", "Gorsel", "GorselEN", "Isim", "IsimEN", "SEO", "SEOEN" },
                values: new object[] { 1, false, null, null, "Ana Kategori", null, null, null });


            migrationBuilder.UpdateData(
                table: "Yoneticiler",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Email", "KullaniciAdi" },
                values: new object[] { null, "admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BlogKategorileri",
                keyColumn: "Id",
                keyValue: 1);

            
            migrationBuilder.UpdateData(
                table: "Yoneticiler",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Email", "KullaniciAdi" },
                values: new object[] { "admin", null });
        }
    }
}
