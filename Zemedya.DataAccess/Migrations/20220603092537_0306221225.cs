﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class _0306221225 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "basin",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    BaslikTR = table.Column<string>(maxLength: 100, nullable: false),
                    BaslikEN = table.Column<string>(maxLength: 100, nullable: false),
                    GorselTR = table.Column<string>(maxLength: 100, nullable: false),
                    GorselEN = table.Column<string>(maxLength: 100, nullable: false),
                    IcerikTR = table.Column<string>(nullable: true),
                    IcerikEN = table.Column<string>(nullable: true),
                    Tarih = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_basin", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "basin");
        }
    }
}
