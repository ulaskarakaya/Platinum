﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class etkinlik222 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Detay2",
                table: "Etkinlikler",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Detay2EN",
                table: "Etkinlikler",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gorsel2",
                table: "Etkinlikler",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gorsel2EN",
                table: "Etkinlikler",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Detay2",
                table: "Etkinlikler");

            migrationBuilder.DropColumn(
                name: "Detay2EN",
                table: "Etkinlikler");

            migrationBuilder.DropColumn(
                name: "Gorsel2",
                table: "Etkinlikler");

            migrationBuilder.DropColumn(
                name: "Gorsel2EN",
                table: "Etkinlikler");
        }
    }
}
