﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class LinkBaslikSlider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LinkBaslik",
                table: "Sliderlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LinkBaslikEN",
                table: "Sliderlar",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinkBaslik",
                table: "Sliderlar");

            migrationBuilder.DropColumn(
                name: "LinkBaslikEN",
                table: "Sliderlar");
        }
    }
}
