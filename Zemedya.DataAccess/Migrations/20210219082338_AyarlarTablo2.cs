﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class AyarlarTablo2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CanlıDestekKodu",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionEN",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GoogleAnalyticsKodu",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GoogleDoğrulamaKodu",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecaptchaSecretKey",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecaptchaSiteKey",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SiteTitle",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SiteTitleEN",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SiteUrl",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "WhatsappDurum",
                table: "Ayarlar",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "WhatsappMesaj",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WhatsappTelefon",
                table: "Ayarlar",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CanlıDestekKodu",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "DescriptionEN",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "GoogleAnalyticsKodu",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "GoogleDoğrulamaKodu",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "RecaptchaSecretKey",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "RecaptchaSiteKey",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "SiteTitle",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "SiteTitleEN",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "SiteUrl",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "WhatsappDurum",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "WhatsappMesaj",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "WhatsappTelefon",
                table: "Ayarlar");
        }
    }
}
