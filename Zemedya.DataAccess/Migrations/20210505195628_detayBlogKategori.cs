﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class detayBlogKategori : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.AddColumn<string>(
                name: "Detay",
                table: "blogkategorileri",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DetayEN",
                table: "blogkategorileri",
                nullable: true);

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.DropColumn(
                name: "Detay",
                table: "blogkategorileri");

            migrationBuilder.DropColumn(
                name: "DetayEN",
                table: "blogkategorileri");

            
        }
    }
}
