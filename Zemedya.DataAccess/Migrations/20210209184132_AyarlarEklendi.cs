﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class AyarlarEklendi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ayarlar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Favicon = table.Column<string>(nullable: true),
                    Logo = table.Column<string>(nullable: true),
                    Adres = table.Column<string>(nullable: true),
                    Server = table.Column<string>(nullable: true),
                    Sifre = table.Column<string>(nullable: true),
                    Port = table.Column<string>(nullable: true),
                    IletilecekAdres = table.Column<string>(nullable: true),
                    Facebook = table.Column<string>(nullable: true),
                    Twitter = table.Column<string>(nullable: true),
                    Instagram = table.Column<string>(nullable: true),
                    Linkedin = table.Column<string>(nullable: true),
                    Google = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ayarlar", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Duyurular",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(nullable: true),
                    IsimEN = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    Ozet = table.Column<string>(nullable: true),
                    OzetEN = table.Column<string>(nullable: true),
                    Detay = table.Column<string>(nullable: true),
                    DetayEN = table.Column<string>(nullable: true),
                    Tarih = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Duyurular", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ayarlar");

            migrationBuilder.DropTable(
                name: "Duyurular");
        }
    }
}
