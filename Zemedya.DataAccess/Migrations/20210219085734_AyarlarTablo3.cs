﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class AyarlarTablo3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CanlıDestekKodu",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "GoogleDoğrulamaKodu",
                table: "Ayarlar");

            migrationBuilder.AddColumn<string>(
                name: "CanliDestekKodu",
                table: "Ayarlar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GoogleDogrulamaKodu",
                table: "Ayarlar",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CanliDestekKodu",
                table: "Ayarlar");

            migrationBuilder.DropColumn(
                name: "GoogleDogrulamaKodu",
                table: "Ayarlar");

            migrationBuilder.AddColumn<string>(
                name: "CanlıDestekKodu",
                table: "Ayarlar",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GoogleDoğrulamaKodu",
                table: "Ayarlar",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
