﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class EkipAktif : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Aktif",
                table: "GaleriKategorileri",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Aktif",
                table: "EkipListesi",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Aktif",
                table: "GaleriKategorileri");

            migrationBuilder.DropColumn(
                name: "Aktif",
                table: "EkipListesi");
        }
    }
}
