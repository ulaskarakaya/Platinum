﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class _0806221730 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "videomedyakategori",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NameTR = table.Column<string>(maxLength: 150, nullable: true),
                    NameEN = table.Column<string>(maxLength: 150, nullable: true),
                    Image = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_videomedyakategori", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "videomediaoge",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    video = table.Column<string>(nullable: true),
                    DescriptionTR = table.Column<string>(nullable: true),
                    DescriptionEN = table.Column<string>(nullable: true),
                    VideoMediaCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_videomediaoge", x => x.Id);
                    table.ForeignKey(
                        name: "FK_videomediaoge_videomedyakategori_VideoMediaCategoryId",
                        column: x => x.VideoMediaCategoryId,
                        principalTable: "videomedyakategori",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_videomediaoge_VideoMediaCategoryId",
                table: "videomediaoge",
                column: "VideoMediaCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "videomediaoge");

            migrationBuilder.DropTable(
                name: "videomedyakategori");
        }
    }
}
