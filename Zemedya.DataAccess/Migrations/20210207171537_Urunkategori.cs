﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class Urunkategori : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UrunKategorileri_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri");

            migrationBuilder.DropIndex(
                name: "IX_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri");

            migrationBuilder.DropColumn(
                name: "UstKategorisiId",
                table: "UrunKategorileri");

            migrationBuilder.CreateIndex(
                name: "IX_UrunKategorileri_UstKategoriId",
                table: "UrunKategorileri",
                column: "UstKategoriId");

            migrationBuilder.AddForeignKey(
                name: "FK_UrunKategorileri_UrunKategorileri_UstKategoriId",
                table: "UrunKategorileri",
                column: "UstKategoriId",
                principalTable: "UrunKategorileri",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UrunKategorileri_UrunKategorileri_UstKategoriId",
                table: "UrunKategorileri");

            migrationBuilder.DropIndex(
                name: "IX_UrunKategorileri_UstKategoriId",
                table: "UrunKategorileri");

            migrationBuilder.AddColumn<int>(
                name: "UstKategorisiId",
                table: "UrunKategorileri",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri",
                column: "UstKategorisiId");

            migrationBuilder.AddForeignKey(
                name: "FK_UrunKategorileri_UrunKategorileri_UstKategorisiId",
                table: "UrunKategorileri",
                column: "UstKategorisiId",
                principalTable: "UrunKategorileri",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
