﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class MevcutSayfalar2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Ekip",
                table: "MevcutSayfalar",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Galeri",
                table: "MevcutSayfalar",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Ekip",
                table: "MevcutSayfalar");

            migrationBuilder.DropColumn(
                name: "Galeri",
                table: "MevcutSayfalar");
        }
    }
}
