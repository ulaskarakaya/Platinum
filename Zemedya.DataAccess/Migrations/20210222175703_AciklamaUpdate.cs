﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class AciklamaUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Acıklama",
                table: "EkipListesi");

            migrationBuilder.AddColumn<string>(
                name: "Aciklama",
                table: "EkipListesi",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Aciklama",
                table: "EkipListesi");

            migrationBuilder.AddColumn<string>(
                name: "Acıklama",
                table: "EkipListesi",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
