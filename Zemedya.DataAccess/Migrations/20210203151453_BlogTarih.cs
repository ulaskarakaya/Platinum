﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class BlogTarih : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Detay",
                table: "Bloglar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DetayEN",
                table: "Bloglar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ozet",
                table: "Bloglar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OzetEN",
                table: "Bloglar",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Tarih",
                table: "Bloglar",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Detay",
                table: "Bloglar");

            migrationBuilder.DropColumn(
                name: "DetayEN",
                table: "Bloglar");

            migrationBuilder.DropColumn(
                name: "Ozet",
                table: "Bloglar");

            migrationBuilder.DropColumn(
                name: "OzetEN",
                table: "Bloglar");

            migrationBuilder.DropColumn(
                name: "Tarih",
                table: "Bloglar");
        }
    }
}
