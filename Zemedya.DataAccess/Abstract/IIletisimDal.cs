﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.IletisimEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IIletisimDal : IBaseDal<Iletisim>
    {
    }
}
