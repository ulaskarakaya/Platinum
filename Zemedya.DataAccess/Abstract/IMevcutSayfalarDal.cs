﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IMevcutSayfalarDal : IBaseDal<MevcutSayfalar>
    {
    }
}
