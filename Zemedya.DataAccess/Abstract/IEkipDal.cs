﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.EkipEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IEkipDal : IBaseDal<Ekip>
    {
    }
}
