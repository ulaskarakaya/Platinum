﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IImageMediaCategoryDal : IBaseDal<ImageMediaCategory>
    {
        public void ItemSil(Expression<Func<ImageMediaItem, bool>> filter);
        public ImageMediaItem ItemGet(Expression<Func<ImageMediaItem, bool>> filter);
        public void ItemAdd(ImageMediaItem model);
        public void ItemUpdate(ImageMediaItem model);
    }
}
