﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.ReferansEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IReferansDal : IBaseDal<Referans>
    {
    }
}
