﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.EtkinlikEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IEtkinlikDal : IBaseDal<Etkinlik>
    {
    }
}
