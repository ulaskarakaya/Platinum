﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.MembershipEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IMembershipDal : IBaseDal<Membership>
    {
    }
}
