﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.Team;

namespace Zemedya.DataAccess.Abstract
{
    public interface ITeamListDal : IBaseDal<TeamList>
    {
    }
}
