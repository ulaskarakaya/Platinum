﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.PopupEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IPopupDal : IBaseDal<Popup>
    {
    }
}
