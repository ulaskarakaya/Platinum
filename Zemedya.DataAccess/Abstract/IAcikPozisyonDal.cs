﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.AcikPozisyonEtities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IAcikPozisyonDal : IBaseDal<AcikPozisyon>
    {
    }
}
