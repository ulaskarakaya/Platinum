﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IVideoMediaCategoryDal : IBaseDal<VideoMediaCategory>
    {
        public void ItemSil(Expression<Func<VideoMediaItem, bool>> filter);
        public VideoMediaItem ItemGet(Expression<Func<VideoMediaItem, bool>> filter);
        public void ItemAdd(VideoMediaItem model);
        public void ItemUpdate(VideoMediaItem model);
    }
}
