﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.DuyuruEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IDuyuruDal : IBaseDal<Duyuru>
    {
    }
}
