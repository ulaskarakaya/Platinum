﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IBlogKategoriDal : IBaseDal<BlogKategori>
    {
    }
}
