﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.HaberEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IHaberKategoriDal : IBaseDal<HaberKategori>
    {
    }
}
