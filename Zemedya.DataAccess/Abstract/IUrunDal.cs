﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.UrunEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IUrunDal : IBaseDal<Urun>
    {
    }
}
