﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.KariyerFirsatlariEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IKariyerFirsatiDal : IBaseDal<KariyerFirsati>
    {
    }
}
