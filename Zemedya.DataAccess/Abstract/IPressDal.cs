﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.PressEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IPressDal : IBaseDal<Press>
    {
    }
}
