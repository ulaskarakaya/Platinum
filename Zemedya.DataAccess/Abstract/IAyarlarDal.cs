﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.AyarEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IAyarlarDal : IBaseDal<Ayarlar>
    {
    }
}
