﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.SliderEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface ISliderDal : IBaseDal<Slider>
    {
    }
}
