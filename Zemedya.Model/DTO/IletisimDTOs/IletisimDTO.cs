﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.IletisimDTOs
{
    public class IletisimDTO
    {
        public int Id { get; set; }
        public string Baslik { get; set; }
        public string BaslikEN { get; set; }
        public string Adres { get; set; }
        public string Fax { get; set; }
        public string Telefon { get; set; }
        public string Telefon2 { get; set; }
        public string Email { get; set; }
        public string HaritaKod { get; set; }
        public bool Aktif { get; set; }
    }
}
