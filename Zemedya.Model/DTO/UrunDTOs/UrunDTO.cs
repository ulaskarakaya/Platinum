﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.UrunDTOs
{
    public class UrunDTO
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string Link { get; set; }
        public string LinkEN { get; set; }
        public int SıraNo { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public string SEO { get; set; }
        public string SEOEN { get; set; }
        public bool Aktif { get; set; }
        public string Kod { get; set; }
        public string Ozet { get; set; }
        public string OzetEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public string Gorsel_2 { get; set; }
        public string Gorsel_2EN { get; set; }
        public string Gorsel_3 { get; set; }
        public string Gorsel_3EN { get; set; }
        public string Gorsel_4 { get; set; }
        public string Gorsel_4EN { get; set; }
        public string Gorsel_5 { get; set; }
        public string Gorsel_5EN { get; set; }
        public string Gorsel_6 { get; set; }
        public string Gorsel_6EN { get; set; }

        public int UrunKategoriId { get; set; }
        public virtual UrunKategoriDTO UrunKategori { get; set; }
    }
}
