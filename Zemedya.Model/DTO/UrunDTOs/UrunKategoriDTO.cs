﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.UrunDTOs
{
    public class UrunKategoriDTO
    {

        public int UstKategoriId { get; set; }
        public virtual UrunKategoriDTO UstKategori { get; set; }
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string SEO { get; set; }
        public string SEOEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public bool Aktif { get; set; }
    }
}
