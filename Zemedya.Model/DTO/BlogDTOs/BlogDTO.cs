﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.BlogDTOs
{
    public class BlogDTO
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public string SEO { get; set; }
        public string SEOEN { get; set; }
        public string Ozet { get; set; }
        public string OzetEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public DateTime Tarih { get; set; }
        public bool Aktif { get; set; }
        public int SıraNo { get; set; }
        public int BlogKategoriId { get; set; }
        public virtual BlogKategoriDTO BlogKategori { get; set; }
    }
}
