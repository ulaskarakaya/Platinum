﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.EmailDTOs
{
    public class EmailDTO
    {
        public string EpostaAdı { get; set; }
        public string AdSoyad { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Telefon { get; set; }
        public string Mesaj { get; set; }
        public string Konu { get; set; }
    }
}
