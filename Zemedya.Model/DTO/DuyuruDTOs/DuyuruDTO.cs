﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Entity.Concrete.ReferansEntities;

namespace Zemedya.Model.DTO.DuyuruDTOs
{
    public class DuyuruDTO
    {
        public DuyuruDTO()
        {
            this.Galeri = new List<Galeri>();
            this.Referans = new List<Referans>();
        }
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string Ozet { get; set; }
        public string OzetEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public string SEO { get; set; }
        public string SEOEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public DateTime Tarih { get; set; }
        public bool Aktif { get; set; }

        public string Detay2 { get; set; }
        public string Detay2EN { get; set; }

        public int GaleriKategoriId { get; set; }

        public string AnaSayfaBaslik { get; set; }
        public string AnaSayfaGorsel { get; set; }

        public string AnaSayfaBaslikEN { get; set; }
        public string AnaSayfaGorselEN { get; set; }

        public virtual List<Galeri> Galeri { get; set; }
        public virtual List<Referans> Referans { get; set; }
    }
}
