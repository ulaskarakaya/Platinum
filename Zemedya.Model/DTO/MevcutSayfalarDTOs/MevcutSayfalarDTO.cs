﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.MevcutSayfalarDTOs
{
    public class MevcutSayfalarDTO
    {
        public int Id { get; set; }
        public bool Haberler { get; set; }
        public bool Duyuru { get; set; }
        public bool Etkinlik { get; set; }
        public bool Blog { get; set; }
        public bool Iletisim { get; set; }
        public bool Kurumsal { get; set; }
        public bool Popup { get; set; }
        public bool Slider { get; set; }
        public bool Urunler { get; set; }
        public bool Yonetici { get; set; }
        public bool Video { get; set; }
        public bool Referans { get; set; }
        public bool Ayarlar { get; set; }
        public bool Ekip { get; set; }
        public bool Galeri { get; set; }
        public bool GaleriKategori { get; set; }
        public bool UrunKategori { get; set; }
        public bool BlogKategori { get; set; }
    }
}
