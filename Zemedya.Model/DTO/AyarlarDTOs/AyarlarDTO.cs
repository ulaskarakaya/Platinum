﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.AyarlarDTOs
{
    public class AyarlarDTO
    {
        public int Id { get; set; }
        public string Favicon { get; set; }
        public string Logo { get; set; }
        public string Adres { get; set; }
        public string Server { get; set; }
        public string Sifre { get; set; }
        public string Port { get; set; }
        public string IletilecekAdres { get; set; }
        public string IletilecekAdres2 { get; set; }
        public string Youtube { get; set; }
        public int YoutubeOrder { get; set; }
        public string Facebook { get; set; }
        public int FacebookOrder { get; set; }
        public string Twitter { get; set; }
        public int TwitterOrder { get; set; }
        public string Instagram { get; set; }
        public int InstagramOrder { get; set; }
        public string Linkedin { get; set; }
        public int LinkedinOrder { get; set; }
        public string Google { get; set; }
        public string WhatsappMesaj { get; set; }
        public string WhatsappTelefon { get; set; }
        public bool WhatsappDurum { get; set; }
        public string CanliDestekKodu { get; set; }
        public bool CanliDestekAktif { get; set; }
        public string SiteUrl { get; set; }
        public string SiteTitle { get; set; }
        public string Description { get; set; }
        public string SiteTitleEN { get; set; }
        public string DescriptionEN { get; set; }
        public string GoogleDogrulamaKodu { get; set; }
        public string GoogleAnalyticsKodu { get; set; }
        public string RecaptchaSiteKey { get; set; }
        public string RecaptchaSecretKey { get; set; }
        public bool EN { get; set; }
    }
}
