﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.AyarlarDTOs
{
    public class SocialListDTO
    {
        public int Order { get; set; }
        public string Link { get; set; }
    }
}
