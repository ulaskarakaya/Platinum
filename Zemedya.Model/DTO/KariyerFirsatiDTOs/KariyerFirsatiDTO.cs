﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.KariyerFirsatiDTOs
{
    public class KariyerFirsatiDTO
    {
        public int Id { get; set; }
        public string ReferansNo { get; set; }
        public string Pozisyon { get; set; }
        public string Sehir { get; set; }
        public string IsAlani { get; set; }
        public string PozisyonTipi { get; set; }
        public DateTime Tarih { get; set; }
        public string Detay { get; set; }
        public string SEO { get; set; }
    }
}
