﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.YoneticiDTOs
{
    public class YoneticiDTO
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Email { get; set; }
        public string KullaniciAdi { get; set; }
        public string Sifre { get; set; }
        public bool Aktif { get; set; }
    }
}
