﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.SliderDTOs
{
    public class SliderDTO 
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string Link { get; set; }
        public string LinkEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public bool Aktif { get; set; }
        public int SıraNo { get; set; }
        public bool EN { get; set; }
        public string LinkBaslik { get; set; }
        public string LinkBaslikEN { get; set; }
        public string Aciklama { get; set; }
        public string AciklamaEN { get; set; }
    }
}
