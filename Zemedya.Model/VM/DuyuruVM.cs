﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Entity.Concrete.GaleriEntities;

namespace Zemedya.Model.VM
{
    public class DuyuruVM
    {
        public IEnumerable<Duyuru> Duyurular { get; set; }
        public IEnumerable<GaleriKategori> GaleriKategorileri { get; set; }
    }
}
