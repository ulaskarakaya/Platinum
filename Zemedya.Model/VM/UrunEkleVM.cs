﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Model.DTO.UrunDTOs;

namespace Zemedya.Model.VM
{
    public class UrunEkleVM
    {
        public UrunEkleVM()
        {
            this.UrunKategorileri = new List<UrunKategori>();
        }
        public virtual List<UrunKategori> UrunKategorileri { get; set; }
        public UrunDTO Urun { get; set; }
        public IFormFile Gorsel { get; set; }
        public IFormFile GorselEN { get; set; }
        public IFormFile Gorsel_2 { get; set; }
        public IFormFile Gorsel_2EN { get; set; }
        public IFormFile Gorsel_3 { get; set; }
        public IFormFile Gorsel_3EN { get; set; }
        public IFormFile Gorsel_4 { get; set; }
        public IFormFile Gorsel_4EN { get; set; }
        public IFormFile Gorsel_5 { get; set; }
        public IFormFile Gorsel_5EN { get; set; }
        public IFormFile Gorsel_6 { get; set; }
        public IFormFile Gorsel_6EN { get; set; }
        public bool EN { get; set; }
    }
}
