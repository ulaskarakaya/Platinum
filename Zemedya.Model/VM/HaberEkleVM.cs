﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Model.DTO.HaberDTOs;

namespace Zemedya.Model.VM
{
    public class HaberEkleVM
    {
        public HaberEkleVM()
        {
            this.HaberKategorileri = new List<HaberKategori>();
        }
        public List<HaberKategori> HaberKategorileri { get; set; }
        public HaberDTO HaberDTO { get; set; }
        public IFormFile Gorsel { get; set; }
        public IFormFile GorselEN { get; set; }
    }
}
