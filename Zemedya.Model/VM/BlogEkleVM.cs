﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Model.DTO.BlogDTOs;

namespace Zemedya.Model.VM
{
    public class BlogEkleVM
    {
        public List<BlogKategori> BlogKategorileri { get; set; }
        public BlogDTO BlogDTO { get; set; }
        public IFormFile Gorsel { get; set; }
        public IFormFile GorselEN { get; set; }
    }
}
