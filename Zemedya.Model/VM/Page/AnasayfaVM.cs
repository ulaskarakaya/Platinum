﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Entity.Concrete.PopupEntities;
using Zemedya.Entity.Concrete.ReferansEntities;
using Zemedya.Entity.Concrete.SliderEntities;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Model.DTO.KurumsalDTOs;
using Zemedya.Model.DTO.PopupDTOs;

namespace Zemedya.Model.VM.Page
{
    public class AnasayfaVM
    {
        public AnasayfaVM()
        {
            this.Referans = new List<Referans>();
            this.Sliderlar = new List<Slider>();
            this.Videolar = new List<Video>();
            this.BlogKategoriListesi = new List<BlogKategori>();
            this.YayinListesi = new List<Blog>();
            this.Solutions = new List<Duyuru>();
        }
        public List<Slider> Sliderlar { get; set; }
        public string Title { get; set; }
        public string TitleEN { get; set; }
        public string GoogleDogrulama { get; set; }
        public string Favicon { get; set; }
        public string Logo { get; set; }
        public string GoogleAnalytics { get; set; }
        public List<Video> Videolar { get; set; }
        public List<BlogKategori> BlogKategoriListesi { get; set; }
        public List<Blog> YayinListesi { get; set; }
        public List<Duyuru> Solutions { get; set; }
        public List<Referans> Referans { get; set; }
        public KurumsalDTO KurumsalDTO { get; set; }

    }
}
