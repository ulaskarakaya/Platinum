﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Entity.Concrete.EkipEntities;
using Zemedya.Entity.Concrete.EtkinlikEntities;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Model.DTO.AyarlarDTOs;
using Zemedya.Model.DTO.IletisimDTOs;
using Zemedya.Model.DTO.KurumsalDTOs;
using Zemedya.Model.DTO.VideoDTOs;

namespace Zemedya.Model.VM.Page
{
    public class FooterVM
    {
        public virtual IletisimDTO Iletisim { get; set; }
        public virtual AyarlarDTO Ayarlar { get; set; }
        public virtual List<Duyuru> Duyurular { get; set; }
        public virtual List<Etkinlik> Etkinlikler { get; set; }
        public virtual List<Ekip> MediaListesi { get; set; }
        public virtual List<BlogKategori> BlogKategorileri { get; set; }
        public virtual KurumsalDTO KurumsalDTO { get; set; }
        public virtual List<Video> Videolar { get; set; }
        public virtual VideoDTO VideoDTO { get; set; }
    }
}
