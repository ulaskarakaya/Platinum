﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Entity.Concrete.EkipEntities;
using Zemedya.Entity.Concrete.EtkinlikEntities;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Entity.Concrete.KurumsalEntities;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Model.DTO.AyarlarDTOs;
using Zemedya.Model.DTO.KurumsalDTOs;
using Zemedya.Model.DTO.MevcutDillerDTOs;
using Zemedya.Model.DTO.MevcutSayfalarDTOs;
using Zemedya.Model.DTO.VideoDTOs;

namespace Zemedya.Model.VM.Page
{
    public class NavbarVM
    {
        public NavbarVM()
        {
            this.KurumsalListesi = new List<Kurumsal>();
            this.BlogKategorileri = new List<BlogKategori>();
            this.UrunKategorileri = new List<UrunKategori>();
            this.AltKategoriler = new List<UrunKategori>();
            this.EtkinlikListesi = new List<Etkinlik>();
            this.DuyuruListesi = new List<Duyuru>();
        }
        public virtual List<BlogKategori> BlogKategorileri { get; set; }
        public virtual List<UrunKategori> UrunKategorileri { get; set; }
        public virtual List<UrunKategori> AltKategoriler { get; set; }
        public MevcutSayfalarDTO MevcutSayfalar { get; set; }
        public MevcutDillerDTO MevcutDiller { get; set; }
        public AyarlarDTO Ayarlar { get; set; }
        public virtual List<Kurumsal> KurumsalListesi { get; set; }
        public virtual List<Etkinlik> EtkinlikListesi { get; set; }
        public virtual List<Duyuru> DuyuruListesi { get; set; }
        public virtual List<Ekip> MediaListesi { get; set; }
        public virtual KurumsalDTO KurumsalDTO { get; set; }
        public virtual List<Video> Videolar { get; set; }
        public virtual VideoDTO VideoDTO { get; set; }

    }
}
