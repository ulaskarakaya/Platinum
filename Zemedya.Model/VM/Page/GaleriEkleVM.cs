﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.GaleriEntities;

namespace Zemedya.Model.VM.Page
{
    public class GaleriEkleVM
    {
        public GaleriEkleVM()
        {
            this.GaleriKategorileri = new List<GaleriKategori>();
            this.GaleriListesi = new List<Galeri>();
        }
        public List<Galeri> GaleriListesi { get; set; }
        public List<GaleriKategori> GaleriKategorileri { get; set; }
    }
}
