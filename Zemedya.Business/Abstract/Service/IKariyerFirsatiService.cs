﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.KariyerFirsatlariEntities;
using Zemedya.Model.DTO.KariyerFirsatiDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IKariyerFirsatiService
    {
        List<KariyerFirsati> HepsiniGetir(Expression<Func<KariyerFirsati, bool>> filter = null);
        KariyerFirsatiDTO Getir(Expression<Func<KariyerFirsati, bool>> filter);
        void Ekle(KariyerFirsatiDTO KariyerFirsatiDTO);
        void Guncelle(KariyerFirsatiDTO KariyerFirsatiDTO);
        void PasifYap(Expression<Func<KariyerFirsati, bool>> filter);
        void KaliciSil(Expression<Func<KariyerFirsati, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
