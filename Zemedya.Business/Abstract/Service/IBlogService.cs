﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Model.DTO.BlogDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IBlogService
    {
        List<Blog> HepsiniGetir(Expression<Func<Blog, bool>> filter = null);
        BlogDTO Getir(Expression<Func<Blog, bool>> filter);
        void Ekle(BlogDTO blogDTO);
        void Guncelle(BlogDTO blogDTO);
        void PasifYap(Expression<Func<Blog, bool>> filter);
        void KalıcıSil(Expression<Func<Blog, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
