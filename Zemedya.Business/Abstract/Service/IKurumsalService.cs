﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.KurumsalEntities;
using Zemedya.Model.DTO.KurumsalDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IKurumsalService
    {
        List<Kurumsal> HepsiniGetir(Expression<Func<Kurumsal, bool>> filter = null);
        KurumsalDTO Getir(Expression<Func<Kurumsal, bool>> filter);
        void Ekle(KurumsalDTO KurumsalDTO);
        void Guncelle(KurumsalDTO KurumsalDTO);
        void PasifYap(Expression<Func<Kurumsal, bool>> filter);
        void KalıcıSil(Expression<Func<Kurumsal, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
