﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Model.DTO.GaleriDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IGaleriService
    {
        List<Galeri> HepsiniGetir(Expression<Func<Galeri, bool>> filter = null);
        GaleriDTO Getir(Expression<Func<Galeri, bool>> filter);
        void Ekle(GaleriDTO GaleriDTO);
        void Guncelle(GaleriDTO GaleriDTO);
        void PasifYap(Expression<Func<Galeri, bool>> filter);
        void KalıcıSil(Expression<Func<Galeri, bool>> filter);
    }
}
