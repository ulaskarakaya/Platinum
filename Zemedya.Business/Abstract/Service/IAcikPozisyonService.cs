﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.AcikPozisyonEtities;

namespace Zemedya.Business.Abstract.Service
{
    public interface IAcikPozisyonService
    {
        List<AcikPozisyon> GetAll();
        AcikPozisyon Get(Expression<Func<AcikPozisyon, bool>> filter);
        void AddOrUpdate(AcikPozisyon model);
        void Delete(Expression<Func<AcikPozisyon, bool>> filter);
    }
}
