﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.YoneticiEntities;
using Zemedya.Model.DTO.YoneticiDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IYoneticiService
    {
        List<Yonetici> HepsiniGetir(Expression<Func<Yonetici, bool>> filter = null);
        YoneticiDTO Getir(Expression<Func<Yonetici, bool>> filter);
        void Ekle(YoneticiDTO yoneticiDTO);
        void Guncelle(YoneticiDTO yoneticiDTO);
        void PasifYap(Expression<Func<Yonetici, bool>> filter);
        void KaliciSil(Expression<Func<Yonetici, bool>> filter);
        bool KullaniciAdiKontrol(string kullaniciadi);
    }
}
