﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.PressEntities;

namespace Zemedya.Business.Abstract.Service
{
    public interface IPressService
    {
        List<Press> GetAll();
        Press Get(Expression<Func<Press, bool>> filter);
        void AddOrUpdate(Press model);
        void Delete(Expression<Func<Press, bool>> filter);
        void Up(Expression<Func<Press, bool>> filter);
        void Down(Expression<Func<Press, bool>> filter);
    }
}
