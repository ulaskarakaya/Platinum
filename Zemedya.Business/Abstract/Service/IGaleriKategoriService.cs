﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Model.DTO.GaleriDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IGaleriKategoriService
    {
        List<GaleriKategori> HepsiniGetir(Expression<Func<GaleriKategori, bool>> filter = null);
        GaleriKategoriDTO Getir(Expression<Func<GaleriKategori, bool>> filter);
        void Ekle(GaleriKategoriDTO GaleriKategoriDTO);
        void Guncelle(GaleriKategoriDTO GaleriKategoriDTO);
        void PasifYap(Expression<Func<GaleriKategori, bool>> filter);
        void KalıcıSil(Expression<Func<GaleriKategori, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
