﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.OdaEntities;
using Zemedya.Model.DTO.OdaDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IOdaService
    {
        List<Oda> HepsiniGetir(Expression<Func<Oda, bool>> filter = null);
        OdaDTO Getir(Expression<Func<Oda, bool>> filter);
        void Ekle(OdaDTO OdaDTO);
        void Guncelle(OdaDTO OdaDTO);
        void PasifYap(Expression<Func<Oda, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
