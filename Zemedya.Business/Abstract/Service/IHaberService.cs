﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Model.DTO.HaberDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IHaberService
    {
        List<Haber> HepsiniGetir(Expression<Func<Haber, bool>> filter = null);
        HaberDTO Getir(Expression<Func<Haber, bool>> filter);
        void Ekle(HaberDTO haberDTO);
        void Guncelle(HaberDTO haberDTO);
        void PasifYap(Expression<Func<Haber, bool>> filter);
        void KaliciSil(Expression<Func<Haber, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
