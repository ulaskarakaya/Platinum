﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.IletisimEntities;
using Zemedya.Model.DTO.IletisimDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IIletisimService
    {
        List<Iletisim> HepsiniGetir(Expression<Func<Iletisim, bool>> filter = null);
        IletisimDTO Getir(Expression<Func<Iletisim, bool>> filter);
        void Ekle(IletisimDTO iletisimDTO);
        void Guncelle(IletisimDTO iletisimDTO);
        void PasifYap(Expression<Func<Iletisim, bool>> filter);
        void KalıcıSil(Expression<Func<Iletisim, bool>> filter);
    }
}
