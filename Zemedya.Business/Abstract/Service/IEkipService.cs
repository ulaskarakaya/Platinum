﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.EkipEntities;
using Zemedya.Model.DTO.EkipDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IEkipService
    {
        List<Ekip> HepsiniGetir(Expression<Func<Ekip, bool>> filter = null);
        EkipDTO Getir(Expression<Func<Ekip, bool>> filter);
        void Ekle(EkipDTO EkipDTO);
        void Guncelle(EkipDTO EkipDTO);
        void PasifYap(Expression<Func<Ekip, bool>> filter);
        void KalıcıSil(Expression<Func<Ekip, bool>> filter);
    }
}
