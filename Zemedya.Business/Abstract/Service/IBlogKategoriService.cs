﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Model.DTO.BlogDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IBlogKategoriService
    {
        List<BlogKategori> HepsiniGetir(Expression<Func<BlogKategori, bool>> filter = null);
        BlogKategoriDTO Getir(Expression<Func<BlogKategori, bool>> filter);
        void Ekle(BlogKategoriDTO blogKategoriDTO);
        void Guncelle(BlogKategoriDTO blogKategoriDTO);
        void PasifYap(Expression<Func<BlogKategori, bool>> filter);
        void KalıcıSil(Expression<Func<BlogKategori, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);

    }
}
