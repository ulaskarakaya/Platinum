﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Model.DTO.UrunDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IUrunKategoriService
    {
        List<UrunKategori> HepsiniGetir(Expression<Func<UrunKategori, bool>> filter = null);
        UrunKategoriDTO Getir(Expression<Func<UrunKategori, bool>> filter);
        void Ekle(UrunKategoriDTO UrunKategoriDTO);
        void Guncelle(UrunKategoriDTO UrunKategoriDTO);
        void PasifYap(Expression<Func<UrunKategori, bool>> filter);
        void KalıcıSil(Expression<Func<UrunKategori, bool>> filter);
    }
}
