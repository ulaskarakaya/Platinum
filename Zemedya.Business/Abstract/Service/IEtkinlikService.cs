﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.EtkinlikEntities;
using Zemedya.Model.DTO.EtkinlikDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IEtkinlikService
    {
        List<Etkinlik> HepsiniGetir(Expression<Func<Etkinlik, bool>> filter = null);
        EtkinlikDTO Getir(Expression<Func<Etkinlik, bool>> filter);
        void Ekle(EtkinlikDTO EtkinlikDTO);
        void Guncelle(EtkinlikDTO EtkinlikDTO);
        void PasifYap(Expression<Func<Etkinlik, bool>> filter);
        void KalıcıSil(Expression<Func<Etkinlik, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
