﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.Business.Abstract.Service
{
    public interface IImageMediaCategoryService
    {
        List<ImageMediaCategory> GetAll();
        ImageMediaCategory Get(Expression<Func<ImageMediaCategory, bool>> filter);
        void AddOrUpdate(ImageMediaCategory model);
        void Delete(Expression<Func<ImageMediaCategory, bool>> filter);
        ImageMediaItem GetItem(Expression<Func<ImageMediaItem, bool>> filter);
        void AddOrUpdateItem(ImageMediaItem model);
        void DeleteItem(Expression<Func<ImageMediaItem, bool>> filter);

    }
}
