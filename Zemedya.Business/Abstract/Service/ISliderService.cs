﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.SliderEntities;
using Zemedya.Model.DTO.SliderDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface ISliderService
    {
        List<Slider> HepsiniGetir(Expression<Func<Slider, bool>> filter = null);
        SliderDTO Getir(Expression<Func<Slider, bool>> filter);
        void Ekle(SliderDTO SliderDTO);
        void Guncelle(SliderDTO SliderDTO);
        void PasifYap(Expression<Func<Slider, bool>> filter);
        void KalıcıSil(Expression<Func<Slider, bool>> filter);
    }
}
