﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.MembershipEntities;

namespace Zemedya.Business.Abstract.Service
{
    public interface IMembershipService
    {
        List<Membership> GetAll();
        Membership Get(Expression<Func<Membership, bool>> filter);
        void AddOrUpdate(Membership model);
        void Delete(Expression<Func<Membership, bool>> filter);
        void UpMembership(Expression<Func<Membership, bool>> filter);
        void DownMembership(Expression<Func<Membership, bool>> filter);
        object GetAllCertificates();
    }
}
