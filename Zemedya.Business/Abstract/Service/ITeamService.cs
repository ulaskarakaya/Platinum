﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.Team;

namespace Zemedya.Business.Abstract.Service
{
    public interface ITeamService
    {
        void AddOrUpdateCategory(TeamCategory model);
        List<TeamCategory> GetAllTeam();
        TeamCategory GetCategory(Expression<Func<TeamCategory, bool>> filter);
        void DeleteCategory(Expression<Func<TeamCategory, bool>> filter);
        void UpCategory(Expression<Func<TeamCategory, bool>> filter);
        void DownCategory(Expression<Func<TeamCategory, bool>> filter);
        TeamList GetCategoryList(Expression<Func<TeamList, bool>> filter);
        void AddOrUpdateTeamList(TeamList model);
        void DeleteTeamList(Expression<Func<TeamList, bool>> filter);
        void UpTeamList(Expression<Func<TeamList, bool>> filter);
        void DownTeamList(Expression<Func<TeamList, bool>> filter);
    }
}
