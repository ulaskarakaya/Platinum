﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.ReferansEntities;
using Zemedya.Model.DTO.ReferansDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IReferansService
    {
        List<Referans> HepsiniGetir(Expression<Func<Referans, bool>> filter = null);
        ReferansDTO Getir(Expression<Func<Referans, bool>> filter);
        void Ekle(ReferansDTO ReferansDTO);
        void Guncelle(ReferansDTO ReferansDTO);
        void PasifYap(Expression<Func<Referans, bool>> filter);
        void KalıcıSil(Expression<Func<Referans, bool>> filter);
    }
}
