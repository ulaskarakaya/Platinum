﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.Business.Abstract.Service
{
    public interface IVideoMediaCategoryService
    {
        List<VideoMediaCategory> GetAll();
        VideoMediaCategory Get(Expression<Func<VideoMediaCategory, bool>> filter);
        void AddOrUpdate(VideoMediaCategory model);
        void Delete(Expression<Func<VideoMediaCategory, bool>> filter);
        VideoMediaItem GetItem(Expression<Func<VideoMediaItem, bool>> filter);
        void AddOrUpdateItem(VideoMediaItem model);
        void DeleteItem(Expression<Func<VideoMediaItem, bool>> filter);

    }
}
