﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Model.DTO.DuyuruDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IDuyuruService
    {
        List<Duyuru> HepsiniGetir(Expression<Func<Duyuru, bool>> filter = null);
        DuyuruDTO Getir(Expression<Func<Duyuru, bool>> filter);
        void Ekle(DuyuruDTO DuyuruDTO);
        void Guncelle(DuyuruDTO DuyuruDTO);
        void PasifYap(Expression<Func<Duyuru, bool>> filter);
        void KalıcıSil(Expression<Func<Duyuru, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
