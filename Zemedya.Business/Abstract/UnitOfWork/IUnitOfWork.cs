﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Business.Abstract.Service;

namespace Zemedya.Business.Abstract.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        public bool Complete();
        public IHaberService HaberManager { get; }
        public IHaberKategoriService HaberKategoriManager { get; }
        public IYoneticiService YoneticiManager { get; }
        public IBlogKategoriService BlogKategoriManager { get; }
        public IBlogService BlogManager { get; }
        public IIletisimService IletisimManager { get; }
        public IPopupService PopupManager { get; }
        public ISliderService SliderManager { get; }
        public IMevcutSayfalarService MevcutSayfalarManager { get; }
        public IUrunService UrunManager { get; }
        public IUrunKategoriService UrunKategoriManager { get; }
        public IAyarlarService AyarlarManager { get; }
        public IDuyuruService DuyuruManager { get; }
        public IEtkinlikService EtkinlikManager { get; }
        public IReferansService ReferansManager { get; }
        public IVideoService VideoManager { get; }
        public IKurumsalService KurumsalManager { get; }
        public IMevcutDillerService MevcutDillerManager { get; }
        public IEkipService EkipManager { get; }
        public IGaleriService GaleriManager { get; }
        public IGaleriKategoriService GaleriKategoriManager { get; }
        public IOdaService OdaManager { get; }
        public ITeamService TeamManager { get; }
        public IMembershipService MembershipManager { get; }
        public IPressService PressService { get; }
        public IImageMediaCategoryService ImageMediaCategoryManager { get; }
        public IVideoMediaCategoryService VideoMediaCategoryManager { get; }
        public IAcikPozisyonService AcikPozisyonManager { get; }
    }
}
