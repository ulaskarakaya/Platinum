﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.Entity.Concrete.AyarEntities;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Entity.Concrete.EkipEntities;
using Zemedya.Entity.Concrete.EtkinlikEntities;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Entity.Concrete.IletisimEntities;
using Zemedya.Entity.Concrete.KurumsalEntities;
using Zemedya.Entity.Concrete.MevcutDillerEntities;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;
using Zemedya.Entity.Concrete.OdaEntities;
using Zemedya.Entity.Concrete.PopupEntities;
using Zemedya.Entity.Concrete.ReferansEntities;
using Zemedya.Entity.Concrete.SliderEntities;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Entity.Concrete.YoneticiEntities;
using Zemedya.Model.DTO.AyarlarDTOs;
using Zemedya.Model.DTO.BlogDTOs;
using Zemedya.Model.DTO.DuyuruDTOs;
using Zemedya.Model.DTO.EkipDTOs;
using Zemedya.Model.DTO.EtkinlikDTOs;
using Zemedya.Model.DTO.GaleriDTOs;
using Zemedya.Model.DTO.HaberDTOs;
using Zemedya.Model.DTO.IletisimDTOs;
using Zemedya.Model.DTO.KurumsalDTOs;
using Zemedya.Model.DTO.MevcutDillerDTOs;
using Zemedya.Model.DTO.MevcutSayfalarDTOs;
using Zemedya.Model.DTO.OdaDTOs;
using Zemedya.Model.DTO.PopupDTOs;
using Zemedya.Model.DTO.ReferansDTOs;
using Zemedya.Model.DTO.SliderDTOs;
using Zemedya.Model.DTO.UrunDTOs;
using Zemedya.Model.DTO.VideoDTOs;
using Zemedya.Model.DTO.YoneticiDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class AutoMapperManager : IMapperService
    {
        private Mapper _mapper;
        public AutoMapperManager()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddCollectionMappers();
                cfg.CreateMap<HaberDTO, Haber>().ReverseMap();
                cfg.CreateMap<HaberKategoriDTO, HaberKategori>().ReverseMap();
                cfg.CreateMap<YoneticiDTO, Yonetici>().ReverseMap();
                cfg.CreateMap<BlogKategoriDTO, BlogKategori>().ReverseMap();
                cfg.CreateMap<BlogDTO, Blog>().ReverseMap();
                cfg.CreateMap<IletisimDTO, Iletisim>().ReverseMap();
                cfg.CreateMap<PopupDTO, Popup>().ReverseMap();
                cfg.CreateMap<SliderDTO, Slider>().ReverseMap();
                cfg.CreateMap<UrunDTO, Urun>().ReverseMap();
                cfg.CreateMap<UrunKategoriDTO, UrunKategori>().ReverseMap();
                cfg.CreateMap<Duyuru, DuyuruDTO>().ReverseMap();
                cfg.CreateMap<Ayarlar, AyarlarDTO>().ReverseMap();
                cfg.CreateMap<Etkinlik, EtkinlikDTO>().ReverseMap();
                cfg.CreateMap<Referans, ReferansDTO>().ReverseMap();
                cfg.CreateMap<Kurumsal, KurumsalDTO>().ReverseMap();
                cfg.CreateMap<Video, VideoDTO>().ReverseMap();
                cfg.CreateMap<MevcutSayfalar, MevcutSayfalarDTO>().ReverseMap();
                cfg.CreateMap<MevcutDiller, MevcutDillerDTO>().ReverseMap();
                cfg.CreateMap<Ekip, EkipDTO>().ReverseMap();
                cfg.CreateMap<Galeri, GaleriDTO>().ReverseMap();
                cfg.CreateMap<GaleriKategori, GaleriKategoriDTO>().ReverseMap();
                cfg.CreateMap<Oda, OdaDTO>().ReverseMap();
            });
            config.CreateMapper();


            _mapper = new Mapper(config);
        }
        public THedef Map<TKaynak, THedef>(TKaynak kaynak)
        {
            return _mapper.Map<THedef>(kaynak);
        }

        public THedef Map<TKaynak, THedef>(TKaynak kaynak, THedef hedef)
        {
            return _mapper.Map(kaynak, hedef);
        }
    }
}
