﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Model.DTO.BlogDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class BlogManager : IBlogService
    {
        private IMapperService _mapperService;
        private IBlogDal _blogDal;
        public BlogManager(IMapperService mapperService, IBlogDal blogDal)
        {
            _mapperService = mapperService;
            _blogDal = blogDal;
        }

        public void Ekle(BlogDTO BlogDTO)
        {
            var entity = _mapperService.Map<BlogDTO, Blog>(BlogDTO);
            _blogDal.Ekle(entity);
        }

        public BlogDTO Getir(Expression<Func<Blog, bool>> filter)
        {
            var entity = _blogDal.Getir(filter);
            return _mapperService.Map<Blog, BlogDTO>(entity);
        }


        public void Guncelle(BlogDTO BlogDTO)
        {
            var entity = _blogDal.Getir(x => x.Id == BlogDTO.Id);
            var guncelentity = _mapperService.Map<BlogDTO, Blog>(BlogDTO, entity);
            _blogDal.Guncelle(guncelentity);
        }

        public List<Blog> HepsiniGetir(Expression<Func<Blog, bool>> filter = null)
        {
            return _blogDal.HepsiniGetir(filter).OrderByDescending(x => x.Tarih).ToList(); 
        }


        public void KalıcıSil(Expression<Func<Blog, bool>> filter)
        {
            _blogDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Blog, bool>> filter)
        {
            _blogDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var entity = _blogDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SEOKontrolEN(string seo)
        {
            var entity = _blogDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
