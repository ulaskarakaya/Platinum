﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.YoneticiEntities;
using Zemedya.Model.DTO.YoneticiDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class YoneticiManager : IYoneticiService
    {
        private IMapperService _mapperService;
        private IYoneticiDal _yoneticiDal;
        public YoneticiManager(IMapperService mapperService, IYoneticiDal yoneticiDal)
        {
            _mapperService = mapperService;
            _yoneticiDal = yoneticiDal;
        }
        public void Ekle(YoneticiDTO yoneticiDTO)
        {
            var entity = _mapperService.Map<YoneticiDTO, Yonetici>(yoneticiDTO);
            _yoneticiDal.Ekle(entity);
        }

        public YoneticiDTO Getir(Expression<Func<Yonetici, bool>> filter)
        {
            var entity = _yoneticiDal.Getir(filter);
            return _mapperService.Map<Yonetici, YoneticiDTO>(entity);

        }
        public void Guncelle(YoneticiDTO yoneticiDTO)
        {
            var yonetici = _yoneticiDal.Getir(x => x.Id == yoneticiDTO.Id);
            var guncelYonetici = _mapperService.Map<YoneticiDTO, Yonetici>(yoneticiDTO, yonetici);
            _yoneticiDal.Guncelle(guncelYonetici);
        }
    

        public List<Yonetici> HepsiniGetir(Expression<Func<Yonetici, bool>> filter = null)
        {
            return _yoneticiDal.HepsiniGetir(filter).ToList();
        }

        public void KaliciSil(Expression<Func<Yonetici, bool>> filter)
        {
            _yoneticiDal.KaliciSil(filter);
        }

        public bool KullaniciAdiKontrol(string kullaniciadi)
        {
            var kullanici = _yoneticiDal.Getir(x => x.KullaniciAdi == kullaniciadi);
            if (kullanici != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void PasifYap(Expression<Func<Yonetici, bool>> filter)
        {
            _yoneticiDal.PasifYap(filter);
        }
    }
}
