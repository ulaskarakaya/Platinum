﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.KurumsalEntities;
using Zemedya.Model.DTO.KurumsalDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class KurumsalManager : IKurumsalService
    {
        private IMapperService _mapperService;
        private IKurumsalDal _kurumsalDal;

        public KurumsalManager(IMapperService mapperService, IKurumsalDal kurumsalDal)
        {
            _mapperService = mapperService;
            _kurumsalDal = kurumsalDal;
        }

        public void Ekle(KurumsalDTO KurumsalDTO)
        {
            var entity = _mapperService.Map<KurumsalDTO, Kurumsal>(KurumsalDTO);
            _kurumsalDal.Ekle(entity);
        }

        public KurumsalDTO Getir(Expression<Func<Kurumsal, bool>> filter)
        {
            var entity = _kurumsalDal.Getir(filter);
            return _mapperService.Map<Kurumsal, KurumsalDTO>(entity);
        }


        public void Guncelle(KurumsalDTO KurumsalDTO)
        {
            var entity = _kurumsalDal.Getir(x => x.Id == KurumsalDTO.Id);
            var guncelentity = _mapperService.Map<KurumsalDTO, Kurumsal>(KurumsalDTO, entity);
            _kurumsalDal.Guncelle(guncelentity);
        }

        public List<Kurumsal> HepsiniGetir(Expression<Func<Kurumsal, bool>> filter = null)
        {
            return _kurumsalDal.HepsiniGetir(filter).OrderBy(x => x.SıralamaNo).ToList();
        }


        public void KalıcıSil(Expression<Func<Kurumsal, bool>> filter)
        {
            _kurumsalDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Kurumsal, bool>> filter)
        {
            _kurumsalDal.PasifYap(filter);
        }
        public bool SEOKontrol(string seo)
        {
            var entity = _kurumsalDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SEOKontrolEN(string seo)
        {
            var entity = _kurumsalDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
