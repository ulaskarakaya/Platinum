﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.Team;

namespace Zemedya.Business.Concrete.Manager
{
    public class TeamManager : ITeamService
    {
        private IMapperService _mapperService;
        private ITeamCategoryDal _teamCategoryDal;
        private ITeamListDal _teamListDal;
        public TeamManager(IMapperService mapperService, ITeamCategoryDal teamCategoryDal, ITeamListDal teamListDal)
        {
            _teamCategoryDal = teamCategoryDal;
            _teamListDal = teamListDal;
            _mapperService = mapperService;
        }

        public void AddOrUpdateCategory(TeamCategory model)
        {
            if (model.Id == 0)
            {
                model.Order = 9999;
                _teamCategoryDal.Ekle(model);
            }
            else
            {
                _teamCategoryDal.Guncelle(model);
            }
        }

        public void AddOrUpdateTeamList(TeamList model)
        {
            if (model.Id == 0)
            {
                model.Order = 9999;
                _teamListDal.Ekle(model);
            }
            else
            {
                _teamListDal.Guncelle(model);
            }
        }

        public void DeleteCategory(Expression<Func<TeamCategory, bool>> filter)
        {
            _teamCategoryDal.KaliciSil(filter);
        }

        public void DeleteTeamList(Expression<Func<TeamList, bool>> filter)
        {
            _teamListDal.KaliciSil(filter);
        }

        public void DownCategory(Expression<Func<TeamCategory, bool>> filter)
        {
            var entity = _teamCategoryDal.Getir(filter);

            var entities = _teamCategoryDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == entities.Count() - 1 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index + 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _teamCategoryDal.Guncelle(entities[i]);
            }
        }

        public void DownTeamList(Expression<Func<TeamList, bool>> filter)
        {
            var entity = _teamListDal.Getir(filter);

            var entities = _teamListDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == entities.Count() - 1 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index + 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _teamListDal.Guncelle(entities[i]);
            }
        }

        public List<TeamCategory> GetAllTeam()
        {
            var entity = _teamCategoryDal.HepsiniGetir();

            return entity.ToList();
        }

        public TeamCategory GetCategory(Expression<Func<TeamCategory, bool>> filter)
        {
            return _teamCategoryDal.Getir(filter);
        }

        public TeamList GetCategoryList(Expression<Func<TeamList, bool>> filter)
        {
            return _teamListDal.Getir(filter);
        }

        public void UpCategory(Expression<Func<TeamCategory, bool>> filter)
        {
            var entity = _teamCategoryDal.Getir(filter);

            var entities = _teamCategoryDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == 0 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index - 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _teamCategoryDal.Guncelle(entities[i]);
            }
        }

        public void UpTeamList(Expression<Func<TeamList, bool>> filter)
        {
            var entity = _teamListDal.Getir(filter);

            var entities = _teamListDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == 0 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index - 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _teamListDal.Guncelle(entities[i]);
            }
        }
    }
}
