﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.EkipEntities;
using Zemedya.Model.DTO.EkipDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class EkipManager : IEkipService
    {
        private IMapperService _mapperService;
        private IEkipDal _ekipDal;

        public EkipManager(IMapperService mapperService, IEkipDal ekipDal)
        {
            _mapperService = mapperService;
            _ekipDal = ekipDal;
        }

        public void Ekle(EkipDTO EkipDTO)
        {
            var entity = _mapperService.Map<EkipDTO, Ekip>(EkipDTO);
            _ekipDal.Ekle(entity);
        }

        public EkipDTO Getir(Expression<Func<Ekip, bool>> filter)
        {
            var entity = _ekipDal.Getir(filter);
            return _mapperService.Map<Ekip, EkipDTO>(entity);
        }


        public void Guncelle(EkipDTO EkipDTO)
        {
            var entity = _ekipDal.Getir(x => x.Id == EkipDTO.Id);
            var guncelentity = _mapperService.Map<EkipDTO, Ekip>(EkipDTO, entity);
            _ekipDal.Guncelle(guncelentity);
        }

        public List<Ekip> HepsiniGetir(Expression<Func<Ekip, bool>> filter = null)
        {
            return _ekipDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Ekip, bool>> filter)
        {
            _ekipDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Ekip, bool>> filter)
        {
            _ekipDal.PasifYap(filter);
        }
    }
}
