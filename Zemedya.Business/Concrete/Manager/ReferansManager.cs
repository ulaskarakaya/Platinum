﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.ReferansEntities;
using Zemedya.Model.DTO.ReferansDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class ReferansManager : IReferansService
    {
        private IMapperService _mapperService;
        private IReferansDal _referansDal;
        public ReferansManager(IMapperService mapperService, IReferansDal referansDal)
        {
            _mapperService = mapperService;
            _referansDal = referansDal;
        }

        public void Ekle(ReferansDTO ReferansDTO)
        {
            var entity = _mapperService.Map<ReferansDTO, Referans>(ReferansDTO);
            _referansDal.Ekle(entity);
        }

        public ReferansDTO Getir(Expression<Func<Referans, bool>> filter)
        {
            var entity = _referansDal.Getir(filter);
            return _mapperService.Map<Referans, ReferansDTO>(entity);
        }


        public void Guncelle(ReferansDTO ReferansDTO)
        {
            var entity = _referansDal.Getir(x => x.Id == ReferansDTO.Id);
            var guncelentity = _mapperService.Map<ReferansDTO, Referans>(ReferansDTO, entity);
            _referansDal.Guncelle(guncelentity);
        }

        public List<Referans> HepsiniGetir(Expression<Func<Referans, bool>> filter = null)
        {
            return _referansDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Referans, bool>> filter)
        {
            _referansDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Referans, bool>> filter)
        {
            _referansDal.PasifYap(filter);
        }
    }
}
