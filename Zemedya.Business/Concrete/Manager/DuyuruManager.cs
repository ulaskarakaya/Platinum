﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Model.DTO.DuyuruDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class DuyuruManager : IDuyuruService
    {
        private IMapperService _mapperService;
        private IDuyuruDal _duyuruDal;
        public DuyuruManager(IMapperService mapperService, IDuyuruDal duyuruDal)
        {
            _duyuruDal = duyuruDal;
            _mapperService = mapperService;
        }
        public void Ekle(DuyuruDTO DuyuruDTO)
        {
            var entity = _mapperService.Map<DuyuruDTO, Duyuru>(DuyuruDTO);
            _duyuruDal.Ekle(entity);
        }

        public DuyuruDTO Getir(Expression<Func<Duyuru, bool>> filter)
        {
            var Duyuru = _duyuruDal.Getir(filter);
            return _mapperService.Map<Duyuru, DuyuruDTO>(Duyuru);
        }

        public void Guncelle(DuyuruDTO DuyuruDTO)
        {
            var Duyuru = _duyuruDal.Getir(x => x.Id == DuyuruDTO.Id);
            var guncelDuyuru = _mapperService.Map<DuyuruDTO, Duyuru>(DuyuruDTO, Duyuru);
            _duyuruDal.Guncelle(guncelDuyuru);
        }

        public List<Duyuru> HepsiniGetir(Expression<Func<Duyuru, bool>> filter = null)
        {
            return _duyuruDal.HepsiniGetir(filter).ToList();

        }


        public void KalıcıSil(Expression<Func<Duyuru, bool>> filter)
        {
            _duyuruDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Duyuru, bool>> filter)
        {
            _duyuruDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var entity = _duyuruDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SEOKontrolEN(string seo)
        {
            var entity = _duyuruDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
