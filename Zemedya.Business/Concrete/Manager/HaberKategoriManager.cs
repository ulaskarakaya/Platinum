﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Model.DTO.HaberDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class HaberKategoriManager : IHaberKategoriService
    {
        private IMapperService _mapperService;
        private IHaberKategoriDal _haberKategoriDal;
        public HaberKategoriManager(IMapperService mapperService, IHaberKategoriDal haberKategoriDal)
        {
            _mapperService = mapperService;
            _haberKategoriDal = haberKategoriDal;
        }
        public void Ekle(HaberKategoriDTO haberKategoriDTO)
        {
            var entity = _mapperService.Map<HaberKategoriDTO, HaberKategori>(haberKategoriDTO);
            _haberKategoriDal.Ekle(entity);
        }

        public HaberKategoriDTO Getir(Expression<Func<HaberKategori, bool>> filter)
        {
            var kategori = _haberKategoriDal.Getir(filter);
            return _mapperService.Map<HaberKategori, HaberKategoriDTO>(kategori);
        }

        public void Guncelle(HaberKategoriDTO haberKategoriDTO)
        {
            var kategori = _haberKategoriDal.Getir(x => x.Id == haberKategoriDTO.Id);
            var guncelKategori = _mapperService.Map<HaberKategoriDTO, HaberKategori>(haberKategoriDTO, kategori);
            _haberKategoriDal.Guncelle(guncelKategori);
        }

        public List<HaberKategori> HepsiniGetir(Expression<Func<HaberKategori, bool>> filter = null)
        {
            return _haberKategoriDal.HepsiniGetir(filter).ToList();
        }

        public void KalıcıSil(Expression<Func<HaberKategori, bool>> filter)
        {
            _haberKategoriDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<HaberKategori, bool>> filter)
        {
            _haberKategoriDal.PasifYap(filter);
        }
    }
}
