﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.AcikPozisyonEtities;

namespace Zemedya.Business.Concrete.Manager
{
    public class AcikPozisyonManager : IAcikPozisyonService
    {
        private IAcikPozisyonDal _acikPozisyonDal;
        public AcikPozisyonManager(IAcikPozisyonDal acikPozisyonDal)
        {
            _acikPozisyonDal = acikPozisyonDal;
        }

        public void AddOrUpdate(AcikPozisyon model)
        {
            if (model.Id == 0)
            {
                _acikPozisyonDal.Ekle(model);
            }
            else
            {
                _acikPozisyonDal.Guncelle(model);
            }
        }

        public void Delete(Expression<Func<AcikPozisyon, bool>> filter)
        {
            _acikPozisyonDal.KaliciSil(filter);
        }

        public AcikPozisyon Get(Expression<Func<AcikPozisyon, bool>> filter)
        {
            return _acikPozisyonDal.Getir(filter);
        }

        public List<AcikPozisyon> GetAll()
        {
            return _acikPozisyonDal.HepsiniGetir().ToList();
        }
    }
}
