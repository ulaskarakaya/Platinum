﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.OdaEntities;
using Zemedya.Model.DTO.OdaDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class OdaManager : IOdaService
    {
        private IMapperService _mapperService;
        private IOdaDal _odaDal;

        public OdaManager(IMapperService mapperService, IOdaDal odaDal)
        {
            _mapperService = mapperService;
            _odaDal = odaDal;
        }

        public void Ekle(OdaDTO OdaDTO)
        {
            var entity = _mapperService.Map<OdaDTO, Oda>(OdaDTO);
            _odaDal.Ekle(entity);
        }

        public OdaDTO Getir(Expression<Func<Oda, bool>> filter)
        {
            var entity = _odaDal.Getir(filter);
            return _mapperService.Map<Oda, OdaDTO>(entity);
        }


        public void Guncelle(OdaDTO OdaDTO)
        {
            var entity = _odaDal.Getir(x => x.Id == OdaDTO.Id);
            var guncelentity = _mapperService.Map<OdaDTO, Oda>(OdaDTO, entity);
            _odaDal.Guncelle(guncelentity);
        }

        public List<Oda> HepsiniGetir(Expression<Func<Oda, bool>> filter = null)
        {
            return _odaDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Oda, bool>> filter)
        {
            _odaDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Oda, bool>> filter)
        {
            _odaDal.PasifYap(filter);
        }
        public bool SEOKontrol(string seo)
        {
            var entity = _odaDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SEOKontrolEN(string seo)
        {
            var entity = _odaDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
