﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.EtkinlikEntities;
using Zemedya.Model.DTO.EtkinlikDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class EtkinlikManager : IEtkinlikService
    {
        private IMapperService _mapperService;
        private IEtkinlikDal _etkinlikDal;
        public EtkinlikManager(IMapperService mapperService, IEtkinlikDal etkinlikDal)
        {
            _etkinlikDal = etkinlikDal;
            _mapperService = mapperService;
        }

        public void Ekle(EtkinlikDTO EtkinlikDTO)
        {
            var entity = _mapperService.Map<EtkinlikDTO, Etkinlik>(EtkinlikDTO);
            _etkinlikDal.Ekle(entity);
        }

        public EtkinlikDTO Getir(Expression<Func<Etkinlik, bool>> filter)
        {
            var Etkinlik = _etkinlikDal.Getir(filter);
            return _mapperService.Map<Etkinlik, EtkinlikDTO>(Etkinlik);
        }

        public void Guncelle(EtkinlikDTO EtkinlikDTO)
        {
            var Etkinlik = _etkinlikDal.Getir(x => x.Id == EtkinlikDTO.Id);
            var guncelEtkinlik = _mapperService.Map<EtkinlikDTO, Etkinlik>(EtkinlikDTO, Etkinlik);
            _etkinlikDal.Guncelle(guncelEtkinlik);
        }

        public List<Etkinlik> HepsiniGetir(Expression<Func<Etkinlik, bool>> filter = null)
        {
            return _etkinlikDal.HepsiniGetir(filter).OrderBy(x => x.Id).ToList();

        }


        public void KalıcıSil(Expression<Func<Etkinlik, bool>> filter)
        {
            _etkinlikDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Etkinlik, bool>> filter)
        {
            _etkinlikDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var entity = _etkinlikDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SEOKontrolEN(string seo)
        {
            var entity = _etkinlikDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
