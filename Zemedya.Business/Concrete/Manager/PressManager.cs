﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.PressEntities;

namespace Zemedya.Business.Concrete.Manager
{
    public class PressManager : IPressService
    {
        private readonly IPressDal _pressDal;
        public PressManager(IPressDal pressDal)
        {
            _pressDal = pressDal;
        }

        public void AddOrUpdate(Press model)
        {
            if (model.Id == 0)
            {
                model.Order = 9999;
                _pressDal.Ekle(model);
            }
            else
            {
                _pressDal.Guncelle(model);
            }
        }

        public void Delete(Expression<Func<Press, bool>> filter)
        {
            _pressDal.KaliciSil(filter);
        }
        public Press Get(Expression<Func<Press, bool>> filter)
        {
            return _pressDal.Getir(filter);
        }

        public List<Press> GetAll()
        {
            return _pressDal.HepsiniGetir().ToList();
        }

        public void Up(Expression<Func<Press, bool>> filter)
        {
            var entity = _pressDal.Getir(filter);

            var entities = _pressDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == 0 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index - 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _pressDal.Guncelle(entities[i]);
            }
        }
        public void Down(Expression<Func<Press, bool>> filter)
        {
            var entity = _pressDal.Getir(filter);

            var entities = _pressDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == entities.Count() - 1 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index + 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _pressDal.Guncelle(entities[i]);
            }
        }

    }
}
