﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;
using Zemedya.Model.DTO.MevcutSayfalarDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class MevcutSayfalarManager : IMevcutSayfalarService
    {
        private IMapperService _mapperService;
        private IMevcutSayfalarDal _mevcutSayfalarDal;
        public MevcutSayfalarManager(IMapperService mapperService, IMevcutSayfalarDal mevcutSayfalarDal)
        {
            _mevcutSayfalarDal = mevcutSayfalarDal;
            _mapperService = mapperService;
        }

        public MevcutSayfalarDTO Getir(Expression<Func<MevcutSayfalar, bool>> filter)
        {
            var entity = _mevcutSayfalarDal.Getir(filter);
            return _mapperService.Map<MevcutSayfalar, MevcutSayfalarDTO>(entity);
        }

        public void Guncelle(MevcutSayfalarDTO mevcutSayfalarDTO)
        {
            var entity = _mevcutSayfalarDal.Getir(x => x.Id == mevcutSayfalarDTO.Id);
            var guncelentity = _mapperService.Map<MevcutSayfalarDTO, MevcutSayfalar>(mevcutSayfalarDTO, entity);
            _mevcutSayfalarDal.Guncelle(guncelentity);
        }
    }
}
