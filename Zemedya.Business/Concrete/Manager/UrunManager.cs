﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Model.DTO.UrunDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class UrunManager : IUrunService
    {
        private IMapperService _mapperService;
        private IUrunDal _urunDal;
        public UrunManager(IMapperService mapperService, IUrunDal urunDal)
        {
            _urunDal = urunDal;
            _mapperService = mapperService;
        }

        public void Ekle(UrunDTO urunDTO)
        {
            var entity = _mapperService.Map<UrunDTO, Urun>(urunDTO);
            _urunDal.Ekle(entity);
        }

        public UrunDTO Getir(Expression<Func<Urun, bool>> filter)
        {
            var entity = _urunDal.Getir(filter);
            return _mapperService.Map<Urun, UrunDTO>(entity);
        }


        public void Guncelle(UrunDTO UrunDTO)
        {
            var entity = _urunDal.Getir(x => x.Id == UrunDTO.Id);
            var guncelentity = _mapperService.Map<UrunDTO, Urun>(UrunDTO, entity);
            _urunDal.Guncelle(guncelentity);
        }

        public List<Urun> HepsiniGetir(Expression<Func<Urun, bool>> filter = null)
        {
            return _urunDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Urun, bool>> filter)
        {
            _urunDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Urun, bool>> filter)
        {
            _urunDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var entity = _urunDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SEOKontrolEN(string seo)
        {
            var entity = _urunDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
