﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.Business.Concrete.Manager
{
    public class ImageMediaCategoryManager : IImageMediaCategoryService
    {
        private readonly IImageMediaCategoryDal _imageMediaCategoryDal;
        public ImageMediaCategoryManager(IImageMediaCategoryDal imageMediaCategoryDal)
        {
            _imageMediaCategoryDal = imageMediaCategoryDal;
        }

        public void AddOrUpdate(ImageMediaCategory model)
        {
            if (model.Id > 0)
            {
                _imageMediaCategoryDal.Guncelle(model);
            }
            else
            {
                _imageMediaCategoryDal.Ekle(model);
            }
        }

        public void AddOrUpdateItem(ImageMediaItem model)
        {
            if (model.Id > 0)
            {
                _imageMediaCategoryDal.ItemUpdate(model);
            }
            else
            {
                _imageMediaCategoryDal.ItemAdd(model);
            }
        }

        public void Delete(Expression<Func<ImageMediaCategory, bool>> filter)
        {
            _imageMediaCategoryDal.KaliciSil(filter);
        }

        public void DeleteItem(Expression<Func<ImageMediaItem, bool>> filter)
        {
            _imageMediaCategoryDal.ItemSil(filter);
        }

        public ImageMediaCategory Get(Expression<Func<ImageMediaCategory, bool>> filter)
        {
            return _imageMediaCategoryDal.Getir(filter);
        }

        public List<ImageMediaCategory> GetAll()
        {
            return _imageMediaCategoryDal.HepsiniGetir().ToList();
        }

        public ImageMediaItem GetItem(Expression<Func<ImageMediaItem, bool>> filter)
        {
            return _imageMediaCategoryDal.ItemGet(filter);
        }
    }
}
