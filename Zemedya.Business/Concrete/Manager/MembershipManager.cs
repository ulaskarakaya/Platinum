﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.MembershipEntities;

namespace Zemedya.Business.Concrete.Manager
{
    public class MembershipManager : IMembershipService
    {
        private IMapperService _mapperService;
        private IMembershipDal _membershipDal;


        public MembershipManager(IMapperService mapperService, IMembershipDal membershipDal)
        {
            _membershipDal = membershipDal;
            _mapperService = mapperService;
        }

        public void AddOrUpdate(Membership model)
        {
            if (model.Id == 0)
            {
                model.Order = 9999;
                _membershipDal.Ekle(model);
            }
            else
            {
                _membershipDal.Guncelle(model);
            }
        }

        public void Delete(Expression<Func<Membership, bool>> filter)
        {
            _membershipDal.KaliciSil(filter);
        }

        public void DownMembership(Expression<Func<Membership, bool>> filter)
        {
            var entity = _membershipDal.Getir(filter);

            var entities = _membershipDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == entities.Count() - 1 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index + 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _membershipDal.Guncelle(entities[i]);
            }
        }

        public Membership Get(Expression<Func<Membership, bool>> filter)
        {
            return _membershipDal.Getir(filter);
        }

        public List<Membership> GetAll()
        {
            return _membershipDal.HepsiniGetir(x => x.Type == 0).ToList();
        }

        public object GetAllCertificates()
        {
            return _membershipDal.HepsiniGetir(x => x.Type == 1).ToList();
        }

        public void UpMembership(Expression<Func<Membership, bool>> filter)
        {
            var entity = _membershipDal.Getir(filter);

            var entities = _membershipDal.HepsiniGetir().OrderBy(x => x.Order).ToList();

            var listItem = entities.FirstOrDefault(x => x.Id == entity.Id);

            var index = entities.IndexOf(listItem);

            if (index == 0 || index == -1) return;

            entities.Remove(listItem);

            entities.Insert(index - 1, listItem);

            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Order = i;

                _membershipDal.Guncelle(entities[i]);
            }
        }
    }
}
