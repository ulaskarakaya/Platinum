﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.SliderEntities;
using Zemedya.Model.DTO.SliderDTOs;

namespace Zemedya.Business.Concrete.Manager
{ 
    public class SliderManager : ISliderService
    {
        private IMapperService _mapperService;
        private ISliderDal _sliderDal;
        public SliderManager(IMapperService mapperService, ISliderDal sliderDal)
        {
            _mapperService = mapperService;
            _sliderDal = sliderDal;
        }

        public void Ekle(SliderDTO SliderDTO)
        {
            var entity = _mapperService.Map<SliderDTO, Slider>(SliderDTO);
            _sliderDal.Ekle(entity);
        }

        public SliderDTO Getir(Expression<Func<Slider, bool>> filter)
        {
            var entity = _sliderDal.Getir(filter);
            return _mapperService.Map<Slider, SliderDTO>(entity);
        }


        public void Guncelle(SliderDTO SliderDTO)
        {
            var entity = _sliderDal.Getir(x => x.Id == SliderDTO.Id);
            var guncelentity = _mapperService.Map<SliderDTO, Slider>(SliderDTO, entity);
            _sliderDal.Guncelle(guncelentity);
        }

        public List<Slider> HepsiniGetir(Expression<Func<Slider, bool>> filter = null)
        {
            return _sliderDal.HepsiniGetir(filter).OrderBy(x => x.SıraNo).ToList();
        }


        public void KalıcıSil(Expression<Func<Slider, bool>> filter)
        {
            _sliderDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Slider, bool>> filter)
        {
            _sliderDal.PasifYap(filter);
        }
    }
}
