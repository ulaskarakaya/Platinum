﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Model.DTO.GaleriDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class GaleriManager : IGaleriService
    {
        private IMapperService _mapperService;
        private IGaleriDal _galeriDal;

        public GaleriManager(IMapperService mapperService, IGaleriDal galeriDal)
        {
            _galeriDal = galeriDal;
            _mapperService = mapperService;
        }

        public void Ekle(GaleriDTO GaleriDTO)
        {
            var entity = _mapperService.Map<GaleriDTO, Galeri>(GaleriDTO);
            _galeriDal.Ekle(entity);
        }

        public GaleriDTO Getir(Expression<Func<Galeri, bool>> filter)
        {
            var entity = _galeriDal.Getir(filter);
            return _mapperService.Map<Galeri, GaleriDTO>(entity);
        }


        public void Guncelle(GaleriDTO GaleriDTO)
        {
            var entity = _galeriDal.Getir(x => x.Id == GaleriDTO.Id);
            var guncelentity = _mapperService.Map<GaleriDTO, Galeri>(GaleriDTO, entity);
            _galeriDal.Guncelle(guncelentity);
        }

        public List<Galeri> HepsiniGetir(Expression<Func<Galeri, bool>> filter = null)
        {
            return _galeriDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Galeri, bool>> filter)
        {
            _galeriDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Galeri, bool>> filter)
        {
            _galeriDal.PasifYap(filter);
        }
    }
}
