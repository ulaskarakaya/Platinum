﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Model.DTO.HaberDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class HaberManager : IHaberService
    {
        private IMapperService _mapperService;
        private IHaberDal _haberDal;

        public HaberManager(IMapperService mapperService, IHaberDal haberDal)
        {
            _mapperService = mapperService;
            _haberDal = haberDal;
        }


        public void Ekle(HaberDTO haberDTO)
        {
            var entity = _mapperService.Map<HaberDTO, Haber>(haberDTO);
            _haberDal.Ekle(entity);
        }

        public HaberDTO Getir(Expression<Func<Haber, bool>> filter)
        {
            var haber = _haberDal.Getir(filter);
            return _mapperService.Map<Haber, HaberDTO>(haber);
        }

        public void Guncelle(HaberDTO haberDTO)
        {
            var haber = _haberDal.Getir(x => x.Id == haberDTO.Id);
            var guncelHaber = _mapperService.Map<HaberDTO, Haber>(haberDTO, haber);
            _haberDal.Guncelle(guncelHaber);
        }

        public List<Haber> HepsiniGetir(Expression<Func<Haber, bool>> filter = null)
        {
            return _haberDal.HepsiniGetir(filter).ToList(); 
            
        }

        public void KaliciSil(Expression<Func<Haber, bool>> filter)
        {
            _haberDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Haber, bool>> filter)
        {
            _haberDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var haber = _haberDal.Getir(x => x.SEO == seo);
            if (haber != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SEOKontrolEN(string seo)
        {
            var haber = _haberDal.Getir(x => x.SEOEN == seo);
            if (haber != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
