﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.IletisimEntities;
using Zemedya.Model.DTO.IletisimDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class IletisimManager : IIletisimService
    {
        private IMapperService _mapperService;
        private IIletisimDal _iletisimDal;
        public IletisimManager(IMapperService mapperService, IIletisimDal iletisimDal)
        {
            _mapperService = mapperService;
            _iletisimDal = iletisimDal;
        }

        public void Ekle(IletisimDTO IletisimDTO)
        {
            var entity = _mapperService.Map<IletisimDTO, Iletisim>(IletisimDTO);
            _iletisimDal.Ekle(entity);
        }

        public IletisimDTO Getir(Expression<Func<Iletisim, bool>> filter)
        {
            var entity = _iletisimDal.Getir(filter);
            return _mapperService.Map<Iletisim, IletisimDTO>(entity);
        }


        public void Guncelle(IletisimDTO IletisimDTO)
        {
            var entity = _iletisimDal.Getir(x => x.Id == IletisimDTO.Id);
            var guncelentity = _mapperService.Map<IletisimDTO, Iletisim>(IletisimDTO, entity);
            _iletisimDal.Guncelle(guncelentity);
        }

        public List<Iletisim> HepsiniGetir(Expression<Func<Iletisim, bool>> filter = null)
        {
            return _iletisimDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Iletisim, bool>> filter)
        {
            _iletisimDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Iletisim, bool>> filter)
        {
            _iletisimDal.PasifYap(filter);
        }
    }
}
