﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.AyarEntities;
using Zemedya.Model.DTO.AyarlarDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class AyarlarManager : IAyarlarService
    {
        private IMapperService _mapperService;
        private IAyarlarDal _ayarlarDal;
        public AyarlarManager(IMapperService mapperService, IAyarlarDal ayarlarDal )
        {
            _ayarlarDal = ayarlarDal;
            _mapperService = mapperService;
        }
        public void Ekle(AyarlarDTO AyarlarDTO)
        {
            var entity = _mapperService.Map<AyarlarDTO, Ayarlar>(AyarlarDTO);
            _ayarlarDal.Ekle(entity);
        }

        public AyarlarDTO Getir(Expression<Func<Ayarlar, bool>> filter)
        {
            var entity = _ayarlarDal.Getir(filter);
            return _mapperService.Map<Ayarlar, AyarlarDTO>(entity);
        }


        public void Guncelle(AyarlarDTO AyarlarDTO)
        {
            var entity = _ayarlarDal.Getir(x => x.Id == AyarlarDTO.Id);
            var guncelentity = _mapperService.Map<AyarlarDTO, Ayarlar>(AyarlarDTO, entity);
            _ayarlarDal.Guncelle(guncelentity);
        }

        public List<Ayarlar> HepsiniGetir(Expression<Func<Ayarlar, bool>> filter = null)
        {
            return _ayarlarDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Ayarlar, bool>> filter)
        {
            _ayarlarDal.KaliciSil(filter);
        }
    }
}
