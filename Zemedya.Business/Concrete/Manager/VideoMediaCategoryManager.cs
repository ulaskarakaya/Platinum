﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.MediaEntities;

namespace Zemedya.Business.Concrete.Manager
{
    public class VideoMediaCategoryManager : IVideoMediaCategoryService
    {
        private readonly IVideoMediaCategoryDal _videoMediaCategoryDal;
        public VideoMediaCategoryManager(IVideoMediaCategoryDal videoMediaCategoryDal)
        {
            _videoMediaCategoryDal = videoMediaCategoryDal;
        }

        public void AddOrUpdate(VideoMediaCategory model)
        {
            if (model.Id > 0)
            {
                _videoMediaCategoryDal.Guncelle(model);
            }
            else
            {
                _videoMediaCategoryDal.Ekle(model);
            }
        }

        public void AddOrUpdateItem(VideoMediaItem model)
        {
            if (model.Id > 0)
            {
                _videoMediaCategoryDal.ItemUpdate(model);
            }
            else
            {
                _videoMediaCategoryDal.ItemAdd(model);
            }
        }

        public void Delete(Expression<Func<VideoMediaCategory, bool>> filter)
        {
            _videoMediaCategoryDal.KaliciSil(filter);
        }

        public void DeleteItem(Expression<Func<VideoMediaItem, bool>> filter)
        {
            _videoMediaCategoryDal.ItemSil(filter);
        }

        public VideoMediaCategory Get(Expression<Func<VideoMediaCategory, bool>> filter)
        {
            return _videoMediaCategoryDal.Getir(filter);
        }

        public List<VideoMediaCategory> GetAll()
        {
            return _videoMediaCategoryDal.HepsiniGetir().ToList();
        }

        public VideoMediaItem GetItem(Expression<Func<VideoMediaItem, bool>> filter)
        {
            return _videoMediaCategoryDal.ItemGet(filter);
        }
    }
}
