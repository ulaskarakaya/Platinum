﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.Business.Abstract.UnitOfWork;
using Zemedya.Business.Concrete.Manager;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals;

namespace Zemedya.Business.Concrete.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectContext _context;

        private IHaberDal _haberDal;
        private IHaberKategoriDal _haberKategoriDal;
        private IYoneticiDal _yoneticiDal;
        private IBlogKategoriDal _blogKategoriDal;
        private IBlogDal _blogDal;
        private IIletisimDal _iletisimDal;
        private IPopupDal _popupDal;
        private ISliderDal _sliderDal;
        private IMevcutSayfalarDal _mevcutSayfalarDal;
        private IUrunDal _urunDal;
        private IUrunKategoriDal _urunKategoriDal;
        private IAyarlarDal _ayarlarDal;
        private IDuyuruDal _duyuruDal;
        private IEtkinlikDal _etkinlikDal;
        private IKurumsalDal _kurumsalDal;
        private IVideoDal _videoDal;
        private IReferansDal _referansDal;
        private IMevcutDillerDal _mevcutDillerDal;
        private IEkipDal _ekipDal;
        private IGaleriDal _galeriDal;
        private IGaleriKategoriDal _galeriKategoriDal;
        private IOdaDal _odaDal;
        private ITeamCategoryDal _teamCategoryDal;
        private ITeamListDal _teamListDal;
        private IMembershipDal _membershipDal;
        private IPressDal _pressDal;
        private IImageMediaCategoryDal _imageMediaCategoryDal;
        private IVideoMediaCategoryDal _videoMediaCategoryDal;
        private IAcikPozisyonDal _acikPozisyonDal;

        private IMapperService _mapperService;
        private IHaberService _haberService;
        private IHaberKategoriService _haberKategoriService;
        private IYoneticiService _yoneticiService;
        private IBlogKategoriService _blogKategoriService;
        private IBlogService _blogService;
        private IIletisimService _iletisimService;
        private IPopupService _popupService;
        private ISliderService _sliderService;
        private IMevcutSayfalarService _mevcutSayfalarService;
        private IUrunService _urunService;
        private IUrunKategoriService _urunKategoriService;
        private IAyarlarService _ayarlarService;
        private IDuyuruService _duyuruService;
        private IEtkinlikService _etkinlikService;
        private IKurumsalService _kurumsalService;
        private IReferansService _referansService;
        private IVideoService _videoService;
        private IMevcutDillerService _mevcutDillerService;
        private IGaleriService _galeriService;
        private IEkipService _ekipService;
        private IGaleriKategoriService _galeriKategoriService;
        private IOdaService _odaService;
        private ITeamService _teamService;
        private IMembershipService _membershipService;
        private IPressService _pressService;
        private IImageMediaCategoryService _imageMediaCategoryService;
        private IVideoMediaCategoryService _videoMediaCategoryService;
        private IAcikPozisyonService _acikPozisyonService;

        public UnitOfWork()
        {
            _context = new ProjectContext();

            _haberDal = new EfHaberDal(_context);
            _haberKategoriDal = new EfHaberKategoriDal(_context);
            _yoneticiDal = new EfYoneticiDal(_context);
            _blogKategoriDal = new EfBlogKategoriDal(_context);
            _blogDal = new EfBlogDal(_context);
            _iletisimDal = new EfIletisimDal(_context);
            _popupDal = new EfPopupDal(_context);
            _sliderDal = new EfSliderDal(_context);
            _mevcutSayfalarDal = new EfMevcutSayfalarDal(_context);
            _urunDal = new EfUrunDal(_context);
            _urunKategoriDal = new EfUrunKategoriDal(_context);
            _ayarlarDal = new EfAyarlarDal(_context);
            _duyuruDal = new EfDuyuruDal(_context);
            _etkinlikDal = new EfEtkinlikDal(_context);
            _referansDal = new EfReferansDal(_context);
            _kurumsalDal = new EfKurumsalDal(_context);
            _videoDal = new EfVideoDal(_context);
            _mevcutDillerDal = new EfMevcutDillerDal(_context);
            _ekipDal = new EfEkipDal(_context);
            _galeriDal = new EfGaleriDal(_context);
            _galeriKategoriDal = new EfGaleriKategoriDal(_context);
            _odaDal = new EfOdaDal(_context);
            _teamCategoryDal = new EfTeamCategoryDal(_context);
            _teamListDal = new EfTeamListDal(_context);
            _membershipDal = new EfMembershipDal(_context);
            _pressDal = new EfPressDal(_context);
            _imageMediaCategoryDal = new EfImageMediaCategoryDal(_context);
            _videoMediaCategoryDal = new EfVideoMediaCategoryDal(_context);
            _acikPozisyonDal = new EfAcikPozisyonDal(_context);

            _mapperService = new AutoMapperManager();
            _haberService = new HaberManager(_mapperService, _haberDal);
            _haberKategoriService = new HaberKategoriManager(_mapperService, _haberKategoriDal);
            _yoneticiService = new YoneticiManager(_mapperService, _yoneticiDal);
            _blogKategoriService = new BlogKategoriManager(_mapperService, _blogKategoriDal);
            _blogService = new BlogManager(_mapperService, _blogDal);
            _iletisimService = new IletisimManager(_mapperService, _iletisimDal);
            _popupService = new PopupManager(_mapperService, _popupDal);
            _sliderService = new SliderManager(_mapperService, _sliderDal);
            _mevcutSayfalarService = new MevcutSayfalarManager(_mapperService, _mevcutSayfalarDal);
            _urunService = new UrunManager(_mapperService, _urunDal);
            _urunKategoriService = new UrunKategoriManager(_mapperService, _urunKategoriDal);
            _ayarlarService = new AyarlarManager(_mapperService, _ayarlarDal);
            _duyuruService = new DuyuruManager(_mapperService, _duyuruDal);
            _etkinlikService = new EtkinlikManager(_mapperService, _etkinlikDal);
            _kurumsalService = new KurumsalManager(_mapperService, _kurumsalDal);
            _videoService = new VideoManager(_mapperService, _videoDal);
            _referansService = new ReferansManager(_mapperService, _referansDal);
            _mevcutDillerService = new MevcutDillerManager(_mapperService, _mevcutDillerDal);
            _ekipService = new EkipManager(_mapperService, _ekipDal);
            _galeriService = new GaleriManager(_mapperService, _galeriDal);
            _galeriKategoriService = new GaleriKategoriManager(_mapperService, _galeriKategoriDal);
            _odaService = new OdaManager(_mapperService, _odaDal);
            _teamService = new TeamManager(_mapperService, _teamCategoryDal, _teamListDal);
            _membershipService = new MembershipManager(_mapperService, _membershipDal);
            _pressService = new PressManager(_pressDal);
            _imageMediaCategoryService = new ImageMediaCategoryManager(_imageMediaCategoryDal);
            _videoMediaCategoryService = new VideoMediaCategoryManager(_videoMediaCategoryDal);
            _acikPozisyonService = new AcikPozisyonManager(_acikPozisyonDal);

        }
        public IHaberService HaberManager { get { return _haberService; } }

        public IHaberKategoriService HaberKategoriManager { get { return _haberKategoriService; } }

        public IYoneticiService YoneticiManager { get { return _yoneticiService; } }

        public IBlogKategoriService BlogKategoriManager { get { return _blogKategoriService; } }

        public IBlogService BlogManager { get { return _blogService; } }

        public IIletisimService IletisimManager { get { return _iletisimService; } }

        public IPopupService PopupManager { get { return _popupService; } }

        public ISliderService SliderManager { get { return _sliderService; } }

        public IMevcutSayfalarService MevcutSayfalarManager { get { return _mevcutSayfalarService; } }

        public IUrunService UrunManager { get { return _urunService; } }

        public IUrunKategoriService UrunKategoriManager { get { return _urunKategoriService; } }

        public IAyarlarService AyarlarManager { get { return _ayarlarService; } }

        public IDuyuruService DuyuruManager { get { return _duyuruService; } }

        public IEtkinlikService EtkinlikManager { get { return _etkinlikService; } }

        public IReferansService ReferansManager { get { return _referansService; } }

        public IVideoService VideoManager { get { return _videoService; } }

        public IKurumsalService KurumsalManager { get { return _kurumsalService; } }

        public IMevcutDillerService MevcutDillerManager { get { return _mevcutDillerService; } }

        public IEkipService EkipManager { get { return _ekipService; } }

        public IGaleriService GaleriManager { get { return _galeriService; } }

        public IGaleriKategoriService GaleriKategoriManager { get { return _galeriKategoriService; } }

        public IOdaService OdaManager { get { return _odaService; } }
        public ITeamService TeamManager { get { return _teamService; } }
        public IMembershipService MembershipManager { get { return _membershipService; } }
        public IPressService PressService { get { return _pressService; } }
        public IImageMediaCategoryService ImageMediaCategoryManager { get { return _imageMediaCategoryService; } }
        public IVideoMediaCategoryService VideoMediaCategoryManager { get { return _videoMediaCategoryService; } }
        public IAcikPozisyonService AcikPozisyonManager { get { return _acikPozisyonService; } }

        public bool Complete()
        {
            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
